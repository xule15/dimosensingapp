package mami.dimoapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import mami.dimoapp.custom.CustomRequest2;
import mami.dimoapp.model.Store;

/**
 * Created by Fernando on 07/09/2016.
 */
public class ActivityRecognizedService extends IntentService {

    static long startTime, totalTime;
    String currentActivity="", pastActivity=Store.get().getPastActivity();
    private int activitySave = 0;
    String email=Store.get().getEmail();


    public ActivityRecognizedService() {
        super("ActivityRecognizedService");
    }

    public ActivityRecognizedService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            handleDetectedActivities( result.getProbableActivities() , result.getTime());
        }
    }

    private void handleDetectedActivities(List<DetectedActivity> probableActivities, long time) {
        for( DetectedActivity activity : probableActivities ) {
            switch( activity.getType() ) {
                case DetectedActivity.IN_VEHICLE: {
                    Log.e("type ","type "+activity.getType());
                    Log.e("emailActivity ","emailActivity "+email);

                    Log.e( "ActivityRecogition", "In Vehicle: " + activity.getConfidence() );
                    Log.e("Actividad","en coche");
                    //Vehicle is similar to Still in this version
                    if( activity.getConfidence() >= 75 ) {

                        doing("Still", String.valueOf(activity.getType()));
                    }
                    break;
                }
                case DetectedActivity.ON_BICYCLE: {
                    if( activity.getConfidence() >= 75 ) {
                        Log.e("emailActivity ","emailActivity "+email);
                        Log.e("type ","type "+activity.getType());

                        doing("Bicycle", String.valueOf(activity.getType()));
                        Log.e("ActivityRecogition", "On Bicycle: " + activity.getConfidence());
                        Log.e("Actividad", "en bici");
                    }
                    break;
                }
                case DetectedActivity.ON_FOOT: {
                    Log.e("emailActivity ","emailActivity "+email);
                    Log.e("type ","type "+activity.getType());

                    Log.e("Actividad","en foot");
                    Log.e( "ActivityRecogition", "On Foot: " + activity.getConfidence() );
                    if( activity.getConfidence() >= 75 ) {
                        doing("On Foot", String.valueOf(activity.getType()));
                    }
                    break;
                }
                case DetectedActivity.RUNNING: {
                    Log.e("emailActivity ","emailActivity "+email);
                    Log.e("type ","type "+activity.getType());

                    Log.e("Actividad","corriendo");
                    //doing("Running");
                    Log.e( "ActivityRecogition", "Running: " + activity.getConfidence() );
                    if( activity.getConfidence() >= 75 ) {
                        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
                        builder.setContentText( "Are you running?" );
                        builder.setSmallIcon( R.mipmap.ic_launcher );
                        builder.setContentTitle( getString( R.string.app_name ) );
                        NotificationManagerCompat.from(this).notify(0, builder.build());*/
                        doing("Running", String.valueOf(activity.getType()));
                    }
                    break;
                }
                case DetectedActivity.STILL: {
                    Log.e("emailActivity ","emailActivity "+email);
                    Log.e("type ","type "+activity.getType());

                    Log.e("Actividad","en reposo");
                    Log.e( "ActivityRecogition", "Still: " + activity.getConfidence() );
                    if( activity.getConfidence() >= 75 ) {
                        Log.e("dentroStrill","dentroStill");
                       //doing("Still");
                    }
                    break;
                }
                case DetectedActivity.TILTING: {
                    Log.e("emailActivity ","emailActivity "+email);
                    Log.e("type ","type "+activity.getType());

                    Log.e("Actividad","en tilting");
                    Log.e( "ActivityRecogition", "Tilting: " + activity.getConfidence() );
                    if( activity.getConfidence() >= 75 ) {
                        //doing("Tilting");
                    }
                    break;
                }
                case DetectedActivity.WALKING: {
                    Log.e("emailActivity ","emailActivity "+email);
                    Log.e("type ","type "+activity.getType());

                    Log.e("Actividad","andando");
                    Log.e( "ActivityRecogition", "Walking: " + activity.getConfidence() );
                    if( activity.getConfidence() >= 65 ) {

                        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
                        builder.setContentText( "Are you walking?" );
                        builder.setSmallIcon( R.mipmap.ic_launcher );
                        builder.setContentTitle( getString( R.string.app_name ) );
                        NotificationManagerCompat.from(this).notify(0, builder.build());*/
                        doing("Walking",String.valueOf(activity.getType()));
                    }
                    break;
                }
                case DetectedActivity.UNKNOWN: {
                    Log.e("emailActivity ","emailActivity "+email);
                    if( activity.getConfidence() >= 65 ) {

                       // doing("Uknow");
                        Log.e("Actividad","desconocida");
                        Log.e( "ActivityRecogition", "Unknown: " + activity.getConfidence() );
                    }

                    break;
                }
            }
            Log.e( "TimeActivityRecogition", "Time: " + time );
        }
    }

    public void doing(String currentActivity, String type){
        Log.e("comparacion ","past "+pastActivity);
        Log.e("comparacion ","current "+currentActivity);

        if(currentActivity != pastActivity){
            Log.e("iguales????","somosDistintos" +"current "+currentActivity+" past "+pastActivity);
            if(pastActivity != null){
                Log.e("iguales????","HolaPastaActivity");
                saveActivity(pastActivity,type);


            }
            Store.get().setPastActivity(currentActivity);
            pastActivity=currentActivity;
            Log.e("Dentro igual ","Dentro igual "+"past "+pastActivity+" curremt "+currentActivity);

            //saveActivity(pastActivity);
        }else{
            Log.e("iguales????","somosIguales" +"current "+currentActivity+" past "+pastActivity);

        }
        //saveActivity((currentActivity));

    }

    public void saveActivity(String pastActivitySaved, String type){
        Log.e("dentroSave","dentroSave");
        //0=WALK, 1=RUN
        totalTime = System.currentTimeMillis() - startTime;
        Log.e("TOTALTIME0",totalTime+"");
        int calories = 0;

        if(Store.get().getWeight()== 0)Store.get().setWeight(70f);
        int weight = (int)Store.get().getWeight();
        //Distance (meters) = steps /1320 * 1000
        int pasos=Store.get().getSteps();
       // float distance = pasos/1320*1000;



        //Calories(walking) = 2/3 * kilogrames * distance (kilometers)
        //calories = (int)(2/3* weight * distance/1000);


        JSONObject activity = new JSONObject();

            Log.e("HolaType ","holaType");



        try {
            activity.put("email",Store.get().getEmail());
            activity.put("type", type);
            activity.put("duration", totalTime/1000);
            activity.put("steps", Store.get().getSteps());
            activity.put("weight", weight);
           // activity.put("calories", calories);



            Log.e("JsonBienformado", activity.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Instantiate the RequestQueue.


        String url = "http://aplicaciondimo.com/setActivity.php";
        Log.e("urlActivity ","urlActivity "+url);

        CustomRequest2 customRequest = new CustomRequest2(Request.Method.POST, url,activity,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("GUARDAR ACTIVIDAD","Response is: "+ jsonArray.toString());
                        //Store.get().setSteps(0);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errorActi ","errorActi "+ error.toString() );
            }
        });


        // and finally add the request to the queue
        Log.e("customRequestActi1","customRequest1 "+ Arrays.toString(customRequest.getBody()));
        Volley.newRequestQueue(getApplicationContext()).add(customRequest);

        //Reset Activity
        startTime = 0;
        totalTime = 0;
        //pastActivity = "";
    }
}
