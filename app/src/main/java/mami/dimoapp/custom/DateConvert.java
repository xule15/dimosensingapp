package mami.dimoapp.custom;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Fernando on 16/12/2016.
 */

public class DateConvert {

    public DateConvert(){

    }

    public static long valuesToMilliseconds(int day, int month, int year, int hour, int minutes, int seconds){
        Date date = null;

        //String dateInString = "22/01/2015 10:20:56";

        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm:ss");
        String dateInString = checkDate(day, month, year, hour, minutes, seconds);

        try {
            date = sdf.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date.getTime();
    }


    //Date format
    public static String checkDate(int dayOfMonth, int monthOfYear, int year, int hour, int minutes, int seconds){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year+" ";
        if(hour<10){
            date = date + "0"+hour;
        }else{
            date = date +hour;
        }
        if(minutes<10){
            date = date + ":0"+minutes;
        }else{
            date = date +":"+minutes;
        }
        if(seconds<10){
            date = date + ":0"+seconds;
        }else{
            date = date +":"+seconds;
        }
        return date;
    }

    //Date format
    public static String checkDateWithoutHours(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    //Date format
    public static String checkDateWithoutHoursWithSlashBar(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    public static String millisecondsToDateStringWithoutSeconds(long time){
        Date date = new Date(time);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return formatter.format(date).toString();
    }

    public static String millisecondsToDateStringWithoutDate(long time){
        Date date = new Date(time);
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(date).toString();
    }

    public static String millisecondsToDateStringWithoutSecondsBreak(long time){
        Date date = new Date(time);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy \n HH:mm");
        return formatter.format(date).toString();
    }

    public static String millisecondsToDateStringWithoutHours(long time){
        Date date = new Date(time);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(date).toString();
    }

    public static String millisecondsToDateStringWithoutHoursandYear(long time){
        Date date = new Date(time);
        DateFormat formatter = new SimpleDateFormat("dd/MM");
        return formatter.format(date).toString();
    }

    public static Date millisecondsToDate(long time){
        Date date = new Date(time);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return date;
    }

    public static Date valuesToDate(int day, int month, int year){
        Date date = null;

        //TODO fecha bien
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String dateInString = checkDateWithoutHours(day, month, year);

        try {
            date = sdf.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
