package mami.dimoapp.custom;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

/**
 * Created by philipp on 02/06/16.
 */
public class MyValueFormatter implements IAxisValueFormatter
{

    private BarLineChartBase<?> chart;

    public MyValueFormatter(BarLineChartBase<?> chart) {
        this.chart = chart;

    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

        long time = (long) value;


        String date = DateConvert.millisecondsToDateStringWithoutHoursandYear(time);

        return date;

    }

}
