package mami.dimoapp.custom;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Fernando on 20/01/2017.
 */

public class CustomRequest3 extends JsonObjectRequest {

    // Since we're extending a Request class
    // we just use its constructor
    public CustomRequest3(int method, String url, JSONObject jsonRequest,
                          Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    private Map<String, String> headers = new HashMap<>();

    /**
     * Custom class!
     */



}