package mami.dimoapp.custom;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

/**
 * Created by Fernando on 29/01/2017.
 */

public class XAxisDateFormatter implements IAxisValueFormatter {
    private final static String logTAG = XAxisDateFormatter.class.getName() + ".";

    private int yearRange;
    private Object dataRange;
    private ArrayList list;

    public XAxisDateFormatter(int yearRange, Object dataRange, ArrayList list) {
        this.yearRange = yearRange;
        this.dataRange = dataRange;

        this.list = list;
    }

    public XAxisDateFormatter(int yearRange, Object dataRange) {
        this.yearRange = yearRange;
        this.dataRange = dataRange;
        this.list = null;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        long time = (long)value;
        if (((int) value) < list.size()) {
            return DateConvert.millisecondsToDateStringWithoutHoursandYear(time);
        }
        return "";
    }


}

