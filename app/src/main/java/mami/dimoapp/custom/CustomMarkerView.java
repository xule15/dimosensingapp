package mami.dimoapp.custom;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.utils.MPPointF;

import mami.dimoapp.R;

public class CustomMarkerView extends MarkerView {

    private TextView tvContent;

    public CustomMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    /*@Override
    public void refreshContent(Entry e, Highlight highlight) {

        tvContent.setText("" + e.getY()); // set the entry-value as the display text

        //tvContent.setText("" + Utils.formatNumber(e.getY(), 0, true));

    }*/

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
