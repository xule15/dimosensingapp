package mami.dimoapp;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import mami.dimoapp.fragments.FragmentActivity;
import mami.dimoapp.fragments.FragmentAwards;
import mami.dimoapp.fragments.FragmentAwards2;
import mami.dimoapp.fragments.FragmentAwards3;
import mami.dimoapp.fragments.FragmentAwards4;
import mami.dimoapp.fragments.FragmentGlucose;
import mami.dimoapp.fragments.FragmentGraphics;
import mami.dimoapp.fragments.FragmentNutrition;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class ActivityRewards extends AppCompatActivity implements TabLayout.OnTabSelectedListener{
    private FragmentGraphics fragmentGraph;
    private FragmentActivity fragmentAct;
    private FragmentNutrition fragmentNut;
    private FragmentGlucose fragmentGlu;
    private FragmentAwards fragmentAwards;
    private FragmentAwards2 fragmentAwards2;
    private FragmentAwards3 fragmentAwards3;
    private FragmentAwards4 fragmentAwards4;
    private String name, email, personPhoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Premios");

        fragmentGraph = new FragmentGraphics();
        fragmentAct = new FragmentActivity();
        fragmentNut = new FragmentNutrition();
        fragmentGlu = new FragmentGlucose();

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }





        TabLayout tabs = (TabLayout) findViewById(R.id.tabsStats);

        tabs.addTab(tabs.newTab().setText("Premio 1"),true);
        tabs.addTab(tabs.newTab().setText("Premio 2"));
        tabs.addTab(tabs.newTab().setText("Premio 3"));
        tabs.addTab(tabs.newTab().setText("Premio 4"));
        setCurrentTabFragment(0);

        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                setCurrentTabFragment(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("email", email);
            intent.putExtra("name", name);
            intent.putExtra("photo", personPhoto);
            NavUtils.navigateUpTo(this, intent);
            return true;
        }
        if(id == R.id.action_stats_help) {
            new MaterialShowcaseView.Builder(this)

                    .withOvalShape()
                    .setDismissText(getString(R.string.Helptext_Next))
                    .setContentText(getString(R.string.Helptext_Stats))
                    .show();
        }


            return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reward, menu);

        //setBadgeCount(this, mCartMenuIcon, ""+ 1);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        setCurrentTabFragment(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void setCurrentTabFragment(int tabPosition)
    {
        switch (tabPosition)
        {
            case 0 :


                fragmentAwards = new FragmentAwards();
                replaceFragment(fragmentAwards);
                break;
            case 1 :
                fragmentAwards2 = new  FragmentAwards2();
                replaceFragment(fragmentAwards2);
                break;
            case 2 :
                fragmentAwards3 = new  FragmentAwards3();
                replaceFragment(fragmentAwards3);
                break;
            case 3 :
                fragmentAwards4 = new  FragmentAwards4();
                replaceFragment(fragmentAwards4);
        }
    }
    public void replaceFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }
}
