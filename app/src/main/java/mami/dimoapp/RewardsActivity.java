package mami.dimoapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import mami.dimoapp.fragments.FragmentActivity;
import mami.dimoapp.fragments.FragmentAwards;
import mami.dimoapp.fragments.FragmentAwards2;
import mami.dimoapp.fragments.FragmentAwards3;
import mami.dimoapp.fragments.FragmentAwards4;
import mami.dimoapp.fragments.FragmentAwards5;
import mami.dimoapp.fragments.FragmentAwards6;
import mami.dimoapp.fragments.FragmentAwards7;
import mami.dimoapp.fragments.FragmentAwards8;
import mami.dimoapp.fragments.FragmentGlucose;
import mami.dimoapp.fragments.FragmentGraphics;
import mami.dimoapp.fragments.FragmentNutrition;
import mami.dimoapp.model.Store;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

import static mami.dimoapp.R.drawable.icon_help;

public class RewardsActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{
    private FragmentAwards fragmentAwards;
    private FragmentAwards2 fragmentAwards2;
    private FragmentAwards3 fragmentAwards3;
    private FragmentAwards4 fragmentAwards4;
    private FragmentAwards5 fragmentAwards5;
    private FragmentAwards6 fragmentAwards6;
    private FragmentAwards7 fragmentAwards7;
    private FragmentAwards8 fragmentAwards8;
    private ImageView btnhelp;

    private String name, email, personPhoto,tabs4,tabMainS,subtabS;
    private int tabMain=0;
    private int subtab=0;
    private int tabs2;




    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewardtab);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Premios");

        btnhelp= (ImageView) findViewById(R.id.btnhelp);
        final ImageView iconAward= (ImageView) findViewById(R.id.iconReward);
        if(Store.get().getCategoria()==0){
            iconAward.setImageResource(R.drawable.ic_bronce);
        }
        else if(Store.get().getCategoria()==1){
            iconAward.setImageResource(R.drawable.ic_plata);
        }
        else{
            iconAward.setImageResource(R.drawable.ic_oro);
        }





        fragmentAwards = new FragmentAwards();
        fragmentAwards2 = new FragmentAwards2();
        fragmentAwards3 = new FragmentAwards3();
        fragmentAwards4 = new FragmentAwards4();
        fragmentAwards5 = new FragmentAwards5();
        fragmentAwards6 = new FragmentAwards6();
        fragmentAwards7 = new FragmentAwards7();
        fragmentAwards8 = new FragmentAwards8();

        btnhelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ShowcaseConfig config = new ShowcaseConfig();
                config.setDelay(500); // half second between each showcase view
                config.setShapePadding(0);

                MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(RewardsActivity.this);

                sequence.setConfig(config);
                sequence.addSequenceItem(iconAward,
                        getString(R.string.Helptext_Category),getString(R.string.Helptext_Next));


                if(fragmentAwards.isResumed()&& fragmentAwards.btnCanjear.getVisibility()==View.VISIBLE){
                    sequence.addSequenceItem(new MaterialShowcaseView.Builder(RewardsActivity.this)

                            .setTarget(fragmentAwards.btnCanjear)

                            .withRectangleShape()
                            .setContentText(getString(R.string.Helptext_Awards))
                            .setDismissText(getString(R.string.Helptext_Next))

                            .build());

                } else  if(fragmentAwards2.isResumed()&& fragmentAwards2.btnCanjear.getVisibility()==View.VISIBLE){
                    sequence.addSequenceItem(new MaterialShowcaseView.Builder(RewardsActivity.this)
                            .setTarget(fragmentAwards2.btnCanjear)

                            .withRectangleShape()
                            .setContentText(getString(R.string.Helptext_Awards))
                            .setDismissText(getString(R.string.Helptext_Next))

                            .build());

                } else  if(fragmentAwards3.isResumed()&& fragmentAwards3.btnCanjear.getVisibility()==View.VISIBLE){
                    sequence.addSequenceItem(new MaterialShowcaseView.Builder(RewardsActivity.this)
                            .setTarget(fragmentAwards3.btnCanjear)

                            .withRectangleShape()
                            .setContentText(getString(R.string.Helptext_Awards))
                            .setDismissText(getString(R.string.Helptext_Next))

                            .build());

                } else  if(fragmentAwards4.isResumed()&& fragmentAwards4.btnCanjear.getVisibility()==View.VISIBLE){
                    sequence.addSequenceItem(new MaterialShowcaseView.Builder(RewardsActivity.this)
                            .setTarget(fragmentAwards4.btnCanjear)

                            .withRectangleShape()
                            .setContentText(getString(R.string.Helptext_Awards))
                            .setDismissText(getString(R.string.Helptext_Next))

                            .build());

                } else  if(fragmentAwards5.isResumed()&& fragmentAwards5.btnCanjear.getVisibility()==View.VISIBLE){
                    sequence.addSequenceItem(new MaterialShowcaseView.Builder(RewardsActivity.this)
                            .setTarget(fragmentAwards5.btnCanjear)

                            .withRectangleShape()
                            .setContentText(getString(R.string.Helptext_Awards))
                            .setDismissText(getString(R.string.Helptext_Next))

                            .build());

                } else  if(fragmentAwards6.isResumed()&& fragmentAwards6.btnCanjear.getVisibility()==View.VISIBLE){
                    sequence.addSequenceItem(new MaterialShowcaseView.Builder(RewardsActivity.this)
                            .setTarget(fragmentAwards6.btnCanjear)

                            .withRectangleShape()
                            .setContentText(getString(R.string.Helptext_Awards))
                            .setDismissText(getString(R.string.Helptext_Next))

                            .build());

                } else  if(fragmentAwards7.isResumed()&& fragmentAwards7.btnCanjear.getVisibility()==View.VISIBLE){
                    sequence.addSequenceItem(new MaterialShowcaseView.Builder(RewardsActivity.this)
                            .setTarget(fragmentAwards7.btnCanjear)

                             .withRectangleShape()
                            .setContentText(getString(R.string.Helptext_Awards))
                            .setDismissText(getString(R.string.Helptext_Next))

                            .build());

                } else  if(fragmentAwards8.isResumed()&& fragmentAwards8.btnCanjear.getVisibility()==View.VISIBLE){
                    sequence.addSequenceItem(new MaterialShowcaseView.Builder(RewardsActivity.this)
                            .setTarget(fragmentAwards8.btnCanjear)

                            .withRectangleShape()
                            .setContentText(getString(R.string.Helptext_Awards))
                            .setDismissText(getString(R.string.Helptext_Next))

                            .build());

                }







                sequence.start();

            }
        });

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/


        final TabLayout[] tabs = {(TabLayout) findViewById(R.id.tabsStats)};


        tabs[0].addTab(tabs[0].newTab().setText("Diarios"),true);
        tabs[0].addTab(tabs[0].newTab().setText("Semanales"));

        final TabLayout[] tabs2 = {(TabLayout) findViewById(R.id.tabsStats2)};
        tabs2[0].addTab(tabs2[0].newTab().setText("Premio 1"),true);
        tabs2[0].addTab(tabs2[0].newTab().setText("Premio 2"));
        tabs2[0].addTab(tabs2[0].newTab().setText("Premio 3"));
        tabs2[0].addTab(tabs2[0].newTab().setText("Premio 4"));

        tabMainS="0";
        subtabS="0";
        tabs4 = tabMainS+subtabS;
        setCurrentTabFragment(tabs4);






        tabs[0].setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {


                tabMain=tab.getPosition();
                tabMainS= String.valueOf(tab.getPosition());
                tabs4 = tabMainS+subtabS;
                setCurrentTabFragment(tabs4);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });

        tabs2[0].setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                subtab=tab.getPosition();
                subtabS= String.valueOf(tab.getPosition());
                tabs4 = tabMainS+subtabS;

               setCurrentTabFragment(tabs4);

            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("email", email);
            intent.putExtra("name", name);
            intent.putExtra("photo", personPhoto);
            NavUtils.navigateUpTo(this, intent);
            return true;
        }




        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onTabSelected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void setCurrentTabFragment(String tabPosition)
    {


        switch (tabPosition)
        {
            case "00" :
                fragmentAwards = new FragmentAwards();
                replaceFragment(fragmentAwards);

                break;
            case "01" :
                fragmentAwards2 = new  FragmentAwards2();
                replaceFragment(fragmentAwards2);
                break;
            case "02" :
                fragmentAwards3 = new  FragmentAwards3();
                replaceFragment(fragmentAwards3);
                break;
            case "03" :
                fragmentAwards4 = new  FragmentAwards4();
                replaceFragment(fragmentAwards4);
                break;
            case "10" :
                fragmentAwards5 = new  FragmentAwards5();
                replaceFragment(fragmentAwards5);
                break;
            case "11" :
                fragmentAwards6 = new  FragmentAwards6();
                replaceFragment(fragmentAwards6);
                break;
            case "12" :
                fragmentAwards7 = new  FragmentAwards7();
                replaceFragment(fragmentAwards7);
                break;
            case "13" :
                fragmentAwards8 = new  FragmentAwards8();
                replaceFragment(fragmentAwards8);
                break;


        }
    }



    public void replaceFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

}
