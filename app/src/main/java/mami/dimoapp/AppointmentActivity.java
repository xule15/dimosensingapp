package mami.dimoapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.clans.fab.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.CustomRequest2;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.dialogs.CustomDialogAppointment;
import mami.dimoapp.model.Appointment;
import mami.dimoapp.model.Store;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class AppointmentActivity extends AppCompatActivity implements View.OnClickListener{

    private RecyclerView recView;
    private SwipeRefreshLayout swipeContainer;
    private List<Appointment> datos,datos2;
    private FloatingActionButton btnAppointment,btnHelp;
    FragmentManager fragmentManager = getSupportFragmentManager();
    private String name, email, personPhoto;
    private SimpleItemRecyclerViewAdapter adapter;
    private TextView tvAppointments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Citas Próximas");
        email=Store.get().getEmail();




        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        tvAppointments = (TextView)findViewById(R.id.tvAppointments);

        btnAppointment = (FloatingActionButton)findViewById(R.id.appointment_action);
        btnAppointment.setOnClickListener(this);
        fragmentManager = getSupportFragmentManager();

        getAppointmentRequest();

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer .setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAppointmentRequest();
                swipeContainer.setRefreshing(false);
            }
        });

    }

    public void setAdapter(final SimpleItemRecyclerViewAdapter adapter){
        //Setear propiedades RecyclerView
        recView = (RecyclerView) findViewById(R.id.appointmentList);
        recView.setHasFixedSize(true);
        recView.setAdapter(adapter);
        recView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false));
    }


    public void getAppointmentRequest(){
        String url= "http://aplicaciondimo.com/getAppointment.php?email="+Store.get().getEmail()+"&start="+System.currentTimeMillis();;


        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetAppointment","Response is: "+ jsonArray.toString());
                        datos = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject appointment = (JSONObject) jsonArray1.get(i);
                                    String description = (String) appointment.get("description");
                                    String place = (String) appointment.get("place");
                                    String title = (String) appointment.get("title");
                                    long timestamp = Long.parseLong(String.valueOf(appointment.get("date")));
                                    Appointment app = new Appointment(timestamp, title, place, description);
                                    setAlarm(app);
                                    datos.add(app);



                                }

                                Collections.sort(datos, new Comparator<Appointment>() {
                                    @Override
                                    public int compare(Appointment gp1, Appointment gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });

                                adapter = new SimpleItemRecyclerViewAdapter(datos);

                                setAdapter(adapter);
                            }else{
                                tvAppointments.setText("No existen próximas citas");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });

        Volley.newRequestQueue(getApplicationContext()).add(customRequest);
    }




    private void setAlarm(Appointment appointment){
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");
        notificationIntent.putExtra("description", appointment.getTitle()+ " "+ DateConvert.millisecondsToDateStringWithoutSeconds(appointment.getTime()));

        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, appointment.getTime(), broadcast); //1 hora antes
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            //NavUtils.navigateUpTo(this, intent);
            return true;
        }
        if(id == R.id.action_Appointment_help) {
            new MaterialShowcaseView.Builder(this)
                    .setTarget(btnAppointment)
                    .setDismissText(getString(R.string.Helptext_Next))
                    .setContentText(getString(R.string.Helptext_RegisterAppointment))
                    .show();


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_appointment, menu);

        //setBadgeCount(this, mCartMenuIcon, ""+ 1);
        return super.onCreateOptionsMenu(menu);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.appointment_action:
                CustomDialogAppointment dialogAppointment = new CustomDialogAppointment();
                dialogAppointment.show(fragmentManager,"AppointmentDialog");
                break;
            case R.id.action_Appointment_help:

                break;
        }

    }

    //Adapter para RecyclerView
    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private List<Appointment> datos;

        public SimpleItemRecyclerViewAdapter(List<Appointment> datos) {
            this.datos = datos;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_view_appointment, parent, false);
            ViewHolder tvh = new ViewHolder(view);
            return tvh;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.appointmentTitle.setText(datos.get(position).getTitle());
            holder.appointmentPlace.setText(datos.get(position).getPlace());
            holder.appointmentDescription.setText(datos.get(position).getDescription());
            holder.appointmentDate.setText(DateConvert.millisecondsToDateStringWithoutSeconds(datos.get(position).getTime()));
            holder.itemView.setTag(0);

        }

        //Conseguir tamaño del array
        @Override
        public int getItemCount() {
            return datos.size();
        }

        public class ViewHolder
                extends RecyclerView.ViewHolder {

            public final TextView appointmentTitle;
            public final TextView appointmentPlace;
            public final TextView appointmentDescription;
            public final TextView appointmentDate;
            public final RelativeLayout appointmentRl;

            public ViewHolder(View view) {
                super(view);
                appointmentTitle = (TextView)view.findViewById(R.id.AppointmentTitle);
                appointmentPlace = (TextView)view.findViewById(R.id.appointmentPlace);
                appointmentDescription = (TextView)view.findViewById(R.id.appointmentDescription);
                appointmentDate = (TextView)view.findViewById(R.id.appointmentDate);
                appointmentRl = (RelativeLayout) view.findViewById(R.id.appointmentRl);
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if(Store.get().getEmail()==null){


        }
    }
}
