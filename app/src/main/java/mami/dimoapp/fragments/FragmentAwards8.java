package mami.dimoapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.LineChart;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Date;

import mami.dimoapp.FoodActivity;
import mami.dimoapp.R;
import mami.dimoapp.model.Store;

public class FragmentAwards8 extends Fragment implements View.OnClickListener {
    public Button btnCanjear;
    public TextView availablePoints;
    public TextView tvNecessaryPoints,tvNecessaryPointsText;
    private TextView tvAwardName;
    private int weeklyPoints;
    private int totalPoints,idAward;
    private String awardName;
    private int neccesaryPoints, category;
    private LinearLayout linearMain;
    private String email;
    private String date;
    private String link;
    private ImageView imageAward;
    public FragmentAwards8() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_awards, container, false);
        btnCanjear = (Button) view.findViewById(R.id.buttonCanjear);

        weeklyPoints =Store.get().getweeklyPoints();

        email=Store.get().getEmail();
        category =Store.get().getCategoria();

        //Indica = bronce, 1 plata, 2 Oro
        if(category ==0) {
            link = Store.get().getLinkP4WeeklyBronze();
            neccesaryPoints = Store.get().getpWeeklyP4Bronze();
            awardName =Store.get().getDescriptionWeeklyP4Bronze();
            idAward=Store.get().getIdP4WeeklyBronze();


        }else if(category ==1){
            link = Store.get().getLinkP4WeeklySilver();
            neccesaryPoints = Store.get().getpWeeklyP4Silver();
            awardName =Store.get().getDescriptionWeeklyP4Silver();
            idAward=Store.get().getIdP4WeeklySilver();




        }else {
            link = Store.get().getLinkP4WeeklyGold();
            neccesaryPoints = Store.get().getpWeeklyP4Gold();
            awardName =Store.get().getDescriptionWeeklyP4Gold();
            idAward=Store.get().getIdP4WeeklyGold();

        }
        Date d = new Date();
        date = (String) DateFormat.format("dd/MM/yyyy", d.getTime());


        //btnCanjear.setOnClickListener(this);
        linearMain = (LinearLayout) view.findViewById(R.id.linearLayoutContent);
        availablePoints = (TextView) view.findViewById(R.id.puntosDisponibles);
        tvNecessaryPoints =(TextView) view.findViewById(R.id.puntosNecesarios1);
        tvNecessaryPointsText=(TextView) view.findViewById(R.id.puntosNecesarios);
        tvAwardName =(TextView) view.findViewById(R.id.NombrePremio);

        tvAwardName.setText(String.valueOf(awardName));
        tvNecessaryPoints.setText(String.valueOf(neccesaryPoints));



        imageAward= (ImageView) view.findViewById(R.id.imageAward);


        //cargar puntos de la BD
        if(weeklyPoints <0){
            availablePoints.setText(String.valueOf(0));
        } else {
            availablePoints.setText(String.valueOf(weeklyPoints));
        }

        btnCanjear.setOnClickListener(this);
        //Comprobar si ha sido canjeado, si lo ha sido ponerlo visible, si no se deja por defecto hasta que se pulse canjear
        if(Store.get().isExchange17() && Store.get().getCategoria()==0 ){

            tvAwardName.setVisibility(View.VISIBLE);
            linearMain.setVisibility(View.VISIBLE);
            btnCanjear.setVisibility(View.INVISIBLE); //oculta el button
            tvNecessaryPoints.setVisibility(View.INVISIBLE);
            tvNecessaryPointsText.setText(R.string.TextAwardExchangue);
            Glide.with(getActivity()).load(link).into(imageAward);


        } else if(Store.get().isExchange21()&& Store.get().getCategoria()==1){

            tvAwardName.setVisibility(View.VISIBLE);
            linearMain.setVisibility(View.VISIBLE);
            btnCanjear.setVisibility(View.INVISIBLE); //oculta el button
            tvNecessaryPoints.setVisibility(View.INVISIBLE);
            tvNecessaryPointsText.setText(R.string.TextAwardExchangue);
            Glide.with(getActivity()).load(link).into(imageAward);


        }else if(Store.get().isExchange25()&& Store.get().getCategoria()==2){

            tvAwardName.setVisibility(View.VISIBLE);
            linearMain.setVisibility(View.VISIBLE);
            btnCanjear.setVisibility(View.INVISIBLE); //oculta el button
            tvNecessaryPoints.setVisibility(View.INVISIBLE);
            tvNecessaryPointsText.setText(R.string.TextAwardExchangue);
            Glide.with(getActivity()).load(link).into(imageAward);


        }
        //



        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonCanjear:
                if(weeklyPoints >= neccesaryPoints) {
                    totalPoints = weeklyPoints - neccesaryPoints;
                    linearMain.setVisibility(View.VISIBLE);
                    Glide.with(getActivity()).load(link).into(imageAward);
                    makeRequestBD("http://aplicaciondimo.com/setExchange.php?idUser=" + email + "&idAward="+idAward+"&date="+date);
                    btnCanjear.setVisibility(View.GONE);
                    makeRequestBD("http://aplicaciondimo.com/updateUser.php?email="+email+"&dailyPoints="+ Store.get().getdailyPoints()+"&weeklyPoints="+ totalPoints);
                    availablePoints.setText(String.valueOf(totalPoints));
                    Store.get().setweeklyPoints(totalPoints);

                    switch (idAward){
                        case 17:
                            Store.get().setExchange17(true);

                            break;
                        case 21:
                            Store.get().setExchange21(true);

                            break;
                        case 25:
                            Store.get().setExchange25(true);

                            break;


                    }

                }else{
                    Toast.makeText(getActivity(), "No dispone de puntos suficientes", Toast.LENGTH_SHORT).show();
                }










        }
    }



    public void makeRequestBD(String URL){
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");
                if ( (response.length()>0)){
                    try {

                        JSONArray ja = new JSONArray(response);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

}

