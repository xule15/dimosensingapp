package mami.dimoapp.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import mami.dimoapp.R;
import mami.dimoapp.custom.CustomMarkerView;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.CustomRequest2;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.custom.MyValueFormatter;
import mami.dimoapp.model.GlucosePoint;
import mami.dimoapp.model.Store;

public class FragmentGlucose extends Fragment implements View.OnClickListener, OnChartValueSelectedListener {

    private EditText etInicio, etFin;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Button btnFind;
    private TextView textViewInfRange, tvContent;
    private LineChart chartGlucose;
    private LinearLayout linearMain;
    private IAxisValueFormatter xAxisFormatter;
    private CustomMarkerView mv;
    private RecyclerView recyclerViewGlucose;
    private List<GlucosePoint> datos,datosOrdenados;
    private Date dateStart;
    private long start, end;

    public FragmentGlucose() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_glucose, container, false);

        //Range Code
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        dateStart = new Date();

        start = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0);
        start = start-259200000L;
        end = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0)+86399999;

        btnFind = (Button)view.findViewById(R.id.buttonFind);
        btnFind.setOnClickListener(this);
        textViewInfRange = (TextView)view.findViewById(R.id.textViewInfRange);

        recyclerViewGlucose = (RecyclerView)view.findViewById(R.id.glucoseList);

        etInicio = (EditText)view.findViewById(R.id.editTextInicio);
        etInicio.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear+1,year))>=0){
                                    dateStart = DateConvert.valuesToDate(dayOfMonth,monthOfYear+1,year);
                                    start = DateConvert.valuesToMilliseconds(dayOfMonth,monthOfYear+1,year,0,0,0);
                                    etInicio.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                }else{
                                    Toast.makeText(getContext(),"La fecha de inicio deber ser antes de la actual", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYear, mMonth-1, mDay);
                dpd.show();
            }
        });

        etFin = (EditText)view.findViewById(R.id.editTextFin);
        etFin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(dateStart.compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear+1,year))<=0){
                                    etFin.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    end = DateConvert.valuesToMilliseconds(dayOfMonth,monthOfYear+1,year,23,59,59);
                                }else{
                                    Toast.makeText(getContext(),"La fecha de fin no puede ser menor que la de inicio", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYear, mMonth-1, mDay);
                dpd.show();
            }
        });

        iniciarFecha(view);

        linearMain = (LinearLayout)view.findViewById(R.id.linearLayoutContent);

        //Custom Marker
        mv = new CustomMarkerView(getContext(), R.layout.marker);
        tvContent = (TextView)mv.findViewById(R.id.tvContent);

        //Glucose - Calories Line Chart
        chartGlucose = (LineChart) view.findViewById(R.id.chartGlucose);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonFind:
                String sourceString = "Datos desde <b>"+ etInicio.getText()+"</b> hasta "+"<b>"+etFin.getText()+"</b>";
                if (Build.VERSION.SDK_INT >= 24) {
                    textViewInfRange.setText(Html.fromHtml(sourceString, Html.FROM_HTML_MODE_LEGACY));// for 24 api and more
                } else {
                    textViewInfRange.setText(Html.fromHtml(sourceString)); // or for older api
                }

                linearMain.setVisibility(View.VISIBLE);
                getChartGlucose();
                //getChartGlucose2();
        }
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected() {

    }

    public void getChartGlucose(){
        final ArrayList<Entry> entries = new ArrayList<Entry>();

        //String url = "http://IP/api/v2.0/glucose?"+startDate+"&"+endDate;
        String url = "http://aplicaciondimo.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;


        datos = new ArrayList<>();
        datosOrdenados = new ArrayList<>();

        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject glucose = (JSONObject) jsonArray1.get(i);
                                    int check = Integer.parseInt(String.valueOf(glucose.get("type")));
                                    float value = Float.parseFloat(String.valueOf(glucose.get("value")));
                                    long id = Long.parseLong(String.valueOf(glucose.get("date")));
                                    GlucosePoint gp = new GlucosePoint(id, value, check);
                                    datos.add(gp);
                                }


                               Collections.sort(datos, new Comparator<GlucosePoint>() {
                                    @Override public int compare(GlucosePoint gp2, GlucosePoint gp1) {
                                      return (int) (gp2.getTime()- gp1.getTime());
                                 }
                                });
                               Collections.reverse(datos);



                                //Inicializar RecyclerView
                                final FragmentGlucose.SimpleItemRecyclerViewAdapter adapter = new FragmentGlucose.SimpleItemRecyclerViewAdapter(datos);

                                //Setear propiedades RecyclerView
                                recyclerViewGlucose.setHasFixedSize(true);
                                recyclerViewGlucose.setAdapter(adapter);
                                recyclerViewGlucose.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));


                                for (int i = 0; i < datos.size(); i++) {
                                    //entries.add(new Entry(i+1, datos.get(i).getIndex(), datos.get(i)));
                                    float floatx = (float) datos.get(i).getTime();
                                    entries.add(new Entry(floatx, datos.get(i).getIndex(), datos.get(i)));
                                }
                              initChartGlu(chartGlucose, entries);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartGlu: ", error.toString() );

            }
        });

        // and finally add the request to the queue
        Volley.newRequestQueue(getContext()).add(customRequest);
    }

    public void getChartGlucose2(){

        datos = new ArrayList<>();
        final ArrayList<Entry> entries = new ArrayList<Entry>();

        int check = 5;
        float value = 70;
        long today =System.currentTimeMillis();



        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());

        String url = "http://aplicaciondimo.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");


                try {
                    JSONArray ja = new JSONArray(response);
                    if(ja.length()>0) {
                        int elemento=0;

                        for (int i=0; i < ja.length()/3; i++) {

                            Log.i("Longitud","longitud "+ja.length());

                            long fecha = ja.getLong(elemento);
                            int type= ja.getInt(elemento+1);
                            float value = (float) ja.getDouble(elemento+2);
                            GlucosePoint gp = new GlucosePoint(fecha, value, type);
                            datos.add(gp);

                            elemento+=3;
                        }



                        //Inicializar RecyclerView
                        final FragmentGlucose.SimpleItemRecyclerViewAdapter adapter = new FragmentGlucose.SimpleItemRecyclerViewAdapter(datosOrdenados);
                        //Setear propiedades RecyclerView
                        recyclerViewGlucose.setHasFixedSize(true);
                        recyclerViewGlucose.setAdapter(adapter);
                        recyclerViewGlucose.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));


                        for (int i = 0; i < datos.size(); i++) {
                            float floatx = (float) datos.get(i).getTime();
                            entries.add(new Entry(floatx, datos.get(i).getIndex(), datos.get(i)));
                        }
                        initChartGlu(chartGlucose, entries);
                    }





                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }




    public void initChartGlu(LineChart chart, ArrayList<Entry> entries){

Collections.reverse(entries);
        mv.setChartView(chart);
        chart.setMarker(mv);


        chart.getDescription().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        chart.setPinchZoom(true);


        LineDataSet dataSet = new LineDataSet(entries, "Glucemia");
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setColor(Color.RED);
        dataSet.setCircleColor(Color.RED);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);




        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setAxisMinimum(0f); // start at zero
        yAxis.setAxisMaximum(350f);

        //Glucose Limits
        if(Store.get().getMaxGlucose()!=0) {
            LimitLine ll = new LimitLine(Store.get().getMaxGlucose(), "MAX");
            LimitLine ll2 = new LimitLine(Store.get().getMinGlucose(), "MIN");
            ll.setLineColor(Color.BLUE);
            ll.setLineWidth(4f);
            ll.setTextColor(Color.BLUE);
            ll.setTextSize(12f);
            ll.enableDashedLine(10f, 10f, 0f);
            ll2.setLineColor(Color.BLUE);
            ll2.setLineWidth(4f);
            ll2.setTextColor(Color.BLUE);
            ll2.setTextSize(12f);
            ll2.enableDashedLine(10f, 10f, 0f);
            yAxis.addLimitLine(ll);
            yAxis.addLimitLine(ll2);
        }

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                GlucosePoint gp = (GlucosePoint) e.getData();
                if(gp.getWhen()==0) {
                    tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                            "\nAntes de comer");
                }else if(gp.getWhen()==1){
                    tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                            "\nDespués de comer");
                }else{
                    tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                            "\nOtros");
                }
            }
            @Override
            public void onNothingSelected() {
            }
        });
    }

    //Adapter para RecyclerView
    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<FragmentGlucose.SimpleItemRecyclerViewAdapter.ViewHolder> {

        private List<GlucosePoint> datos;

        public SimpleItemRecyclerViewAdapter(List<GlucosePoint> datos) {
            this.datos = datos;
        }

        @Override
        public FragmentGlucose.SimpleItemRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_view_glucose, parent, false);
            FragmentGlucose.SimpleItemRecyclerViewAdapter.ViewHolder tvh = new FragmentGlucose.SimpleItemRecyclerViewAdapter.ViewHolder(view);
            return tvh;
        }

        @Override
        public void onBindViewHolder(FragmentGlucose.SimpleItemRecyclerViewAdapter.ViewHolder holder, final int position) {
            holder.glucoseDate.setText(DateConvert.millisecondsToDateStringWithoutSeconds(datos.get(position).getTime()));
            holder.glucoseValue.setText(datos.get(position).getIndex()+ "");
            switch (datos.get(position).getWhen()){
                case 0:
                    holder.glucoseWhen.setText("Antes de comer");
                    break;
                case 1:
                    holder.glucoseWhen.setText("Después de comer");
                    break;
                case 2:
                    holder.glucoseWhen.setText("Otros");
                    break;
            }
            holder.itemView.setTag(0);
        }

        //Conseguir tamaño del array
        @Override
        public int getItemCount() {
            return datos.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            public final TextView glucoseDate;
            public final TextView glucoseValue;
            public final RelativeLayout glucoseRl;
            public final TextView glucoseWhen;

            public ViewHolder(View view) {
                super(view);

                glucoseDate = (TextView)view.findViewById(R.id.glucoseDate);
                glucoseValue = (TextView)view.findViewById(R.id.AppointmentTitle);
                glucoseWhen = (TextView)view.findViewById(R.id.textViewWhen);
                glucoseRl = (RelativeLayout) view.findViewById(R.id.glucoseRl);
            }
        }
    }

    /*void hideData(){
        linearMain.setVisibility(View.INVISIBLE);
        Toast.makeText(getActivity(),"No hay datos en esas fechas",Toast.LENGTH_LONG);
    }*/


    private void iniciarFecha(View view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etFin.setText(formatDate.format(c.getTime()));
        etInicio.setText(formatDate.format(start));
    }

    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

   /* @Override
    public void onResume() {
        super.onResume();
        if(Store.get().getEmail()==null){
            int1.putExtra("activity", "Stats");
            startActivity(int1);
            Toast.makeText(getContext(),"Conectando al servidor",Toast.LENGTH_SHORT).show();
        }
    }*/
}
