package mami.dimoapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import mami.dimoapp.RewardsActivity;
import mami.dimoapp.model.Store;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Date;

import mami.dimoapp.R;

public class FragmentAwards  extends Fragment implements View.OnClickListener {

    public Button btnCanjear;
    public TextView availablePoints;
    public TextView tvNecessaryPoints, tvNecessaryPointsText;
    private TextView tvAwardName;
    private int dailyPoints,idAward;
    private int totalPoints;
    private String awardName;
    private int neccesaryPoints, category;
    private LinearLayout linearMain;
    private String email;
    private String date;
    private String link;
    private ImageView imageAward;

    public FragmentAwards() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_awards, container, false);
        btnCanjear = (Button) view.findViewById(R.id.buttonCanjear);

        dailyPoints=Store.get().getdailyPoints();

        email=Store.get().getEmail();
        category =Store.get().getCategoria();

        //Indica = bronce, 1 plata, 2 Oro
        if(category ==0) {
            link = Store.get().getLinkP1DailyBronze();
            neccesaryPoints = Store.get().getpDailyP1Bronze();
            awardName =Store.get().getDescriptionDailyP1Bronze();
            idAward=Store.get().getIdP1DailyBronze();


        }else if(category ==1){
            neccesaryPoints = Store.get().getpDailyP1Silver();
            link = Store.get().getLinkP1DailySilver();
            awardName =Store.get().getDescriptionDailyP1Silver();
            idAward=Store.get().getIdP1DailySilver();




        }else {
            neccesaryPoints = Store.get().getpDailyP1Gold();
            link = Store.get().getLinkP1DailyGold();
            awardName =Store.get().getDescriptionDailyP1Gold();
            idAward=Store.get().getIdP1DailyGold();



        }
        Date d = new Date();
        date = (String) DateFormat.format("dd/MM/yyyy", d.getTime());




        linearMain = (LinearLayout) view.findViewById(R.id.linearLayoutContent);
        availablePoints = (TextView) view.findViewById(R.id.puntosDisponibles);
        tvNecessaryPoints =(TextView) view.findViewById(R.id.puntosNecesarios1);
        tvNecessaryPointsText=(TextView) view.findViewById(R.id.puntosNecesarios);
        tvAwardName =(TextView) view.findViewById(R.id.NombrePremio);

        tvAwardName.setText(String.valueOf(awardName));
        tvNecessaryPoints.setText(String.valueOf(neccesaryPoints));



       imageAward= (ImageView) view.findViewById(R.id.imageAward);

        //Glide.with(getActivity()).load(Store.get().getLinkP1DailyBronze()).into(imageAward);
        //Glide.with(getActivity()).load("https://uc05b3853839fce147007a7cac8e.previews.dropboxusercontent.com/p/thumb/AA-mb8CHYxn1MtYjCpshIxLORQAGgcqU6OpxcxtjNXum9KbWwZWNS6-Y9Bsk5coBFyM2tDQcT0coQIudGgubT_uf4atlW-nB-f1W9vR_mtBET56CC_eNbIk9vVy466PyPDc5a9cs9Y0xnBcQD5rsdWDic3pt9H9cD0nd44Pl0AjODXxX2KwiEN_uStX-OiZqSYb-teMXHxz0G-vGux279BlmRFARQ3ND9uacAlK1IkzJIrEoLxazGqcnui7FiJm7loX3d4Soae2yX78_KjiaJIaSecgNAZGuZPgUi3I1gmU7Un3ArA3F9nTX9mOoswC4FwlGa_cpd9sNRaPkkD1LSJf6VMm2Qgp1w239VyVMEz_9Ba0Q4GGG8I6x3qr2pY2-LDdNqDczT6LrpwkJxKit7Und/p.jpeg?fv_content=true&size_mode=5").into(imageAward);
       // Glide.with(getActivity()).load(link).into(imageAward);

        //cargar puntos de la BD
        if(dailyPoints <0){
            availablePoints.setText(String.valueOf(0));
        } else {
            availablePoints.setText(String.valueOf(dailyPoints));
        }

        btnCanjear.setOnClickListener(this);



        //Comprobar si ha sido canjeado, si lo ha sido ponerlo visible, si no se deja por defecto hasta que se pulse canjear
        if(Store.get().isExchange1() && Store.get().getCategoria()==0) {

            tvAwardName.setVisibility(View.VISIBLE);
            linearMain.setVisibility(View.VISIBLE);
            btnCanjear.setVisibility(View.INVISIBLE); //oculta el button
            tvNecessaryPoints.setVisibility(View.INVISIBLE);
            tvNecessaryPointsText.setText(R.string.TextAwardExchangue);


            Glide.with(getActivity()).load(link).into(imageAward);




        }
        else if(Store.get().isExchange5() && Store.get().getCategoria()==1) {

            tvAwardName.setVisibility(View.VISIBLE);
            linearMain.setVisibility(View.VISIBLE);
            btnCanjear.setVisibility(View.INVISIBLE); //oculta el button
            tvNecessaryPoints.setVisibility(View.INVISIBLE);
            tvNecessaryPointsText.setText(R.string.TextAwardExchangue);


            Glide.with(getActivity()).load(link).into(imageAward);






        }  else if(Store.get().isExchange10() && Store.get().getCategoria()==2  ) {

            tvAwardName.setVisibility(View.VISIBLE);
            linearMain.setVisibility(View.VISIBLE);
            btnCanjear.setVisibility(View.INVISIBLE); //oculta el button
            tvNecessaryPoints.setVisibility(View.INVISIBLE);
            tvNecessaryPointsText.setText(R.string.TextAwardExchangue);


            Glide.with(getActivity()).load(link).into(imageAward);



        }
        //



        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonCanjear:
                if(dailyPoints>= neccesaryPoints) {
                    totalPoints =dailyPoints- neccesaryPoints;
                    linearMain.setVisibility(View.VISIBLE);
                    Glide.with(getActivity()).load(link).into(imageAward);
                    makeRequestBD("http://aplicaciondimo.com/setExchange.php?idUser=" + email + "&idAward="+idAward+"&date="+date);
                    btnCanjear.setVisibility(View.GONE);
                    makeRequestBD("http://aplicaciondimo.com/updateUser.php?email="+email+"&dailyPoints="+ totalPoints);
                    availablePoints.setText(String.valueOf(totalPoints));
                    Store.get().setdailyPoints(totalPoints);
                    switch (idAward){
                        case 1:
                            Store.get().setExchange1(true);

                            break;
                        case 5:
                            Store.get().setExchange5(true);
                            break;
                        case 10:
                            Store.get().setExchange10(true);
                            break;


                    }





                }else{
                    Toast.makeText(getActivity(), "No dispone de puntos suficientes", Toast.LENGTH_SHORT).show();
                }


                //initChartActivity(chartActivity);
        }
    }



    public void makeRequestBD(String URL){
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");
                if ( (response.length()>0)){
                    try {

                        JSONArray ja = new JSONArray(response);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

}




