package mami.dimoapp.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import mami.dimoapp.R;
import mami.dimoapp.custom.CustomMarkerView;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.CustomRequest2;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.custom.MyValueFormatter;
import mami.dimoapp.custom.Tabla;
import mami.dimoapp.model.NutritionDay;
import mami.dimoapp.model.PointGeneric;
import mami.dimoapp.model.Store;

public class FragmentNutrition extends Fragment implements View.OnClickListener{
    private EditText etInicio, etFin;
    int mYear, mMonth, mDay, mHour, mMinute;
    private Button btnFind;
    private TextView textViewInfRange, tvContent;
    private Date dateStart;
    private LineChart chartCH, chartWeight, chartCalories;
    private LinearLayout linearMain;
    private CustomMarkerView mv;
    private IAxisValueFormatter xAxisFormatter;
    private Tabla tableNutrition;
    private long start, end;

    public FragmentNutrition() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_nutrition, container, false);

        //Range Code
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        dateStart = new Date();

        start = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0);
        start = start-259200000L;
        end = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0)+86399999;

        btnFind = (Button)view.findViewById(R.id.buttonFind);
        btnFind.setOnClickListener(this);
        textViewInfRange = (TextView)view.findViewById(R.id.textViewInfRange);

        //Custom Marker
        mv = new CustomMarkerView(getContext(), R.layout.marker);
        tvContent = (TextView)mv.findViewById(R.id.tvContent);

        chartCH = (LineChart)view.findViewById(R.id.chartHid);
        chartWeight = (LineChart)view.findViewById(R.id.chartWeight);
        chartCalories = (LineChart)view.findViewById(R.id.chartCalories);

        linearMain = (LinearLayout)view.findViewById(R.id.lLNutrition);

        tableNutrition = new Tabla(getActivity(), (TableLayout)view.findViewById(R.id.tableNutritionMain));

        etInicio = (EditText)view.findViewById(R.id.editTextInicio);
        etInicio.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear+1,year))>0){
                                    dateStart = DateConvert.valuesToDate(dayOfMonth,monthOfYear+1,year);
                                    start = DateConvert.valuesToMilliseconds(dayOfMonth,monthOfYear+1,year,0,0,0);
                                    etInicio.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                }else{
                                    Toast.makeText(getContext(),"La fecha de inicio deber ser antes de la actual", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }, mYear, mMonth-1, mDay);
                dpd.show();
            }
        });


        etFin = (EditText)view.findViewById(R.id.editTextFin);
        etFin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(dateStart.compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear+1,year))<=0){
                                    etFin.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    end = DateConvert.valuesToMilliseconds(dayOfMonth,monthOfYear+1,year,23,59,59);
                                }else{
                                    Toast.makeText(getContext(),"La fecha de fin no puede ser mayor que la de inicio", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }, mYear, mMonth-1, mDay);
                dpd.show();
            }
        });

        iniciarFecha(view);

        return view;

    }

    public void initTableNutrition(){
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        //String url = "http://IP/api/v2.0/activities?"+startDate+"&"+endDate;
        String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=39.476245,-0.349448&sensor=true";

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetTableNutrition","Response is: "+ jsonArray.toString());

                        NutritionDay rowDay = new NutritionDay(1484729131000L, 1500, 125, 110, 50, 35);
                        NutritionDay rowDay3 = new NutritionDay(1484840731000L, 1800, 135, 95, 70, 35);
                        NutritionDay rowDay4 = new NutritionDay(1484869531000L, 2100, 122, 98, 65, 35);

                        List<NutritionDay> datos = new ArrayList<>();
                        datos.add(rowDay);
                        datos.add(rowDay3);
                        datos.add(rowDay4);

                        tableNutrition.agregarCabecera(R.array.headerTable);
                        for(int i = 0; i < datos.size(); i++)
                        {
                            ArrayList<String> elementos = new ArrayList<String>();
                            elementos.add(DateConvert.millisecondsToDateStringWithoutHours(datos.get(i).getTime()));
                            elementos.add(datos.get(i).getKcal()+"");
                            elementos.add(datos.get(i).getcH()+"");
                            elementos.add(datos.get(i).getProt()+"");
                            elementos.add(datos.get(i).getFat()+"");
                            elementos.add(datos.get(i).getGluc()+"");
                            tableNutrition.agregarFilaTabla(elementos);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartNutrition: ", error.getMessage() );
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonFind:
                String sourceString = "Datos desde <b>"+ etInicio.getText()+"</b> hasta "+"<b>"+etFin.getText()+"</b>";
                if (Build.VERSION.SDK_INT >= 24) {
                    textViewInfRange.setText(Html.fromHtml(sourceString, Html.FROM_HTML_MODE_LEGACY));// for 24 api and more
                } else {
                    textViewInfRange.setText(Html.fromHtml(sourceString)); // or for older api
                }

                linearMain.setVisibility(View.VISIBLE);
                //initTableNutrition();
                getChartCH();
                getChartWeight();
                getChartCalories();
        }
    }

    public void getChartCH(){

        String url = "http://aplicaciondimo.com/getConsumptions.php?email="+Store.get().getEmail()+"&start="+start +"&end="+end;


        //Create the list of the cookies, i.e. key=value

        final ArrayList<Entry> entries = new ArrayList<>();

        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetChartCH","Response is: "+ jsonArray.toString());
                        ArrayList<PointGeneric> carbohidratesList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject consumption = (JSONObject) jsonArray1.get(i);
                                   // float carbohidrates=0;
                                    float carbohidrates = Float.parseFloat(String.valueOf(consumption.get("total-carbohydrates")));
                                    long id = Long.parseLong(String.valueOf(consumption.get("start")));
                                    PointGeneric pg = new PointGeneric(carbohidrates, id);
                                    carbohidratesList.add(pg);

                                }




                                for(int i=0; i< carbohidratesList.size();i++){
                                    float floatx = (float)carbohidratesList.get(i).getTime();
                                    entries.add(new Entry(floatx, carbohidratesList.get(i).getValue(), carbohidratesList.get(i)));
                                }
                                initChartCH(chartCH, entries);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("PIECHARTMAIN: ", error.toString() );
            }
        });


        Volley.newRequestQueue(getContext()).add(customRequest);
    }

    public void initChartCH(LineChart chart, ArrayList<Entry> entries){

        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.getDescription().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        chart.setPinchZoom(true);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                PointGeneric pg = (PointGeneric) e.getData();
                tvContent.setText("Carbohidratos: "+e.getY()+ " gr\n Fecha: "+DateConvert.millisecondsToDateStringWithoutHours(pg.getTime())+
                        "\nHora: "+DateConvert.millisecondsToDateStringWithoutDate(pg.getTime()));
            }

            @Override
            public void onNothingSelected() {
            }
        });

        LineDataSet dataSet = new LineDataSet(entries, "Carbohidratos");
        dataSet.setMode(LineDataSet.Mode.LINEAR);
        dataSet.setColor(Color.BLUE);
        dataSet.setCircleColor(Color.BLUE);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setAxisMinimum(0f); // start at zero
        yAxis.setAxisMaximum(200f);
    }

    public void getChartWeight(){
        final ArrayList<Entry> entries = new ArrayList<Entry>();
        String url = "http://aplicaciondimo.com/getWeight.php?email="+Store.get().getEmail()+"&start="+start +"&end="+end;
        //Create the list of the cookies, i.e. key=value

        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        ArrayList<PointGeneric> weightList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for(int i=0;i<jsonArray1.length();i++) {

                                    JSONObject weight = (JSONObject) jsonArray1.get(i);
                                    float value = Float.parseFloat(String.valueOf(weight.get("value")));
                                    long id = Long.parseLong(String.valueOf(weight.get("date")));
                                    PointGeneric pg = new PointGeneric(value, id);
                                    weightList.add(pg);
                                }

                                Collections.sort(weightList, new Comparator<PointGeneric>() {
                                    @Override
                                    public int compare(PointGeneric gp1, PointGeneric gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });

                                for (int i = 0; i < weightList.size(); i++) {
                                    //entries.add(new Entry(i+1, datos.get(i).getIndex(), datos.get(i)));
                                    float floatx = (float) weightList.get(i).getTime();
                                    entries.add(new Entry(floatx, weightList.get(i).getValue(), weightList.get(i)));
                                }

                                initChartWeight(chartWeight, entries);

                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartGlu: ", error.toString() );
            }
        });

        // and finally add the request to the queue
        Volley.newRequestQueue(getContext()).add(customRequest);
    }

    public void initChartWeight(LineChart chart, ArrayList<Entry> entries){

        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.getDescription().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        chart.setPinchZoom(true);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                PointGeneric pg = (PointGeneric)e.getData();
                tvContent.setText("Peso: "+e.getY()+ " kg\n Fecha: "+DateConvert.millisecondsToDateStringWithoutHours(pg.getTime())+
                        "\nHora: "+DateConvert.millisecondsToDateStringWithoutDate(pg.getTime()));
            }

            @Override
            public void onNothingSelected() {

            }
        });

        LineDataSet dataSet = new LineDataSet(entries, "Peso");
        dataSet.setMode(LineDataSet.Mode.LINEAR);
        dataSet.setColor(Color.BLACK);
        dataSet.setCircleColor(Color.BLACK);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setAxisMinimum(0f); // start at zero
        yAxis.setAxisMaximum(200f);

    }

    public void getChartCalories(){
        String url = "http://aplicaciondimo.com/getConsumptions.php?email="+Store.get().getEmail()+"&start="+start +"&end="+end;


        final ArrayList<Entry> entries = new ArrayList<>();

        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetChartCalories","Response is: "+ jsonArray.toString());
                        ArrayList<PointGeneric> caloriesList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject consumption = (JSONObject) jsonArray1.get(i);

                                    float carbohidrates = Float.parseFloat(String.valueOf(consumption.get("total-calories")));
                                    long id = Long.parseLong(String.valueOf(consumption.get("start")));
                                    PointGeneric pg = new PointGeneric(carbohidrates, id);
                                    caloriesList.add(pg);
                                }

                                Collections.sort(caloriesList, new Comparator<PointGeneric>() {
                                    @Override
                                    public int compare(PointGeneric gp1, PointGeneric gp2) {
                                        return (int)(gp1.getTime() - gp2.getTime());
                                    }
                                });

                                for(int i=0; i<caloriesList.size();i++){
                                    float floatx = (float)caloriesList.get(i).getTime();
                                    entries.add(new Entry(floatx, caloriesList.get(i).getValue(), caloriesList.get(i)));
                                }
                                initChartCalories(chartCalories, entries);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        Volley.newRequestQueue(getContext()).add(customRequest);
    }

    public void initChartCalories(LineChart chart, ArrayList<Entry> entries){

        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.getDescription().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        chart.setPinchZoom(true);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                PointGeneric pg = (PointGeneric) e.getData();
                tvContent.setText("Calorias: "+e.getY()+ "\n Fecha: "+DateConvert.millisecondsToDateStringWithoutHours(pg.getTime())+
                        "\nHora: "+DateConvert.millisecondsToDateStringWithoutDate(pg.getTime()));
            }

            @Override
            public void onNothingSelected() {

            }
        });

        LineDataSet dataSet = new LineDataSet(entries, "Calorias");
        dataSet.setMode(LineDataSet.Mode.LINEAR);
        dataSet.setColor(Color.YELLOW);
        dataSet.setCircleColor(Color.YELLOW);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setAxisMinimum(0f); // start at zero
        yAxis.setAxisMaximum(3500f);


    }

    private void iniciarFecha(View view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etFin.setText(formatDate.format(c.getTime()));
        etInicio.setText(formatDate.format(start));
    }


}
