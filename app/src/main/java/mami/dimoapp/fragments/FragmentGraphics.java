package mami.dimoapp.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import mami.dimoapp.MainActivity;
import mami.dimoapp.R;
import mami.dimoapp.custom.CustomMarkerView;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.CustomRequest2;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.custom.MyValueFormatter;
import mami.dimoapp.model.CHPoint;
import mami.dimoapp.model.GlucosePoint;
import mami.dimoapp.model.InsulinePoint;
import mami.dimoapp.model.PhysicalActivity;
import mami.dimoapp.model.PointGeneric;
import mami.dimoapp.model.PointGeneric2;
import mami.dimoapp.model.Store;

public class FragmentGraphics extends Fragment implements View.OnClickListener {

    private Spinner spinnerLeft, spinnerRight, spinnerGraphic;
    private EditText etInicio, etFin;
    int mYear, mMonth, mDay, mHour, mMinute;
    private Button btnFind;
    private TextView textViewInfRange,textViewInfRange2,textViewInfRange3, tvContent;
    private LineChart chartGluCal, chartCalHid, chartGlucoseHeart,chartGluMeals, chartCalWeight, chartGluIns, chartGluHid, chartGluAct;
    private PieChart pieChart;
    private LinearLayout linearMain;
    private IAxisValueFormatter xAxisFormatter;
    private CustomMarkerView mv;
    private Date dateStart;
    private long start, end;
    private CardView cvGlucoseCal, cvGlucoseHid, cvCalHid, cvCalWeight, cvGluIns, cvGluMeals, cvGlucoseHeart, cvPieChart, cvGluAct;

    public FragmentGraphics() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_graphic, container, false);

        cvGlucoseCal = (CardView)view.findViewById(R.id.cvGlucoseCal);
        cvGlucoseHid = (CardView)view.findViewById(R.id.cvGluHid);
        cvGlucoseHeart = (CardView)view.findViewById(R.id.cvGlucoseHeart);
        cvCalHid = (CardView)view.findViewById(R.id.cvCalHid);
        cvCalWeight = (CardView)view.findViewById(R.id.cvCalWeight);
        cvGluIns = (CardView)view.findViewById(R.id.cvGluIns);
        cvGluMeals = (CardView)view.findViewById(R.id.cvGluMeals);
        cvPieChart = (CardView)view.findViewById(R.id.cvPieChart);
        cvGluAct = (CardView)view.findViewById(R.id.cvGluAct);

        etInicio = (EditText)view.findViewById(R.id.editTextInicio);
        etFin = (EditText)view.findViewById(R.id.editTextFin);

        //Range Code
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        dateStart = new Date();

        start = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0);
        start = start-259200000L;
        end = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0)+86399999;

        btnFind = (Button)view.findViewById(R.id.buttonFind);
        btnFind.setOnClickListener(this);
        textViewInfRange = (TextView)view.findViewById(R.id.textViewInfRange);

        etInicio.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear,year))>0){
                                    dateStart = DateConvert.valuesToDate(dayOfMonth,monthOfYear,year);
                                    start = DateConvert.valuesToMilliseconds(dayOfMonth,monthOfYear+1,year,0,0,0);
                                    etInicio.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                }else{
                                    Toast.makeText(getContext(),"La fecha de inicio deber ser antes de la actual", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }, mYear, mMonth-1, mDay);
                dpd.show();
            }
        });

        etFin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(dateStart.compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear+1,year))<=0){
                                    etFin.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    end = DateConvert.valuesToMilliseconds(dayOfMonth,monthOfYear+1,year,23,59,59);
                                }else{
                                    Toast.makeText(getContext(),"La fecha de fin no puede ser mayor que la de inicio", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }, mYear, mMonth-1, mDay);
                dpd.show();
            }
        });

        iniciarFecha(view);


        spinnerGraphic = (Spinner) view.findViewById(R.id.spinnerGraphic);

        initSpinnerGraphic(spinnerGraphic);

        linearMain = (LinearLayout)view.findViewById(R.id.linearLayoutContent);

        //Custom Marker
        mv = new CustomMarkerView(getContext(), R.layout.marker);
        tvContent = (TextView)mv.findViewById(R.id.tvContent);

        //Glucose - Calories Line Chart
        chartGluCal = (LineChart) view.findViewById(R.id.chartGluCal);

        //Glucose - Carbohidrates Chart
        chartGluHid = (LineChart)view.findViewById(R.id.chartGluHid);

        //Glucose - Heart Rate Line Chart
        chartGlucoseHeart = (LineChart) view.findViewById(R.id.chartGlucoseHeart);
        mv.setChartView(chartGlucoseHeart);
        chartGlucoseHeart.setMarker(mv);

        //Calories- Carbohidrates Chart
        chartCalHid = (LineChart)view.findViewById(R.id.chartCalHid);
        mv.setChartView(chartCalHid);
        chartCalHid.setMarker(mv);

        //Calories - Weight Chart
        chartCalWeight = (LineChart) view.findViewById(R.id.chartCalWeight);

        //Glucose - Insulin Chart
        chartGluIns = (LineChart) view.findViewById(R.id.chartGluIns);

        chartGluMeals = (LineChart) view.findViewById(R.id.chartGluMeals);

        pieChart = (PieChart)view.findViewById(R.id.piechartCH);

        chartGluAct = (LineChart)view.findViewById(R.id.chartGluAct);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonFind:
                String sourceString = "Datos desde <b>"+ etInicio.getText()+"</b> hasta "+"<b>"+etFin.getText()+"</b>";
                if (Build.VERSION.SDK_INT >= 24) {
                    textViewInfRange.setText(Html.fromHtml(sourceString, Html.FROM_HTML_MODE_LEGACY));// for 24 api and more
                } else {
                    textViewInfRange.setText(Html.fromHtml(sourceString)); // or for older api
                }
                cvGlucoseCal.setVisibility(View.GONE);
                cvGlucoseHid.setVisibility(View.GONE);
                cvGlucoseHeart.setVisibility(View.GONE);
                cvGluIns.setVisibility(View.GONE);
                cvGluMeals.setVisibility(View.GONE);
                cvCalWeight.setVisibility(View.GONE);
                cvCalHid.setVisibility(View.GONE);
                cvPieChart.setVisibility(View.GONE);
                cvGluAct.setVisibility(View.GONE);

                if(spinnerGraphic.getSelectedItem().toString().equals("Glucemia - Calorias ingeridas")){
                    getChartGluCal();
                    cvGlucoseCal.setVisibility(View.VISIBLE);
                }
                if(spinnerGraphic.getSelectedItem().toString().equals("Glucemia - Carbohidratos")){
                    getChartGluHid();
                    cvGlucoseHid.setVisibility(View.VISIBLE);
                }
                if(spinnerGraphic.getSelectedItem().toString().equals("Glucemia - Ritmo Cardiaco")){
                    getChartGluHeart();

                    cvGlucoseHeart.setVisibility(View.VISIBLE);

                }
                if(spinnerGraphic.getSelectedItem().toString().equals("Glucemia - Insulina")){
                    getChartGluIns();
                    cvGluIns.setVisibility(View.VISIBLE);
                }
                if(spinnerGraphic.getSelectedItem().toString().equals("Glucemia - Comidas")){
                    getChartGluMeals();
                    cvGluMeals.setVisibility(View.VISIBLE);
                }
                if(spinnerGraphic.getSelectedItem().toString().equals("Calorias- Carbohidratos")){
                    getChartCalHid();
                    cvCalHid.setVisibility(View.VISIBLE);
                }
                if(spinnerGraphic.getSelectedItem().toString().equals("Calorias ingeridas - Peso")){
                    getChartCalWeigth();
                    cvCalWeight.setVisibility(View.VISIBLE);
                }
                if(spinnerGraphic.getSelectedItem().toString().equals("Carbohidratos según comidas")){
                    getPieChartHid();
                    cvPieChart.setVisibility(View.VISIBLE);
                }

        }
    }

    public void initSpinnerGraphic(Spinner spinner){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.graphicsTypes, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    public void getChartGluCal(){
        String urlGlucose = "http://aplicaciondimo.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;
        String urlCal = "http://aplicaciondimo.com/getConsumptions.php?email="+Store.get().getEmail()+"&start="+start +"&end="+end;

        final ArrayList<Entry> entries = new ArrayList<>();
        final ArrayList<Entry> entries2 = new ArrayList<>();

        final CustomRequest2 customRequest2 = new CustomRequest2(Request.Method.GET, urlCal,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetChartGluCal","Response is: "+ jsonArray.toString());
                        ArrayList<PointGeneric> caloriesList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject consumption = (JSONObject) jsonArray1.get(i);
                                    float calories = Float.parseFloat(String.valueOf(consumption.get("total-calories")));

                                    long id = Long.parseLong(String.valueOf(consumption.get("start")));
                                    PointGeneric pg = new PointGeneric(calories, id);
                                    caloriesList.add(pg);
                                }

                                Collections.sort(caloriesList, new Comparator<PointGeneric>() {
                                    @Override
                                    public int compare(PointGeneric gp1, PointGeneric gp2) {
                                        return (int)(gp1.getTime() - gp2.getTime());
                                    }
                                });

                                for(int i=0; i<caloriesList.size();i++){
                                    float floatx = (float)caloriesList.get(i).getTime();
                                    entries2.add(new Entry(floatx, caloriesList.get(i).getValue(), caloriesList.get(i)));
                                }
                                initChartGluCal(chartGluCal, entries, entries2);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("PIECHARTMAIN: ", error.toString() );
            }
        });

        CustomRequest customRequest1 = new CustomRequest(Request.Method.GET, urlGlucose,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        ArrayList<GlucosePoint> glucoseList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject glucose = (JSONObject) jsonArray1.get(i);
                                    int check = Integer.parseInt(String.valueOf(glucose.get("type")));
                                    float value = Float.parseFloat(String.valueOf(glucose.get("value")));
                                    long id = Long.parseLong(String.valueOf(glucose.get("date")));
                                    GlucosePoint gp = new GlucosePoint(id, value, check);
                                    glucoseList.add(gp);
                                }

                                Collections.sort(glucoseList, new Comparator<GlucosePoint>() {
                                    @Override
                                    public int compare(GlucosePoint gp1, GlucosePoint gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });


                                for (int i = 0; i < glucoseList.size(); i++) {

                                    float floatx = (float) glucoseList.get(i).getTime();
                                    entries.add(new Entry(floatx, glucoseList.get(i).getIndex(), glucoseList.get(i)));
                                }
                                Volley.newRequestQueue(getContext()).add(customRequest2);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartGlu: ", error.toString() );
            }

        });


        Volley.newRequestQueue(getContext()).add(customRequest1);

    }

    public void initChartGluCal(LineChart chart, List<Entry> entries, List<Entry> entries2 ){

        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.getDescription().setEnabled(false);
        chart.setPinchZoom(true);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if(e.getData().getClass().equals(GlucosePoint.class)){
                    GlucosePoint gp = (GlucosePoint) e.getData();
                    if(gp.getWhen()==0) {
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nAntes de comer");
                    }else if(gp.getWhen()==1){
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nDespués de comer");
                    }else{
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nOtros");
                    }
                }else{
                    PointGeneric pg = (PointGeneric) e.getData();
                    tvContent.setText("Calorias: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(pg.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(pg.getTime()));
                }
            }

            @Override
            public void onNothingSelected() {}
        });

        LineDataSet dataSet = new LineDataSet(entries, "Glucemia");
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setColor(Color.RED);
        dataSet.setCircleColor(Color.RED);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        LineDataSet dataSet2 = new LineDataSet(entries2, "Calorías");
        dataSet2.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet2.setColor(Color.YELLOW);
        dataSet2.setCircleColor(Color.YELLOW);
        dataSet2.setCircleRadius(5f);
        dataSet2.setValueTextSize(12f);
        dataSet2.setLineWidth(3);
        dataSet2.setAxisDependency(YAxis.AxisDependency.RIGHT);
        dataSet2.setHighlightEnabled(true);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);
        dataSets.add(dataSet2);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setAxisMinimum(0f); // start at zero
        leftAxis.setAxisMaximum(350f);

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setAxisMinimum(0f); // start at zero
        rightAxis.setAxisMaximum(3500f);

        //Glucose Limits
        if(Store.get().getMaxGlucose()!=0) {
            LimitLine ll = new LimitLine(Store.get().getMaxGlucose(), "MAX");
            LimitLine ll2 = new LimitLine(Store.get().getMinGlucose(), "MIN");
            ll.setLineColor(Color.BLUE);
            ll.setLineWidth(4f);
            ll.setTextColor(Color.BLUE);
            ll.setTextSize(12f);
            ll.enableDashedLine(10f, 10f, 0f);
            ll2.setLineColor(Color.BLUE);
            ll2.setLineWidth(4f);
            ll2.setTextColor(Color.BLUE);
            ll2.setTextSize(12f);
            ll2.enableDashedLine(10f, 10f, 0f);
            leftAxis.addLimitLine(ll);
            leftAxis.addLimitLine(ll2);
        }
    }






    public void getChartGluHid(){
        String urlGlucose = "http://aplicaciondimo.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;
        String urlHid = "http://aplicaciondimo.com/getConsumptions.php?email="+Store.get().getEmail()+"&start="+start +"&end="+end;


        final ArrayList<Entry> entries = new ArrayList<>();
        final ArrayList<Entry> entries2 = new ArrayList<>();

        final CustomRequest2 customRequest2 = new CustomRequest2(Request.Method.GET, urlHid,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetChartGluCal","Response is: "+ jsonArray.toString());
                        ArrayList<PointGeneric> chList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject consumption = (JSONObject) jsonArray1.get(i);
                                    float ch = Float.parseFloat(String.valueOf(consumption.get("total-carbohydrates")));
                                    long id = Long.parseLong(String.valueOf(consumption.get("start")));
                                    PointGeneric pg = new PointGeneric(ch, id);
                                    chList.add(pg);
                                }

                                Collections.sort(chList, new Comparator<PointGeneric>() {
                                    @Override
                                    public int compare(PointGeneric gp1, PointGeneric gp2) {
                                        return (int)(gp1.getTime() - gp2.getTime());
                                    }
                                });

                                for(int i=0; i<chList.size();i++){
                                    float floatx = (float)chList.get(i).getTime();
                                    entries2.add(new Entry(floatx, chList.get(i).getValue(), chList.get(i)));
                                }
                                initChartGluHid(chartGluHid, entries, entries2);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("PIECHARTMAIN: ", error.toString() );
            }
        });

        CustomRequest customRequest1 = new CustomRequest(Request.Method.GET, urlGlucose,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        ArrayList<GlucosePoint> glucoseList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject glucose = (JSONObject) jsonArray1.get(i);
                                    int check = Integer.parseInt(String.valueOf(glucose.get("type")));
                                    float value = Float.parseFloat(String.valueOf(glucose.get("value")));
                                    long id = Long.parseLong(String.valueOf(glucose.get("date")));
                                    GlucosePoint gp = new GlucosePoint(id, value, check);
                                    glucoseList.add(gp);
                                }

                                Collections.sort(glucoseList, new Comparator<GlucosePoint>() {
                                    @Override
                                    public int compare(GlucosePoint gp1, GlucosePoint gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });


                                for (int i = 0; i < glucoseList.size(); i++) {
                                    float floatx = (float) glucoseList.get(i).getTime();
                                    entries.add(new Entry(floatx, glucoseList.get(i).getIndex(), glucoseList.get(i)));
                                }
                                Volley.newRequestQueue(getContext()).add(customRequest2);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartGlu: ", error.toString() );
            }

        });

        // Add the request to the RequestQueue.
        Volley.newRequestQueue(getContext()).add(customRequest1);
    }

    public void initChartGluHid(LineChart chart, List<Entry> entries, List<Entry> entries2){
        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.getDescription().setEnabled(false);
        chart.setPinchZoom(true);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if(e.getData().getClass().equals(GlucosePoint.class)){
                    GlucosePoint gp = (GlucosePoint) e.getData();
                    if(gp.getWhen()==0) {
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nAntes de comer");
                    }else if(gp.getWhen()==1){
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nDespués de comer");
                    }else{
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nOtros");
                    }
                }else{
                    PointGeneric pg = (PointGeneric) e.getData();
                    tvContent.setText("Carbohidratos: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(pg.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(pg.getTime()));
                }
            }

            @Override
            public void onNothingSelected() {}
        });


        LineDataSet dataSet = new LineDataSet(entries, "Glucemia");
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setColor(Color.RED);
        dataSet.setCircleColor(Color.RED);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setCircleRadius(5f);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        LineDataSet dataSet2 = new LineDataSet(entries2, "Hidratos de Carbono");
        dataSet2.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet2.setColor(Color.BLUE);
        dataSet2.setCircleColor(Color.BLUE);
        dataSet2.setLineWidth(3);
        dataSet2.setCircleRadius(5f);
        dataSet2.setValueTextSize(12f);
        dataSet2.setAxisDependency(YAxis.AxisDependency.RIGHT);
        dataSet2.setHighlightEnabled(true);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);
        dataSets.add(dataSet2);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(350f);

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setAxisMinimum(0f);
        rightAxis.setAxisMaximum(350f);

        //Glucose Limits
        if(Store.get().getMaxGlucose()!=0) {
            LimitLine ll = new LimitLine(Store.get().getMaxGlucose(), "MAX");
            LimitLine ll2 = new LimitLine(Store.get().getMinGlucose(), "MIN");
            ll.setLineColor(Color.BLUE);
            ll.setLineWidth(4f);
            ll.setTextColor(Color.BLUE);
            ll.setTextSize(12f);
            ll.enableDashedLine(10f, 10f, 0f);
            ll2.setLineColor(Color.BLUE);
            ll2.setLineWidth(4f);
            ll2.setTextColor(Color.BLUE);
            ll2.setTextSize(12f);
            ll2.enableDashedLine(10f, 10f, 0f);
            yAxis.addLimitLine(ll);
            yAxis.addLimitLine(ll2);
        }
    }



    public void getChartGluHeart(){

        // Instantiate the RequestQueue.
        String urlGlucose = "http://aplicaciondimo.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;
        String urlHeart = "http://aplicaciondimo.com/getHeartRate.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;


        final ArrayList<Entry> entries = new ArrayList<>();
        final ArrayList<Entry> entries2 = new ArrayList<>();

        final CustomRequest2 customRequest2 = new CustomRequest2(Request.Method.GET, urlHeart,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {


                            // Display the first 500 characters of the response string.
                            ArrayList<PointGeneric> heartList = new ArrayList<>();
                            try {

                                JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                                if (jsonArray1.length() > 0) {
                                    for (int i = 0; i < jsonArray1.length(); i++) {
                                        JSONObject heart = (JSONObject) jsonArray1.get(i);
                                        float value = Float.parseFloat(String.valueOf(heart.get("value")));
                                        long id = Long.parseLong(String.valueOf(heart.get("date")));
                                        PointGeneric pg = new PointGeneric(value, id);
                                        heartList.add(pg);
                                    }

                                    Collections.sort(heartList, new Comparator<PointGeneric>() {
                                        @Override
                                        public int compare(PointGeneric gp1, PointGeneric gp2) {
                                            return (int) (gp1.getTime() - gp2.getTime());
                                        }
                                    });

                                    for (int i = 0; i < heartList.size(); i++) {
                                        float floatx = (float) heartList.get(i).getTime();
                                        entries2.add(new Entry(floatx, heartList.get(i).getValue(), heartList.get(i)));
                                    }
                                    initChartGlucoseHeart(chartGlucoseHeart, entries, entries2);
                                } else {
                                    textViewInfRange.setText("No existen datos suficientes");
                                }

                            } catch (JSONException e) {

                                e.printStackTrace();
                            }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("PIECHARTMAIN: ", error.toString() );
                Log.e("DentroError","DentroError");
                textViewInfRange.setText("No existen datos suficientes");
                cvGlucoseHeart.setVisibility(View.INVISIBLE);




            }
        });

        CustomRequest2 customRequest1 = new CustomRequest2(Request.Method.GET, urlGlucose,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        ArrayList<GlucosePoint> glucoseList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");

                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {

                                    JSONObject glucose = (JSONObject) jsonArray1.get(i);
                                    int check = Integer.parseInt(String.valueOf(glucose.get("type")));
                                    float value = Float.parseFloat(String.valueOf(glucose.get("value")));
                                    long id = Long.parseLong(String.valueOf(glucose.get("date")));
                                    GlucosePoint gp = new GlucosePoint(id, value, check);
                                    glucoseList.add(gp);
                                }

                                Collections.sort(glucoseList, new Comparator<GlucosePoint>() {
                                    @Override
                                    public int compare(GlucosePoint gp1, GlucosePoint gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });

                                for (int i = 0; i < glucoseList.size(); i++) {
                                    float floatx = (float) glucoseList.get(i).getTime();
                                    entries.add(new Entry(floatx, glucoseList.get(i).getIndex(), glucoseList.get(i)));
                                }
                                Volley.newRequestQueue(getContext()).add(customRequest2);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                textViewInfRange.setText("No existen datos suficientes");
                cvGlucoseHeart.setVisibility(View.INVISIBLE);
            }
        });



        Volley.newRequestQueue(getContext()).add(customRequest1);
    }

    public void initChartGlucoseHeart(LineChart chart, List<Entry> entries, List<Entry> entries2){
        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.getDescription().setEnabled(false);
        chart.setPinchZoom(true);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if(e.getData().getClass().equals(GlucosePoint.class)){
                    GlucosePoint gp = (GlucosePoint) e.getData();
                    if(gp.getWhen()==0) {
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nAntes de comer");
                    }else if(gp.getWhen()==1){
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nDespués de comer");
                    }else{
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nOtros");
                    }
                }else{
                    PointGeneric pg = (PointGeneric) e.getData();
                    tvContent.setText("Pulsaciones: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(pg.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(pg.getTime()));
                }
            }

            @Override
            public void onNothingSelected() {}
        });

        LineDataSet dataSet = new LineDataSet(entries, "Glucemia");
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setColor(Color.RED);
        dataSet.setCircleColor(Color.RED);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        LineDataSet dataSet2 = new LineDataSet(entries2, "Ritmo Cardiaco");
        dataSet2.setColor(Color.WHITE);
        dataSet2.setCircleColor(Color.GREEN);
        dataSet2.setCircleColorHole(Color.GREEN);
        dataSet2.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet2.setCircleRadius(5f);
        dataSet2.setValueTextSize(12f);
        dataSet2.setLineWidth(3);
        dataSet2.setAxisDependency(YAxis.AxisDependency.RIGHT);
        dataSet2.setHighlightEnabled(true);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);
        dataSets.add(dataSet2);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setAxisMinimum(0f); // start at zero
        yAxis.setAxisMaximum(350f);

        YAxis yAxisRight = chart.getAxisRight();
        yAxisRight.setAxisMinimum(0f); // start at zero
        yAxisRight.setAxisMaximum(200f);

        //Glucose Limits
        if(Store.get().getMaxGlucose()!=0) {
            LimitLine ll = new LimitLine(100, "MAX");
            LimitLine ll2 = new LimitLine(Store.get().getMinGlucose(), "MIN");
            ll.setLineColor(Color.BLUE);
            ll.setLineWidth(4f);
            ll.setTextColor(Color.BLUE);
            ll.setTextSize(12f);
            ll.enableDashedLine(10f, 10f, 0f);
            ll2.setLineColor(Color.BLUE);
            ll2.setLineWidth(4f);
            ll2.setTextColor(Color.BLUE);
            ll2.setTextSize(12f);
            ll2.enableDashedLine(10f, 10f, 0f);
            yAxis.addLimitLine(ll);
            yAxis.addLimitLine(ll2);
        }

        //Customize legend
        Legend l = chart.getLegend();
        LegendEntry le = new LegendEntry();
        LegendEntry le2 = new LegendEntry();
        le.label = "Glucemia";
        le.formColor = Color.RED;
        le2.label = "Ritmo cardiaco";
        le2.formColor = Color.GREEN;
        ArrayList<LegendEntry> legendEntries = new ArrayList();
        legendEntries.add(le);
        legendEntries.add(le2);
        l.setCustom(legendEntries);
    }

    public void getChartCalHid(){
        // Instantiate the RequestQueue.


        String urlHid = "http://aplicaciondimo.com/getConsumptions.php?email="+Store.get().getEmail()+"&start="+start +"&end="+end;
        String urlCalIng = "http://aplicaciondimo.com/getConsumptions.php?email="+Store.get().getEmail()+"&start="+start +"&end="+end;




        final ArrayList<Entry> entries = new ArrayList<>();
        final ArrayList<Entry> entries3 = new ArrayList<>();

        final CustomRequest customRequest3 = new CustomRequest(Request.Method.GET, urlHid,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetChartCalHid","Response is: "+ jsonArray.toString());
                        ArrayList<PointGeneric> caloriesList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject consumption = (JSONObject) jsonArray1.get(i);
                                    float calories = Float.parseFloat(String.valueOf(consumption.get("total-carbohydrates")));
                                    long id = Long.parseLong(String.valueOf(consumption.get("start")));
                                    PointGeneric pg = new PointGeneric(calories, id);
                                    caloriesList.add(pg);
                                }

                                Collections.sort(caloriesList, new Comparator<PointGeneric>() {
                                    @Override
                                    public int compare(PointGeneric gp1, PointGeneric gp2) {
                                        return (int)(gp1.getTime() - gp2.getTime());
                                    }
                                });

                                for(int i=0; i<caloriesList.size();i++){
                                    float floatx = (float)caloriesList.get(i).getTime();
                                    entries3.add(new Entry(floatx, caloriesList.get(i).getValue(), caloriesList.get(i)));
                                }
                                initChartCalHid(chartCalHid, entries, entries3);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORChartCalHid: ", error.toString() );
            }
        });


        CustomRequest customRequest1 = new CustomRequest(Request.Method.GET, urlCalIng,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetChartCalHid","Response is: "+ jsonArray.toString());
                        ArrayList<PointGeneric> caloriesList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject consumption = (JSONObject) jsonArray1.get(i);
                                    float calories = Float.parseFloat(String.valueOf(consumption.get("total-calories")));
                                    long id = Long.parseLong(String.valueOf(consumption.get("start")));
                                    PointGeneric pg = new PointGeneric(calories, id);
                                    caloriesList.add(pg);
                                }

                                Collections.sort(caloriesList, new Comparator<PointGeneric>() {
                                    @Override
                                    public int compare(PointGeneric gp1, PointGeneric gp2) {
                                        return (int)(gp1.getTime() - gp2.getTime());
                                    }
                                });

                                for(int i=0; i<caloriesList.size();i++){
                                    float floatx = (float)caloriesList.get(i).getTime();
                                    entries.add(new Entry(floatx, caloriesList.get(i).getValue(), caloriesList.get(i)));
                                }
                                Volley.newRequestQueue(getContext()).add(customRequest3);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORChartCalHid: ", error.toString() );
            }
        });

        Volley.newRequestQueue(getContext()).add(customRequest1);



    }

    public void initChartCalHid(LineChart chart, List<Entry> entries, List<Entry> entries3){
        chart.getDescription().setEnabled(false);
        chart.setPinchZoom(true);

        mv.setChartView(chart);
        chart.setMarker(mv);

        LineDataSet dataSet = new LineDataSet(entries, "Calorías Ingeridas");
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setColor(Color.YELLOW);
        dataSet.setCircleColor(Color.YELLOW);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);



        LineDataSet dataSet3 = new LineDataSet(entries3, "Carbohidratos");
        dataSet3.setColor(Color.BLUE);
        dataSet3.setCircleColor(Color.BLUE);
        dataSet3.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet3.setValueTextSize(12f);
        dataSet3.setLineWidth(3);
        dataSet3.setAxisDependency(YAxis.AxisDependency.RIGHT);
        dataSet3.setHighlightEnabled(true);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);
        dataSets.add(dataSet3);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setAxisMinimum(0f); // start at zero
        yAxis.setAxisMaximum(3000f);

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setAxisMinimum(0f); // start at zero
        rightAxis.setAxisMaximum(300f);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if(e.getData().getClass().equals(PointGeneric.class)){
                    PointGeneric pg = (PointGeneric) e.getData();
                    tvContent.setText("Carbohidratos: " + pg.getValue() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(pg.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(pg.getTime()));
                }else if(e.getData().getClass().equals(PointGeneric2.class)){
                    PointGeneric2 pg = (PointGeneric2) e.getData();
                    tvContent.setText("Calorias ingeridas: " + pg.getValue() + " kg.\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(pg.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(pg.getTime()));
                }
            }

            @Override
            public void onNothingSelected() {}
        });
    }

    public void getChartGluIns(){
        // Instantiate the RequestQueue.
        String urlGlucose = "http://aplicaciondimo.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;
        String urlInsulin ="http://aplicaciondimo.com/getInsuline.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;



        final ArrayList<Entry> entries = new ArrayList<>();
        final ArrayList<Entry> entries2 = new ArrayList<>();

        final CustomRequest customRequest2 = new CustomRequest(Request.Method.GET, urlInsulin,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        ArrayList<InsulinePoint> insulinList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0 ) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject insulin = (JSONObject) jsonArray1.get(i);
                                    int type = Integer.parseInt(String.valueOf(insulin.get("type")));
                                    float dose = Float.parseFloat(String.valueOf(insulin.get("value")));
                                    long id = Long.parseLong(String.valueOf(insulin.get("date")));
                                    InsulinePoint ip = new InsulinePoint(id, dose, type);
                                    insulinList.add(ip);
                                }

                                Collections.sort(insulinList, new Comparator<InsulinePoint>() {
                                    @Override
                                    public int compare(InsulinePoint gp1, InsulinePoint gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });


                                for (int i = 0; i < insulinList.size(); i++) {
                                    //entries.add(new Entry(i+1, datos.get(i).getIndex(), datos.get(i)));
                                    float floatx = (float) insulinList.get(i).getTime();
                                    entries2.add(new Entry(floatx, insulinList.get(i).getDose(), insulinList.get(i)));
                                }
                                initChartGluIns(chartGluIns, entries, entries2);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("EERORChartGluIns: ", error.toString() );
                textViewInfRange.setText("No existen datos suficientes");
                cvGluIns.setVisibility(View.INVISIBLE);
            }
        });

        CustomRequest customRequest1 = new CustomRequest(Request.Method.GET, urlGlucose,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.i("BIEN","Response is: "+ jsonArray.toString());
                        ArrayList<GlucosePoint> glucoseList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject glucose = (JSONObject) jsonArray1.get(i);
                                    int check = Integer.parseInt(String.valueOf(glucose.get("type")));
                                    float value = Float.parseFloat(String.valueOf(glucose.get("value")));
                                    long id = Long.parseLong(String.valueOf(glucose.get("date")));
                                    GlucosePoint gp = new GlucosePoint(id, value, check);
                                    glucoseList.add(gp);
                                }

                                Collections.sort(glucoseList, new Comparator<GlucosePoint>() {
                                    @Override
                                    public int compare(GlucosePoint gp1, GlucosePoint gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });
                                for (int i = 0; i < glucoseList.size(); i++) {
                                    float floatx = (float) glucoseList.get(i).getTime();
                                    entries.add(new Entry(floatx, glucoseList.get(i).getIndex(), glucoseList.get(i)));
                                }
                                Volley.newRequestQueue(getContext()).add(customRequest2);

                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("EERORChartGluIns: ", error.toString() );
                textViewInfRange.setText("No existen datos suficientes");
                cvGluIns.setVisibility(View.INVISIBLE);


            }
        });



        Volley.newRequestQueue(getContext()).add(customRequest1);
    }



    public void initChartGluIns(LineChart chart,List<Entry> entries, List<Entry> entries2){
        chart.getDescription().setEnabled(false);
        chart.setPinchZoom(true);

        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if(e.getData().getClass().equals(GlucosePoint.class)){
                    GlucosePoint gp = (GlucosePoint) e.getData();
                    if(gp.getWhen()==0) {
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nAntes de comer");
                    }else if(gp.getWhen()==1){
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nDespués de comer");
                    }else{
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nOtros");
                    }
                }else{
                    InsulinePoint ip = (InsulinePoint) e.getData();
                    if(ip.getType()==0) {
                        tvContent.setText("Dosis: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(ip.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(ip.getTime()) +
                                "\nTipo: Rápida");
                    }else if(ip.getType()==1){
                        tvContent.setText("Dosis: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(ip.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(ip.getTime()) +
                                "\nTipo: Corta");
                    }else if(ip.getType()==2){
                        tvContent.setText("Dosis: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(ip.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(ip.getTime()) +
                                "\nTipo: Intermedia");
                    }else {
                        tvContent.setText("Dosis: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(ip.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(ip.getTime()) +
                                "\nTipo: Larga");
                    }
                }
            }

            @Override
            public void onNothingSelected() {}
        });

        LineDataSet dataSet = new LineDataSet(entries, "Glucose");
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setColor(Color.RED);
        dataSet.setCircleColor(Color.RED);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        LineDataSet dataSet2 = new LineDataSet(entries2, "Insulina");
        dataSet2.setColor(Color.WHITE);
        dataSet2.setCircleColor(Color.BLUE);
        dataSet2.setCircleRadius(5f);
        dataSet2.setValueTextSize(12f);
        dataSet2.setAxisDependency(YAxis.AxisDependency.RIGHT);
        dataSet2.setHighlightEnabled(true);
        dataSet2.setDrawValues(false);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);
        dataSets.add(dataSet2);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        chart.setData(data);

        chart.invalidate();
        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis yAxisLeft = chart.getAxisLeft();
        yAxisLeft.setAxisMinimum(0f); // start at zero
        yAxisLeft.setAxisMaximum(350f);

        YAxis yAxisRight = chart.getAxisRight();
        yAxisRight.setAxisMinimum(0f); // start at zero
        yAxisRight.setAxisMaximum(10f);

        //Glucose Limits
        if(Store.get().getMaxGlucose()!=0) {
            LimitLine ll = new LimitLine(Store.get().getMaxGlucose(), "MAX");
            LimitLine ll2 = new LimitLine(Store.get().getMinGlucose(), "MIN");
            ll.setLineColor(Color.BLUE);
            ll.setLineWidth(4f);
            ll.setTextColor(Color.BLUE);
            ll.setTextSize(12f);
            ll.enableDashedLine(10f, 10f, 0f);
            ll2.setLineColor(Color.BLUE);
            ll2.setLineWidth(4f);
            ll2.setTextColor(Color.BLUE);
            ll2.setTextSize(12f);
            ll2.enableDashedLine(10f, 10f, 0f);
            yAxisLeft.addLimitLine(ll);
            yAxisLeft.addLimitLine(ll2);
        }

        //Customize legend
        Legend l = chart.getLegend();
        LegendEntry le = new LegendEntry();
        LegendEntry le2 = new LegendEntry();
        le.label = "Glucemia";
        le.formColor = Color.RED;
        le2.label = "Insuline";
        le2.formColor = Color.BLUE;
        ArrayList<LegendEntry> legendEntries = new ArrayList();
        legendEntries.add(le);
        legendEntries.add(le2);
        l.setCustom(legendEntries);
    }

    public void getChartGluMeals(){
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=39.476245,-0.349448&sensor=true";

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetChartGluIns","Response is: "+ jsonArray.toString());

                        // creating list of entry
                        ArrayList<Entry> entries = new ArrayList<>();
                        entries.add(new Entry(0, 150f));
                        entries.add(new Entry(1, 200f));
                        entries.add(new Entry(2, 210f));
                        entries.add(new Entry(3, 190f));
                        entries.add(new Entry(4, 210f));
                        entries.add(new Entry(5, 180f));
                        entries.add(new Entry(6, 220f));

                        ArrayList<Entry> entries2 = new ArrayList<Entry>();
                        entries2.add(new Entry(0, 75f));
                        entries2.add(new Entry(1, 77f));
                        entries2.add(new Entry(2, 76f));
                        entries2.add(new Entry(3, 74f));
                        entries2.add(new Entry(4, 77f));
                        entries2.add(new Entry(5, 76f));
                        entries2.add(new Entry(6, 75f));

                        initChartGluMeals(chartGluMeals, entries, entries2);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("getChartGluInsMAL: ", error.toString() );
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void initChartGluMeals(LineChart chart, List<Entry> entries, List<Entry> entries2){
        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.getDescription().setEnabled(false);
        chart.setPinchZoom(true);

        LineDataSet dataSet = new LineDataSet(entries, "Glucose");
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setColor(Color.RED);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        LineDataSet dataSet2 = new LineDataSet(entries2, "Comida");
        dataSet2.setColor(Color.WHITE);
        dataSet2.setCircleColor(Color.BLUE);
        dataSet2.setCircleColorHole(Color.BLUE);
        dataSet2.setCircleRadius(5f);
        dataSet2.setValueTextSize(12f);
        dataSet2.setAxisDependency(YAxis.AxisDependency.RIGHT);
        dataSet2.setHighlightEnabled(true);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);
        dataSets.add(dataSet2);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis yAxisLeft = chart.getAxisLeft();
        yAxisLeft.setAxisMinimum(0f); // start at zero
        yAxisLeft.setAxisMaximum(350f);

        YAxis yAxisRight = chart.getAxisRight();
        yAxisRight.setAxisMinimum(0f); // start at zero
        yAxisRight.setAxisMaximum(200f);

        //Glucose Limits
        if(Store.get().getMaxGlucose()!=0) {
            LimitLine ll = new LimitLine(Store.get().getMaxGlucose(), "MAX");
            LimitLine ll2 = new LimitLine(Store.get().getMinGlucose(), "MIN");
            ll.setLineColor(Color.BLUE);
            ll.setLineWidth(4f);
            ll.setTextColor(Color.BLUE);
            ll.setTextSize(12f);
            ll.enableDashedLine(10f, 10f, 0f);
            ll2.setLineColor(Color.BLUE);
            ll2.setLineWidth(4f);
            ll2.setTextColor(Color.BLUE);
            ll2.setTextSize(12f);
            ll2.enableDashedLine(10f, 10f, 0f);
            yAxisLeft.addLimitLine(ll);
            yAxisLeft.addLimitLine(ll2);
        }
    }

    public void getChartCalWeigth(){
        // Instantiate the RequestQueue.
        String urlCal = "http://aplicaciondimo.com/getConsumptions.php?email="+Store.get().getEmail()+"&start="+start +"&end="+end;
        String urlWeight = "http://aplicaciondimo.com/getWeight.php?email="+Store.get().getEmail()+"&start="+start +"&end="+end;




        final ArrayList<Entry> entries = new ArrayList<>();
        final ArrayList<Entry> entries2 = new ArrayList<>();

        final CustomRequest customRequest2 = new CustomRequest(Request.Method.GET, urlWeight,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        ArrayList<PointGeneric2> weightList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for(int i=0;i<jsonArray1.length();i++) {
                                    JSONObject weight = (JSONObject) jsonArray1.get(i);
                                    float value = Float.parseFloat(String.valueOf(weight.get("value")));
                                    long id = Long.parseLong(String.valueOf(weight.get("date")));
                                    PointGeneric2 pg = new PointGeneric2(value, id);
                                    weightList.add(pg);
                                }

                                Collections.sort(weightList, new Comparator<PointGeneric2>() {
                                    @Override
                                    public int compare(PointGeneric2 gp1, PointGeneric2 gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });

                                for (int i = 0; i < weightList.size(); i++) {
                                    float floatx = (float) weightList.get(i).getTime();
                                    entries2.add(new Entry(floatx, weightList.get(i).getValue(), weightList.get(i)));
                                }

                                initChartCalWeight(chartCalWeight, entries, entries2);

                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartGlu: ", error.toString() );
            }
        });

        CustomRequest customRequest1 = new CustomRequest(Request.Method.GET, urlCal,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetChartGluCal","Response is: "+ jsonArray.toString());
                        ArrayList<PointGeneric> caloriesList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject consumption = (JSONObject) jsonArray1.get(i);
                                    float calories = Float.parseFloat(String.valueOf(consumption.get("total-calories")));
                                    long id = Long.parseLong(String.valueOf(consumption.get("start")));
                                    PointGeneric pg = new PointGeneric(calories, id);
                                    caloriesList.add(pg);
                                }

                                Collections.sort(caloriesList, new Comparator<PointGeneric>() {
                                    @Override
                                    public int compare(PointGeneric gp1, PointGeneric gp2) {
                                        return (int)(gp1.getTime() - gp2.getTime());
                                    }
                                });

                                for(int i=0; i<caloriesList.size();i++){
                                    float floatx = (float)caloriesList.get(i).getTime();
                                    entries.add(new Entry(floatx, caloriesList.get(i).getValue(), caloriesList.get(i)));
                                }
                                Volley.newRequestQueue(getContext()).add(customRequest2);

                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("PIECHARTMAIN: ", error.toString() );
            }
        });

        Volley.newRequestQueue(getContext()).add(customRequest1);

    }

    public void initChartCalWeight(LineChart chart, ArrayList<Entry> entries, ArrayList<Entry> entries2 ){
        chart.getDescription().setEnabled(false);
        chart.setPinchZoom(true);

        mv.setChartView(chart);
        chart.setMarker(mv);

        LineDataSet dataSet = new LineDataSet(entries, "Calories");
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setColor(Color.YELLOW);
        dataSet.setCircleColor(Color.YELLOW);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        LineDataSet dataSet2 = new LineDataSet(entries2, "Peso");
        dataSet2.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet2.setColor(Color.BLACK);
        dataSet2.setCircleColor(Color.BLACK);
        dataSet2.setCircleRadius(5f);
        dataSet2.setValueTextSize(12f);
        dataSet2.setLineWidth(3);
        dataSet2.setAxisDependency(YAxis.AxisDependency.RIGHT);
        dataSet2.setHighlightEnabled(true);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);
        dataSets.add(dataSet2);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        chart.setData(data);

        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis yAxisLeft = chart.getAxisLeft();
        yAxisLeft.setAxisMinimum(0f); // start at zero
        yAxisLeft.setAxisMaximum(3500f);

        YAxis yAxisRight = chart.getAxisRight();
        yAxisRight.setAxisMinimum(0f); // start at zero
        yAxisRight.setAxisMaximum(200f);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if(e.getData().getClass().equals(PointGeneric.class)){
                    PointGeneric pg = (PointGeneric) e.getData();
                    tvContent.setText("Calorias: " + pg.getValue() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(pg.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(pg.getTime()));
                }else{
                    PointGeneric2 pg = (PointGeneric2) e.getData();
                    tvContent.setText("Peso: " + pg.getValue() + " kg.\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(pg.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(pg.getTime()));
                }
            }

            @Override
            public void onNothingSelected() {}
        });
    }


    public void initChartGluAct(LineChart chart, List<Entry> entries, List<Entry> entries2 ){

        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.getDescription().setEnabled(false);
        chart.setPinchZoom(true);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if(e.getData().getClass().equals(GlucosePoint.class)){
                    GlucosePoint gp = (GlucosePoint) e.getData();
                    if(gp.getWhen()==0) {
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nAntes de comer");
                    }else if(gp.getWhen()==1){
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nDespués de comer");
                    }else{
                        tvContent.setText("Glucemia: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                                "\nOtros");
                    }
                }else{
                    PhysicalActivity p = (PhysicalActivity)e.getData();
                    if(p.getTipo()==0){
                        tvContent.setText("Actividad: Andar"+"\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(p.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(p.getTime()) +
                                "\nDuración: "+(p.getDuration()/1000000000)/60+" minutos");
                    }else if(p.getTipo()==1){
                        tvContent.setText("Actividad: Correr"+ "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(p.getTime()) +
                                "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(p.getTime()) +
                                "\nDuración: "+(p.getDuration()/1000000000)/60+" minutos");
                    }else{
                        tvContent.setText("Actividad: Parado");
                    }
                }
            }

            @Override
            public void onNothingSelected() {}
        });

        LineDataSet dataSet = new LineDataSet(entries, "Glucemia");
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setColor(Color.RED);
        dataSet.setCircleColor(Color.RED);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        LineDataSet dataSet2 = new LineDataSet(entries2, "Actividad Física");
        dataSet2.setMode(LineDataSet.Mode.LINEAR);
        dataSet2.setColor(Color.GREEN);
        dataSet2.setCircleColor(Color.GREEN);
        dataSet2.setCircleRadius(5f);
        dataSet2.setValueTextSize(12f);
        dataSet2.setLineWidth(3);
        dataSet2.setAxisDependency(YAxis.AxisDependency.RIGHT);
        dataSet2.setHighlightEnabled(true);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);
        dataSets.add(dataSet2);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setAxisMinimum(0f); // start at zero
        leftAxis.setAxisMaximum(350f);

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setAxisMinimum(0f); // start at zero
        rightAxis.setAxisMaximum(2f);

        //Glucose Limits
        if(Store.get().getMaxGlucose()!=0) {
            LimitLine ll = new LimitLine(Store.get().getMaxGlucose(), "MAX");
            LimitLine ll2 = new LimitLine(Store.get().getMinGlucose(), "MIN");
            ll.setLineColor(Color.BLUE);
            ll.setLineWidth(4f);
            ll.setTextColor(Color.BLUE);
            ll.setTextSize(12f);
            ll.enableDashedLine(10f, 10f, 0f);
            ll2.setLineColor(Color.BLUE);
            ll2.setLineWidth(4f);
            ll2.setTextColor(Color.BLUE);
            ll2.setTextSize(12f);
            ll2.enableDashedLine(10f, 10f, 0f);
            leftAxis.addLimitLine(ll);
            leftAxis.addLimitLine(ll2);
        }
    }

    public void getPieChartHid(){
        String urlPieChart = "http://aplicaciondimo.com/getConsumptions.php?email="+Store.get().getEmail()+"&start="+start +"&end="+end;
        final List<PieEntry> entries = new ArrayList<>();


        final ArrayList<CHPoint> carbohidratesList = new ArrayList<>();
        final CHPoint chpBreakfast = new CHPoint(0,0);
        final CHPoint chpMidday = new CHPoint(0,1);
        final CHPoint chpLunch = new CHPoint(0,2);
        final CHPoint chpTea = new CHPoint(0,3);
        final CHPoint chpDinner = new CHPoint(0,4);
        final CHPoint chpSupper = new CHPoint(0,5);

        carbohidratesList.add(chpBreakfast);
        carbohidratesList.add(chpMidday);
        carbohidratesList.add(chpLunch);
        carbohidratesList.add(chpTea);
        carbohidratesList.add(chpDinner);
        carbohidratesList.add(chpSupper);

        CustomRequest customRequest = new CustomRequest(Request.Method.GET, urlPieChart,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetPieChart","Response is: "+ jsonArray.toString());
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject consumption = (JSONObject) jsonArray1.get(i);
                                    float carbohidrates = Float.parseFloat(String.valueOf(consumption.get("total-carbohydrates")));
                                    int when = Integer.parseInt(String.valueOf(consumption.get("when")));
                                    if(when==0) chpBreakfast.setValue(chpBreakfast.getValue()+carbohidrates);
                                    if(when==1) chpMidday.setValue(chpMidday.getValue()+carbohidrates);
                                    if(when==2) chpLunch.setValue(chpLunch.getValue()+carbohidrates);
                                    if(when==3) chpTea.setValue(chpTea.getValue()+carbohidrates);
                                    if(when==4) chpDinner.setValue(chpDinner.getValue()+carbohidrates);
                                    if(when==5) chpSupper.setValue(chpSupper.getValue()+carbohidrates);
                                }

                                float totalCH = (chpBreakfast.getValue()+chpMidday.getValue()+chpLunch.getValue()+chpTea.getValue()+chpDinner.getValue()+chpSupper.getValue());

                                carbohidratesList.add(chpBreakfast);
                                carbohidratesList.add(chpMidday);
                                carbohidratesList.add(chpLunch);
                                carbohidratesList.add(chpTea);
                                carbohidratesList.add(chpDinner);
                                carbohidratesList.add(chpSupper);

                                if(chpBreakfast.getValue()>0)entries.add(new PieEntry(chpBreakfast.getValue()/totalCH, "Desayuno", chpBreakfast));
                                if(chpMidday.getValue()>0)entries.add(new PieEntry(chpMidday.getValue()/totalCH, "Almuerzo", chpMidday));
                                if(chpLunch.getValue()>0)entries.add(new PieEntry(chpLunch.getValue()/totalCH, "Comida", chpLunch));
                                if(chpTea.getValue()>0)entries.add(new PieEntry(chpTea.getValue()/totalCH, "Merienda", chpTea));
                                if(chpDinner.getValue()>0)entries.add(new PieEntry(chpDinner.getValue()/totalCH, "Cena", chpDinner));
                                if(chpSupper.getValue()>0) entries.add(new PieEntry(chpSupper.getValue() / totalCH, "Cena ligera", chpSupper));

                                initPieChartHid(pieChart, entries);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("PIECHARTMAIN: ", error.toString() );
            }
        });


        Volley.newRequestQueue(getContext()).add(customRequest);
    }

    public void initPieChartHid (PieChart chart, List<PieEntry> entries){
        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.setUsePercentValues(true);
        PieDataSet set = new PieDataSet(entries, "");
        set.setValueTextSize(13f);
        set.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData data = new PieData(set);
        data.setValueFormatter(new PercentFormatter());
        chart.getDescription().setEnabled(false);
        chart.setData(data);
        chart.animateY(2000);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                CHPoint chp = (CHPoint) e.getData();
                if(chp.getWhen()==0) {
                    tvContent.setText("Raciones: " + chp.getValue()/10 + "\n Desayuno");
                }else if(chp.getWhen()==1){
                    tvContent.setText("Raciones: " + chp.getValue()/10 + "\n Almuerzo");
                }else if (chp.getWhen()==2){
                    tvContent.setText("Raciones: " + chp.getValue()/10 + "\n Comida");
                }else if(chp.getWhen()==3){
                    tvContent.setText("Raciones: " + chp.getValue()/10 + "\n Merienda");
                }else if(chp.getWhen()==4){
                    tvContent.setText("Raciones: " + chp.getValue()/10 + "\n Cena");
                }else{
                    tvContent.setText("Raciones: " + chp.getValue()/10 + "\n Cena ligera");
                }
            }

            @Override
            public void onNothingSelected() {}
        });
    }

    private void iniciarFecha(View view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etFin.setText(formatDate.format(c.getTime()));
        etInicio.setText(formatDate.format(start));
    }

    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    /*void hideData(){
        linearMain.setVisibility(View.INVISIBLE);
        Toast.makeText(getContext(),"No hay datos en esas fechas",Toast.LENGTH_LONG);
    }*/

    @Override
    public void onResume() {
        super.onResume();
        if(Store.get().getEmail()==null){
            Intent int1 =  new Intent(getContext(), MainActivity.class);
            int1.putExtra("activity", "Stats");
            startActivity(int1);
            Toast.makeText(getContext(),"Conectando al servidor",Toast.LENGTH_SHORT).show();
        }
    }

}
