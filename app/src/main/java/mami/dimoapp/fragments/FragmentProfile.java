package mami.dimoapp.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mami.dimoapp.MainActivity;
import mami.dimoapp.R;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.model.Store;

public class FragmentProfile extends Fragment implements View.OnClickListener {

    private LinearLayout llName, llSurname, llMail, llCountry, llBirthday, llGender, llWeight, llHeight, llDebut, llControlRange;
    public static TextView nameEdit;
    static TextView birthdayEdit;
    static TextView genderEdit;
    static TextView countryEdit;
    static TextView weightEdit;
    static TextView heightEdit;
    static TextView surnameEdit;
    static TextView mailEdit;
    static TextView debutEdit;
    static TextView textViewControlRange,npMin,npMax;
    private int yearBirthday, monthBirthday, dayBirthday, yearDebut, monthDebut, dayDebut;
    private Calendar c;
    private DatePickerDialog dpd;
    NumberPicker np, npKg, npGr;
    private SwipeRefreshLayout swipeContainer;
    private Toolbar toolbar;
    ActionBar actionBar;
    private Spinner spinnerCountry;
    private TextView tvName, tvMail;
    public Button buttonOk;
    private String name, surname, country, gender;
    private long birthday, diabeticDebut;
    private float weightValue, heightValue, minGlucose, maxGlucose;
    private boolean updateRange, updateWeight, updateHeight, updateBirthday, updateCountry, updateDebut;


    public FragmentProfile() {
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        c = Calendar.getInstance();
        nameEdit = (TextView) view.findViewById(R.id.textViewNameEdit);
        surnameEdit = (TextView) view.findViewById(R.id.textViewSurnameEdit);
        birthdayEdit = (TextView) view.findViewById(R.id.textViewBirthday);
        genderEdit = (TextView) view.findViewById(R.id.textViewGender);
        weightEdit = (TextView) view.findViewById(R.id.textViewWeight);
        heightEdit = (TextView) view.findViewById(R.id.textViewHeight);
        mailEdit = (TextView) view.findViewById(R.id.textViewEmailEdit);
        countryEdit = (TextView) view.findViewById(R.id.textViewCountryEdit);
        debutEdit = (TextView) view.findViewById(R.id.textViewDebut);
        textViewControlRange = (TextView) view.findViewById(R.id.textViewControlRange);
        buttonOk = (Button) view.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(this);

        llName = (LinearLayout) view.findViewById(R.id.llName);
        llName.setOnClickListener(this);
        llSurname = (LinearLayout) view.findViewById(R.id.llSurname);
        llSurname.setOnClickListener(this);
        llMail = (LinearLayout) view.findViewById(R.id.llMail);
        llMail.setOnClickListener(this);
        llCountry = (LinearLayout) view.findViewById(R.id.llCountry);
        llCountry.setOnClickListener(this);
        llGender = (LinearLayout) view.findViewById(R.id.llGender);
        llGender.setOnClickListener(this);
        llBirthday = (LinearLayout) view.findViewById(R.id.llBirthday);
        llBirthday.setOnClickListener(this);
        llWeight = (LinearLayout) view.findViewById(R.id.llWeight);
        llWeight.setOnClickListener(this);
        llHeight = (LinearLayout) view.findViewById(R.id.llHeight);
        llHeight.setOnClickListener(this);
        llDebut = (LinearLayout) view.findViewById(R.id.llDebut);
        llDebut.setOnClickListener(this);
        llControlRange = (LinearLayout) view.findViewById(R.id.llControlRange);
        llControlRange.setOnClickListener(this);


        updateRange = false;
        updateWeight = false;
        updateHeight = false;
        updateCountry = false;
        updateBirthday = false;
        updateDebut = false;

        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshProfile();
                swipeContainer.setRefreshing(false);
            }
        });

        llControlRange.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(getContext());

                alert.setTitle("Selecciona tu rango de glucemia: ");

                final View viewRange = inflater.inflate(R.layout.range_picker2, null);

                npMin =  viewRange.findViewById(R.id.npMin);
                npMax =  viewRange.findViewById(R.id.npMax);


                NumberPicker.OnValueChangeListener myValChangedListener = new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        Log.i("value is", "" + newVal);
                    }
                };

                alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                            if (Integer.parseInt(npMin.getText().toString())<= Integer.parseInt(npMax.getText().toString())) {
                            textViewControlRange.setText(npMin.getText().toString() + " - " + npMax.getText().toString());
                            minGlucose = Float.parseFloat(npMin.getText().toString());
                            maxGlucose = Float.parseFloat(npMax.getText().toString());
                            updateRange = true;
                        } else {
                            Toast.makeText(getContext(), "El valor mínimo no puede ser mayor o igual que el máximo", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                alert.setView(viewRange);
                alert.show();
            }
        });

        llWeight.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(getContext());

                alert.setTitle("Selecciona tu peso: ");

                final View viewWeightNp = inflater.inflate(R.layout.weight_picker, null);

                npKg = (NumberPicker) viewWeightNp.findViewById(R.id.npkg);
                npGr = (NumberPicker) viewWeightNp.findViewById(R.id.npgr);

                npKg.setMinValue(15);
                npKg.setMaxValue(200);
                npKg.setWrapSelectorWheel(false);
                npKg.setValue(70);

                npGr.setMinValue(0);
                npGr.setMaxValue(99);
                npGr.setWrapSelectorWheel(false);
                npGr.setValue(0);

                NumberPicker.OnValueChangeListener myValChangedListener = new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        Log.i("value is", "" + newVal);
                    }
                };

                alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value;
                        if (npGr.getValue() < 10) {
                            value = "0" + npGr.getValue();
                        } else {
                            value = "" + npGr.getValue();
                        }
                        weightValue = Float.parseFloat(npKg.getValue() + "." + value + "f");
                        weightEdit.setText(npKg.getValue() + "." + value);
                        updateWeight = true;
                    }
                });

                alert.setView(viewWeightNp);
                alert.show();
            }
        });

        llCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final android.app.AlertDialog.Builder alertDialogCountry = new android.app.AlertDialog.Builder(getContext());
                alertDialogCountry.setTitle("Selecciona el país: ");

                final View viewCountryPicker = inflater.inflate(R.layout.country_picker, null);
                spinnerCountry = (Spinner) viewCountryPicker.findViewById(R.id.spinnerCountry);

                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                        R.array.countries_array, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerCountry.setAdapter(adapter);

                //Default spinner value
                String myString = "Spain";
                ArrayAdapter myAdap = (ArrayAdapter) spinnerCountry.getAdapter();
                int spinnerPosition = myAdap.getPosition(myString);
                spinnerCountry.setSelection(spinnerPosition);

                alertDialogCountry.setView(spinnerCountry);
                alertDialogCountry.setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                countryEdit.setText(spinnerCountry.getSelectedItem().toString());
                                updateCountry = true;
                            }
                        });

                alertDialogCountry.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                alertDialogCountry.setView(viewCountryPicker);
                alertDialogCountry.show();
            }
        });

        if (Store.get().checkInternetConnection(getContext())) {
            refreshProfile();
        }

        return view;
    }

    public void refreshProfile() {

        getProfile();

    }

    @Override
    public void onClick(View v) {
        FragmentManager fragmentManager = getFragmentManager();
        switch (v.getId()) {
            case R.id.buttonOk:

                Toast.makeText(getActivity(), "Perfil actualizado correctamente", Toast.LENGTH_SHORT).show();
                Intent int1 = new Intent(getActivity(), MainActivity.class);
                int1.putExtra("activity", "Main");

                startActivity(int1);
                updateProfile();


                if (!nameEdit.getText().toString().isEmpty() && !surnameEdit.getText().toString().isEmpty()
                        && !textViewControlRange.getText().toString().isEmpty() && !countryEdit.getText().toString().isEmpty()
                        && !birthdayEdit.getText().toString().isEmpty() && !genderEdit.getText().toString().isEmpty()
                        && !weightEdit.getText().toString().isEmpty() && !heightEdit.getText().toString().isEmpty()
                        && !debutEdit.getText().toString().isEmpty()) {

                    //Get all the data
                    name = nameEdit.getText().toString();
                    surname = surnameEdit.getText().toString();

                    if (updateCountry) country = countryEdit.getText().toString();
                    heightValue = Float.parseFloat(heightEdit.getText().toString() + "f");
                    weightValue = Float.parseFloat(weightEdit.getText().toString() + "f");



                }
                break;
            case R.id.llName:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                alertDialog.setTitle("Introduce nombre");

                final EditText input = new EditText(getContext());
                input.setHint(nameEdit.getText());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input);
                alertDialog.setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (!input.getText().toString().isEmpty()) {
                                    nameEdit.setText(input.getText().toString());
                                }
                            }
                        });

                alertDialog.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                alertDialog.show();
                break;
            case R.id.llSurname:
                AlertDialog.Builder alertDialogSurname = new AlertDialog.Builder(getContext());
                alertDialogSurname.setTitle("Introduce apellidos");

                final EditText inputSurname = new EditText(getContext());
                inputSurname.setHint(surnameEdit.getText());
                LinearLayout.LayoutParams lpSurname = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);

                inputSurname.setLayoutParams(lpSurname);
                alertDialogSurname.setView(inputSurname);
                alertDialogSurname.setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (!inputSurname.getText().toString().isEmpty()) {
                                    surnameEdit.setText(inputSurname.getText().toString());
                                }
                            }
                        });

                alertDialogSurname.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                alertDialogSurname.show();
                break;

            case R.id.llBirthday:
                dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                birthdayEdit.setText(DateConvert.checkDateWithoutHoursWithSlashBar(dayOfMonth, monthOfYear + 1, year));
                                yearBirthday = year;
                                monthBirthday = monthOfYear + 1;
                                dayBirthday = dayOfMonth;
                                birthday = DateConvert.valuesToMilliseconds(dayBirthday, monthBirthday, yearBirthday, 0, 0, 0);
                                updateBirthday = true;
                            }
                        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dpd.show();
                break;
            case R.id.llGender:
                new ListGenderRadioDialog().show(fragmentManager, "ListRadioDialog");
                break;
            case R.id.llHeight:
                android.app.AlertDialog.Builder alert2 = new android.app.AlertDialog.Builder(getContext());

                alert2.setTitle("Selecciona tu altura: ");

                np = new NumberPicker(getContext());
                np.setMinValue(120);
                np.setMaxValue(240);
                np.setWrapSelectorWheel(false);
                np.setValue(160);
                NumberPicker.OnValueChangeListener myValChangedListener2 = new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        Log.i("value is", "" + newVal);
                    }
                };

                alert2.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        heightEdit.setText(np.getValue() + "");
                        heightValue = Float.parseFloat(np.getValue() + "f");
                        updateHeight = true;
                    }
                });

                alert2.setView(np);
                alert2.show();
                break;
            case R.id.llDebut:
                dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                debutEdit.setText(DateConvert.checkDateWithoutHoursWithSlashBar(dayOfMonth, monthOfYear + 1, year));
                                yearDebut = year;
                                monthDebut = monthOfYear + 1;
                                dayDebut = dayOfMonth;
                                diabeticDebut = DateConvert.valuesToMilliseconds(dayDebut, monthDebut, yearDebut, 0, 0, 0);
                                updateDebut = true;
                            }
                        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dpd.show();
                break;
        }
    }

    public static class ListGenderRadioDialog extends DialogFragment {

        public ListGenderRadioDialog() {
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return createRadioListGenderDialog();
        }

        /**
         * Crea un diálogo con una lista de radios
         *
         * @return Diálogo
         */
        public AlertDialog createRadioListGenderDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            final CharSequence[] items = new CharSequence[2];

            items[0] = "Hombre";
            items[1] = "Mujer";


            builder.setTitle("Género")
                    .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (items[which].toString().equals("Hombre"))
                                genderEdit.setText("Hombre");
                            else {
                                genderEdit.setText("Mujer");
                            }
                            dismiss();
                        }
                    });

            return builder.create();
        }

    }

    public String checkDate(int dayOfMonth, int monthOfYear, int year) {
        String date = null;
        if (dayOfMonth < 10) {
            date = "0" + dayOfMonth;
        } else {
            date = "" + dayOfMonth;
        }
        if (monthOfYear < 10) {
            date = date + "/0" + monthOfYear;
        } else {
            date = date + "/" + monthOfYear;
        }
        date = date + "/" + year;
        return date;
    }

    public void getProfile() {


        String email = Store.get().getEmail();



        String url = "http://aplicaciondimo.com/getUser.php?email="+email;
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][", ",");

                try {
                    JSONArray ja = new JSONArray(response);

                    String name = ja.getString(0);
                    name = name.replace("_", " ");



                    if (ja.getString(5) != null) {
                        String country = ja.getString(5);
                        Store.get().setCountry(country);
                        countryEdit.setText(country);
                    }
                    genderEdit.setText(ja.getString(7));


                    if (ja.getString(6) != null) {
                        String nacimiento = ja.getString(6);
                        Store.get().setBirthday(nacimiento);
                        birthdayEdit.setText(nacimiento);
                    }


                    weightEdit.setText(ja.getString(8));
                    heightEdit.setText(ja.getString(9));
                    debutEdit.setText(ja.getString(10));
                    textViewControlRange.setText(ja.getString(11));
                    Store.get().setName(name);

                    //Inform UIElements

                   nameEdit.setText(Store.get().getName());
                    surnameEdit.setText(Store.get().getSurname());
                    mailEdit.setText(Store.get().getEmail());



                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //}


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);
    }


    private void updateProfile() {
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        String email = Store.get().getEmail();
        String url = "http://aplicaciondimo.com/updateProfile.php?email=" + email + "&name=" + nameEdit.getText().toString() + "&surname=" + surnameEdit.getText().toString() + "&country=" + countryEdit.getText().toString() + "&birthday=" + birthdayEdit.getText().toString() + "&gender=" + genderEdit.getText().toString() + "&weight=" + weightEdit.getText().toString() + "&height=" + heightEdit.getText().toString() + "&debut=" + debutEdit.getText().toString() + "&rank=" + textViewControlRange.getText().toString();


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][", ",");
                if ((response.length() > 0)) {
                    try {

                        JSONArray ja = new JSONArray(response);

                        Toast.makeText(getActivity(), "Perfil actualizado", Toast.LENGTH_SHORT).show();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Fragment fragment = new FragmentMain();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }








    private void showToast() {
        Toast.makeText(getActivity().getApplicationContext(), "Registrado con éxito", Toast.LENGTH_SHORT).show();
    }
}


