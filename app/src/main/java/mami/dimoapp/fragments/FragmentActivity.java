package mami.dimoapp.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import mami.dimoapp.MainActivity;
import mami.dimoapp.R;
import mami.dimoapp.custom.CustomMarkerView;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.custom.MyValueFormatter;
import mami.dimoapp.model.PhysicalActivity;
import mami.dimoapp.model.Store;


public class FragmentActivity extends Fragment implements View.OnClickListener{

    EditText etInicio, etFin;

    private RecyclerView recView;
    private TextView textViewInfRange, tvContent;
    int mYear, mMonth, mDay, mHour, mMinute;
    private Date dateStart;
    private LineChart chartActivity;
    private LinearLayout llActivity;
    private CustomMarkerView mv;
    private IAxisValueFormatter xAxisFormatter;
    private Button btnFind;
    private long start, end;


    public FragmentActivity() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activity, container, false);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        dateStart = new Date();

        start = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0);
        start = start-259200000L;
        end = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0)+86399999;

        btnFind = (Button)view.findViewById(R.id.buttonFind);
        btnFind.setOnClickListener(this);
        textViewInfRange = (TextView)view.findViewById(R.id.textViewInfRange);

        llActivity = (LinearLayout)view.findViewById(R.id.llActivity);
        chartActivity =(LineChart)view.findViewById(R.id.chartActivity);

        //Custom Marker
        mv = new CustomMarkerView(getContext(), R.layout.marker);
        tvContent = (TextView)mv.findViewById(R.id.tvContent);

        recView = (RecyclerView) view.findViewById(R.id.activityList);

        etInicio = (EditText)view.findViewById(R.id.editTextInicio);
        etInicio.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear,year))>0){
                                    dateStart = DateConvert.valuesToDate(dayOfMonth,monthOfYear+1,year);
                                    etInicio.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    start = DateConvert.valuesToMilliseconds(dayOfMonth,monthOfYear+1,year,0,0,0);
                                }else{
                                    Toast.makeText(getContext(),"La fecha de inicio deber ser antes de la actual", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }, mYear, mMonth-1, mDay);
                dpd.show();
            }
        });


        etFin = (EditText)view.findViewById(R.id.editTextFin);
        etFin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(dateStart.compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear+1,year))<=0){
                                    etFin.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    end = DateConvert.valuesToMilliseconds(dayOfMonth,monthOfYear+1,year,23,59,59);
                                }else{
                                    Toast.makeText(getContext(),"La fecha de fin no puede ser mayor que la de inicio", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        iniciarFecha(view);

        return view;
    }
    private void iniciarFecha(View view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etFin.setText(formatDate.format(c.getTime()));
        etInicio.setText(formatDate.format(start));
    }

    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonFind:
                String sourceString = "Datos desde <b>"+ etInicio.getText()+"</b> hasta "+"<b>"+etFin.getText()+"</b>";
                if (Build.VERSION.SDK_INT >= 24) {
                    textViewInfRange.setText(Html.fromHtml(sourceString, Html.FROM_HTML_MODE_LEGACY));// for 24 api and more

                } else {
                    textViewInfRange.setText(Html.fromHtml(sourceString)); // or for older api
                }
                llActivity.setVisibility(View.VISIBLE);
                getChartActivity();
                //initChartActivity(chartActivity);
        }
    }

    public void getChartActivity(){
        final ArrayList<Entry> entries = new ArrayList<Entry>();

        // Instantiate the RequestQueue.
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        String url = "http://mamilab.esi.uclm.es:5050/api/v2.0/activities?start="+start*1000000+"&end="+end*1000000;
        final List<PhysicalActivity> activitiesList = new ArrayList<>();

        CustomRequest customRequest = new CustomRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {

                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());

                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject activity = (JSONObject) jsonArray1.get(i);
                                    JSONObject attributes = (JSONObject) activity.get("attributes");
                                    int calories = Integer.parseInt(String.valueOf(attributes.get("calories")));
                                    long duration = Long.parseLong(String.valueOf(attributes.get("duration")));
                                    float distance = Float.parseFloat(String.valueOf(attributes.get("distance")));
                                    int type = Integer.parseInt(String.valueOf(attributes.get("type")));
                                    int steps = Integer.parseInt(String.valueOf(attributes.get("steps")));
                                    long id = Long.parseLong(String.valueOf(activity.get("id")));
                                    PhysicalActivity pa = new PhysicalActivity(id/1000000, type, duration, steps, calories, distance);
                                    activitiesList.add(pa);
                                }

                                Collections.sort(activitiesList, new Comparator<PhysicalActivity>() {
                                    @Override
                                    public int compare(PhysicalActivity pa1, PhysicalActivity pa2) {
                                        return (int)(pa1.getTime() - pa2.getTime());
                                    }
                                });

                                PhysicalActivity pa = new PhysicalActivity(0, 2, 0, 0, 0, 0);

                                //RecyclerView adapter
                                final FragmentActivity.SimpleItemRecyclerViewAdapter adapter = new FragmentActivity.SimpleItemRecyclerViewAdapter(activitiesList);
                                recView.setHasFixedSize(true);
                                recView.setAdapter(adapter);
                                recView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

                                for(int i=0; i<activitiesList.size();i++){
                                    float floatx = (float)activitiesList.get(i).getTime();
                                    entries.add(new Entry(floatx, activitiesList.get(i).getTipo()+1, activitiesList.get(i)));
                                    entries.add(new Entry(floatx+((float)activitiesList.get(i).getDuration()), activitiesList.get(i).getTipo()+1, activitiesList.get(i)));
                                    if(i+1<activitiesList.size()) {
                                        entries.add(new Entry(floatx + ((float) activitiesList.get(i).getDuration() ), 0, pa));
                                        entries.add(new Entry((float) activitiesList.get(i + 1).getTime(), 0, pa));
                                    }
                                }

                                initChartActivity(chartActivity, entries);
                            }else{
                                textViewInfRange.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartActivity: ", error.toString() );

            }
        });

        customRequest.setCookies(cookies);
        // and finally add the request to the queue
        Volley.newRequestQueue(getContext()).add(customRequest);
    }

    public void initChartActivity(LineChart chart, ArrayList<Entry> entries){

        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.getDescription().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        chart.setPinchZoom(true);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                PhysicalActivity p = (PhysicalActivity)e.getData();
                if(p.getTipo()==0){
                    tvContent.setText("Actividad: Andar"+"\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(p.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(p.getTime()) +
                            "\nDuración: "+(p.getDuration())/60+" minutos");
                }else if(p.getTipo()==1){
                    tvContent.setText("Actividad: Correr"+ "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(p.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(p.getTime()) +
                            "\nDuración: "+(p.getDuration())/60+" minutos");
                }else{
                    tvContent.setText("Actividad: Parado");
                }

            }

            @Override
            public void onNothingSelected() {

            }
        });

        LineDataSet dataSet = new LineDataSet(entries, "Actividad Física");
        dataSet.setMode(LineDataSet.Mode.LINEAR);
        dataSet.setColor(Color.GREEN);
        dataSet.setCircleColor(Color.GREEN);
        dataSet.setCircleRadius(5f);
        dataSet.setValueTextSize(12f);
        dataSet.setLineWidth(3);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);
        dataSet.setDrawValues(false);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);
        xAxis.setValueFormatter(xAxisFormatter);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setAxisMinimum(0f); // start at zero
        yAxis.setAxisMaximum(2f);
        chart.getAxisLeft().setDrawLabels(false);

    }

    //Adapter para RecyclerView
    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<FragmentActivity.SimpleItemRecyclerViewAdapter.ViewHolder> {

        private List<PhysicalActivity> datos;

        public SimpleItemRecyclerViewAdapter(List<PhysicalActivity> datos) {
            this.datos = datos;
        }

        @Override
        public FragmentActivity.SimpleItemRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_view_physical, parent, false);
            FragmentActivity.SimpleItemRecyclerViewAdapter.ViewHolder tvh = new FragmentActivity.SimpleItemRecyclerViewAdapter.ViewHolder(view);
            return tvh;
        }

        @Override
        public void onBindViewHolder(FragmentActivity.SimpleItemRecyclerViewAdapter.ViewHolder holder, final int position) {
            /*//Decimales
            float duration = datos.get(position).getDuration()/60f;
            String formattedString = String.format("%.02f", duration);*/
            if(datos.get(position).getDuration()<60){
                holder.activityTime.setText(" < 1 minuto");
            }else{
                holder.activityTime.setText(datos.get(position).getDuration()/60+ " minutos");
            }
            switch (datos.get(position).getTipo()){
                case 0:
                    holder.activityName.setText("Andar");
                    holder.activityImage.setImageResource(R.drawable.ic_walk_black_48dp);
                    break;
                case 1:
                    holder.activityName.setText("Correr");
                    holder.activityImage.setImageResource(R.drawable.ic_run_black_48dp);
                        break;
            }
            if(datos.get(position).getSteps()>0)holder.activitySteps.setText(datos.get(position).getSteps()+ " pasos");
            if(datos.get(position).getCalories()>0)holder.activityCalories.setText(datos.get(position).getCalories()+ " calorias");
            if(datos.get(position).getDistance()>0)holder.activityDistance.setText(datos.get(position).getDistance()+" metros");
            holder.activityDate.setText(DateConvert.millisecondsToDateStringWithoutSeconds(datos.get(position).getTime()));
            holder.itemView.setTag(0);
            /*holder.itemView.setOnLongClickListener(new View.OnLongClickListener(){
                @Override
                public boolean onLongClick(View view) {
                    Toast.makeText(getContext(),"hola", Toast.LENGTH_SHORT).show();
                    return false;
                }
            });*/
        }

        //Conseguir tamaño del array
        @Override
        public int getItemCount() {
            return datos.size();
        }

        public class ViewHolder
                extends RecyclerView.ViewHolder {

            public final TextView activityTime;
            public final TextView activityName;
            public final TextView activitySteps;
            public final TextView activityCalories;
            public final TextView activityDistance;
            public final TextView activityDate;
            public final RelativeLayout activityRl;
            public final ImageView activityImage;

            public ViewHolder(View view) {
                super(view);

                activityTime = (TextView)view.findViewById(R.id.appointmentPlace);
                activityName = (TextView)view.findViewById(R.id.AppointmentTitle);
                activitySteps = (TextView)view.findViewById(R.id.appointmentDescription);
                activityCalories = (TextView)view.findViewById(R.id.activityCalories);
                activityDistance = (TextView)view.findViewById(R.id.activityDistance);
                activityDate = (TextView)view.findViewById(R.id.activityDate);
                activityRl = (RelativeLayout) view.findViewById(R.id.activityRl);
                activityImage = (ImageView) view.findViewById(R.id.activityImg);
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if(Store.get().getEmail()==null){
            Intent int1 =  new Intent(getContext(), MainActivity.class);
            int1.putExtra("activity", "Stats");
            startActivity(int1);
            Toast.makeText(getContext(),"Conectando al servidor",Toast.LENGTH_SHORT).show();
        }
    }
}
