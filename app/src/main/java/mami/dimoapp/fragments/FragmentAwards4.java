package mami.dimoapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Date;
import java.util.List;

import mami.dimoapp.FoodActivity;
import mami.dimoapp.R;
import mami.dimoapp.custom.CustomMarkerView;
import mami.dimoapp.model.GlucosePoint;
import mami.dimoapp.model.Store;

public class FragmentAwards4 extends Fragment implements View.OnClickListener {
    public Button btnCanjear;
    public TextView availablePoints;
    public TextView tvNecessaryPoints,tvNecessaryPointsText;
    private TextView tvAwardName;
    private int dailyPoints,idAward;
    private int totalPoints;
    private String awardName;
    private int neccesaryPoints, category;
    private LinearLayout linearMain;
    private String email;
    private String date;
    private String link;
    private ImageView imageAward;

    public FragmentAwards4() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_awards, container, false);
        btnCanjear = (Button) view.findViewById(R.id.buttonCanjear);

        dailyPoints=Store.get().getdailyPoints();

        email=Store.get().getEmail();
        category =Store.get().getCategoria();

        //Indica = bronce, 1 plata, 2 Oro
        if(category ==0) {
            link = Store.get().getLinkP4DailyBronze();
            neccesaryPoints = Store.get().getpDailyP4Bronze();
            awardName =Store.get().getDescriptionDailyP4Bronze();
            idAward=Store.get().getIdP4DailyBronze();


        }else if(category ==1){
            neccesaryPoints = Store.get().getpDailyP4Silver();
            link = Store.get().getLinkP4DailySilver();
            awardName =Store.get().getDescriptionDailyP4Silver();
            idAward=Store.get().getIdP4DailySilver();



        }else {
            neccesaryPoints = Store.get().getpDailyP4Gold();
            link = Store.get().getLinkP4DailyGold();
            awardName =Store.get().getDescriptionDailyP4Gold();
            idAward=Store.get().getIdP4DailyGold();
        }
        Date d = new Date();
        date = (String) DateFormat.format("dd/MM/yyyy", d.getTime());


        linearMain = (LinearLayout) view.findViewById(R.id.linearLayoutContent);
        availablePoints = (TextView) view.findViewById(R.id.puntosDisponibles);
        tvNecessaryPoints =(TextView) view.findViewById(R.id.puntosNecesarios1);
        tvNecessaryPointsText=(TextView) view.findViewById(R.id.puntosNecesarios);

        tvAwardName =(TextView) view.findViewById(R.id.NombrePremio);

        tvAwardName.setText(String.valueOf(awardName));
        tvNecessaryPoints.setText(String.valueOf(neccesaryPoints));



        imageAward= (ImageView) view.findViewById(R.id.imageAward);


        //cargar puntos de la BD
        if(dailyPoints <0){
            availablePoints.setText(String.valueOf(0));
        } else {
            availablePoints.setText(String.valueOf(dailyPoints));
        }

        btnCanjear.setOnClickListener(this);
        //Comprobar si ha sido canjeado, si lo ha sido ponerlo visible, si no se deja por defecto hasta que se pulse canjear
        if(Store.get().isExchange4()&& Store.get().getCategoria()==0){

            tvAwardName.setVisibility(View.VISIBLE);
            linearMain.setVisibility(View.VISIBLE);
            btnCanjear.setVisibility(View.INVISIBLE); //oculta el button
            tvNecessaryPoints.setVisibility(View.INVISIBLE);
            tvNecessaryPointsText.setText(R.string.TextAwardExchangue);
            Glide.with(getActivity()).load(link).into(imageAward);

        } else  if(Store.get().isExchange9() && Store.get().getCategoria()==1 ){

            tvAwardName.setVisibility(View.VISIBLE);
            linearMain.setVisibility(View.VISIBLE);
            btnCanjear.setVisibility(View.INVISIBLE); //oculta el button
            tvNecessaryPoints.setVisibility(View.INVISIBLE);
            tvNecessaryPointsText.setText(R.string.TextAwardExchangue);
            Glide.with(getActivity()).load(link).into(imageAward);

        }else if(Store.get().isExchange13()&& Store.get().getCategoria()==2 ){

            tvAwardName.setVisibility(View.VISIBLE);
            linearMain.setVisibility(View.VISIBLE);
            btnCanjear.setVisibility(View.INVISIBLE); //oculta el button
            tvNecessaryPoints.setVisibility(View.INVISIBLE);
            tvNecessaryPointsText.setText(R.string.TextAwardExchangue);
            Glide.with(getActivity()).load(link).into(imageAward);

        }
        //



        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonCanjear:
                if(dailyPoints>= neccesaryPoints) {
                    totalPoints =dailyPoints- neccesaryPoints;
                    linearMain.setVisibility(View.VISIBLE);
                    Glide.with(getActivity()).load(link).into(imageAward);
                    makeRequestBD("http://aplicaciondimo.com/setExchange.php?idUser=" + email + "&idAward="+idAward+"&date="+date);
                    btnCanjear.setVisibility(View.GONE);
                    makeRequestBD("http://aplicaciondimo.com/updateUser.php?email="+email+"&dailyPoints="+ totalPoints);
                    availablePoints.setText(String.valueOf(totalPoints));
                    Store.get().setdailyPoints(totalPoints);
                    switch (idAward){
                        case 4:
                            Store.get().setExchange4(true);
                            break;
                        case 9:
                            Store.get().setExchange9(true);
                            break;
                        case 13:
                            Store.get().setExchange13(true);
                            break;


                    }
                }else{
                    Toast.makeText(getActivity(), "No dispone de puntos suficientes", Toast.LENGTH_SHORT).show();
                }











        }
    }



    public void makeRequestBD(String URL){
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");
                if ( (response.length()>0)){
                    try {

                        JSONArray ja = new JSONArray(response);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

}




