package mami.dimoapp.fragments;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import mami.dimoapp.MainActivity;
import mami.dimoapp.R;
import mami.dimoapp.custom.CustomMarkerView;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.CustomRequest2;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.custom.MyValueFormatter;
import mami.dimoapp.custom.Tabla;
import mami.dimoapp.model.ActivityPiePoint;
import mami.dimoapp.model.GlucosePoint;
import mami.dimoapp.model.NutritionDay;
import mami.dimoapp.model.PhysicalActivity;
import mami.dimoapp.model.Profile;
import mami.dimoapp.model.Store;

public class FragmentMain extends Fragment{

    private EditText etInicio, etFin;
    int mYear, mMonth, mDay, mHour, mMinute;
    private TextView textViewInfRange;
    static TextView textViewInfRange2;
    static TextView tvContent;
    private LineChart chartGlucose;
    private PieChart pieChart;
    private IAxisValueFormatter xAxisFormatter;
    private Tabla tableNutrition;
    private long start, end, yesterday3, yesterday2, yesterday1, today;
    private Date dateStart;
    private CustomMarkerView mv;
    private CustomRequest customRequest;
    private ArrayList<GlucosePoint> glucoseList;
    private CardView cvTableNutrition,cvPieChart,cvGlucoseChart;
    private SwipeRefreshLayout swipeContainer;


    public FragmentMain() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

        View view = inflater.inflate(R.layout.fragment_main, container, false);


        //Range Code
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        end = System.currentTimeMillis();
        dateStart = DateConvert.valuesToDate(mDay,mMonth,mYear);
        start = dateStart.getTime()-259199000L;  //3 days ago

        yesterday3 = start;
        yesterday2 = yesterday3+86399999;
        Log.i("yesterday2","yes2 "+yesterday2);
        yesterday1 = yesterday2+86399999;

        Log.i("yesterday2","yes1 "+yesterday1);

        today = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0);
        Log.i("today","yes1 "+today);
        cvGlucoseChart =(CardView)view.findViewById(R.id.cvGlu);
        cvTableNutrition = (CardView)view.findViewById(R.id.cvNutrition);
        cvPieChart = (CardView)view.findViewById(R.id.cvPieChart);
        cvGlucoseChart.setVisibility(View.VISIBLE);
        cvTableNutrition.setVisibility(View.VISIBLE);
        cvPieChart.setVisibility(View.VISIBLE);


        //Custom Marker
        mv = new CustomMarkerView(getContext(), R.layout.marker);
        tvContent = (TextView)mv.findViewById(R.id.tvContent);

        textViewInfRange = (TextView)view.findViewById(R.id.textViewInfRange);



            chartGlucose = (LineChart) view.findViewById(R.id.chartGlucose);

            getChartGlu();

            pieChart = (PieChart) view.findViewById(R.id.piechart);
            getPieChart();

            tableNutrition = new Tabla(getActivity(), (TableLayout) view.findViewById(R.id.tableNutritionMain));
            initTableNutrition2();

       swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeContainer .setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeContainer.setRefreshing(false);
            }
        });

        return view;
    }

    public void getChartGlu(){
        final ArrayList<Entry> entries = new ArrayList<Entry>();

        String url = "http://aplicaciondimo.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;

        glucoseList = new ArrayList<>();

        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject glucose = (JSONObject) jsonArray1.get(i);
                                    int check = Integer.parseInt(String.valueOf(glucose.get("type")));
                                    float value = Float.parseFloat(String.valueOf(glucose.get("value")));
                                    long id = Long.parseLong(String.valueOf(glucose.get("date")));
                                    GlucosePoint gp = new GlucosePoint(id, value, check);
                                    glucoseList.add(gp);
                                }

                                Collections.sort(glucoseList, new Comparator<GlucosePoint>() {
                                    @Override
                                    public int compare(GlucosePoint gp1, GlucosePoint gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });

                                for (int i = 0; i < glucoseList.size(); i++) {
                                    float floatx = (float) glucoseList.get(i).getTime();

                                    entries.add(new Entry(floatx, glucoseList.get(i).getIndex(), glucoseList.get(i)));
                                }
                                initChartGlu(chartGlucose, entries);
                            }else{
                                cvGlucoseChart.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cvGlucoseChart.setVisibility(View.INVISIBLE);


            }
        });

        // and finally add the request to the queue
        Volley.newRequestQueue(getContext()).add(customRequest);
    }

    public void getChartGlu2(){

        glucoseList = new ArrayList<>();
        final ArrayList<Entry> entries = new ArrayList<Entry>();

            int check = 5;
            float value = 70;
            long today =System.currentTimeMillis();



        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());

       String url = "http://aplicaciondimo.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+yesterday2+"&end="+today;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");


                try {
                    JSONArray ja = new JSONArray(response);
                    if(ja.length()>0) {
                        int elemento=0;

                        for (int i=0; i < ja.length()/3; i++) {

                            Log.i("Longitud","longitud "+ja.length());

                            long fecha = ja.getLong(elemento);
                            int type= ja.getInt(elemento+1);
                            float value = (float) ja.getDouble(elemento+2);
                            GlucosePoint gp = new GlucosePoint(fecha, value, type);
                            glucoseList.add(gp);

                            elemento+=3;
                        }

                        Collections.sort(glucoseList, new Comparator<GlucosePoint>() {
                            @Override
                            public int compare(GlucosePoint gp1, GlucosePoint gp2) {
                                return (int) (gp1.getTime() - gp2.getTime());
                            }
                        });

                        for (int i = 0; i < glucoseList.size(); i++) {

                            float floatx = (float) glucoseList.get(i).getTime();
                            entries.add(new Entry(floatx, glucoseList.get(i).getIndex(), glucoseList.get(i)));
                        }

                        initChartGlu(chartGlucose, entries);
                    }





                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);



    }



    public void initChartGlu(LineChart chart, ArrayList<Entry> entries){
        mv.setChartView(chart);
        chart.setMarker(mv);




        chart.getDescription().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        chart.setPinchZoom(true);

        LineDataSet dataSet = new LineDataSet(entries, "Glucosa");

        dataSet.setMode(LineDataSet.Mode.LINEAR);
        dataSet.setColor(Color.RED);
        dataSet.setCircleColor(Color.RED);
        dataSet.setCircleRadius(3f);
        dataSet.setValueTextSize(9f);
        dataSet.setLineWidth(2);
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setHighlightEnabled(true);

        LineData dataSets = new LineData();
        dataSets.addDataSet(dataSet);
        chart.setData(dataSets);

        chart.animateY(3000);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisFormatter = new MyValueFormatter(chart);


        xAxis.setValueFormatter(xAxisFormatter);





        YAxis yAxis = chart.getAxisLeft();
        yAxis.setAxisMinimum(0f); // start at zero
        yAxis.setAxisMaximum(350f);
        //Glucose Limits
        if(Store.get().getMaxGlucose()!=0) {
            LimitLine ll = new LimitLine(Store.get().getMaxGlucose(), "MAX");
            LimitLine ll2 = new LimitLine(Store.get().getMinGlucose(), "MIN");
            ll.setLineColor(Color.BLUE);
            ll.setLineWidth(4f);
            ll.setTextColor(Color.BLUE);
            ll.setTextSize(12f);
            ll.enableDashedLine(10f, 10f, 0f);
            ll2.setLineColor(Color.BLUE);
            ll2.setLineWidth(4f);
            ll2.setTextColor(Color.BLUE);
            ll2.setTextSize(12f);
            ll2.enableDashedLine(10f, 10f, 0f);
            yAxis.addLimitLine(ll);
            yAxis.addLimitLine(ll2);
        }

    chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                GlucosePoint gp = (GlucosePoint) e.getData();
                if(gp.getWhen()==0) {
                    tvContent.setText("Glucosa: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                            "\nAntes de comer");
                }else if(gp.getWhen()==1){
                    tvContent.setText("Glucosa: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                            "\nDespués de comer");
                }else{
                    tvContent.setText("Glucosa: " + e.getY() + "\n Fecha: " + DateConvert.millisecondsToDateStringWithoutHours(gp.getTime()) +
                            "\nHora: " + DateConvert.millisecondsToDateStringWithoutDate(gp.getTime()) +
                            "\nOtros");
                }
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    public void getPieChart(){
        final ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // Instantiate the RequestQueue.
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        String url = "http://aplicaciondimo.com/getActivity.php?email="+Store.get().getEmail();
        final List<PhysicalActivity> activitiesList = new ArrayList<>();

        final ActivityPiePoint appWalk = new ActivityPiePoint(0,0);
        final ActivityPiePoint appRun = new ActivityPiePoint(1,0);
        final ActivityPiePoint appStop = new ActivityPiePoint(2,0);

        final float totalDaySeconds = 86400f;

        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {

                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());

                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject activity = (JSONObject) jsonArray1.get(i);


                                    int duration = Integer.parseInt(String.valueOf(activity.get("duration")));
                                    int type = Integer.parseInt(String.valueOf(activity.get("type")));

                                    if(type==0)appWalk.setValue(appWalk.getValue()+duration);
                                    if(type==1)appRun.setValue(appRun.getValue()+duration);
                                    /*int calories = Integer.parseInt(String.valueOf(attributes.get("calories")));
                                    float distance = Float.parseFloat(String.valueOf(attributes.get("distance")));
                                    int steps = Integer.parseInt(String.valueOf(attributes.get("steps")));
                                    long id = Long.parseLong(String.valueOf(activity.get("id")));*/
                                }

                                if(appWalk.getValue()>0)entries.add(new PieEntry(appWalk.getValue()/totalDaySeconds, "Andar", appWalk));
                                if(appRun.getValue()>0)entries.add(new PieEntry(appRun.getValue()/totalDaySeconds, "Correr", appRun));

                                float stopTime = 86400f-((appWalk.getValue()/totalDaySeconds)+(appRun.getValue()/totalDaySeconds));
                                float stopTime1 = ((appWalk.getValue()+appRun.getValue()-86400f));




                               // entries.add((new PieEntry(stopTime/totalDaySeconds, "Parado", appStop)));
                                entries.add((new PieEntry(stopTime, "Parado", appStop)));


                                initPieChart(pieChart, entries);
                            }else{
                                cvPieChart.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartActivity: ", error.toString() );
                Log.e("ChartGlu: ", error.toString() );

            }
        });

        // and finally add the request to the queue
        Volley.newRequestQueue(getContext()).add(customRequest);
    }






    public void initPieChart (PieChart chart, List<PieEntry> entries ){

        chart.setUsePercentValues(true);
        PieDataSet set = new PieDataSet(entries, "");
        set.setValueTextSize(13f);
        set.setColors(ColorTemplate.MATERIAL_COLORS);
        PieData data = new PieData(set);
        data.setValueFormatter(new PercentFormatter());

        chart.getDescription().setEnabled(false);
        chart.setData(data);
        chart.animateY(2000);

        //Text inside the circle
        SpannableString s = new SpannableString("Últimas 72 horas");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, s.length(), 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
        chart.setCenterText(s);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                ActivityPiePoint app = (ActivityPiePoint) e.getData();
                if(app.getType()==0) {
//                    tvContent.setText("Andar\n" + app.getValue()/10 + " segundos");
                    tvContent.setText("Andar\n" + app.getValue()/10 + " segundos");

                }else if(app.getType()==1){
                    tvContent.setText("Correr\n" + app.getValue()/10 + " segundos");
                }else{
                    tvContent.setText("Parado\n");
                }
            }

            @Override
            public void onNothingSelected() {}
        });
    }




    public void initTableNutrition2(){
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        String url = "http://aplicaciondimo.com/getConsumptions.php?email="+Store.get().getEmail()+"&start="+start +"&end="+end;

        final DecimalFormat df = new DecimalFormat("#.##");

        CustomRequest2 customRequest1 = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetChartGluCal","Response is: "+ jsonArray.toString());
                        ArrayList<NutritionDay> daysList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject consumption = (JSONObject) jsonArray1.get(i);
                                    float calories = Float.parseFloat(String.valueOf(consumption.get("total-calories")));
                                    float carbohidrates = Float.parseFloat(String.valueOf(consumption.get("total-carbohydrates")));
                                    float fats = Float.parseFloat(String.valueOf(consumption.get("total-fats")));
                                    float glycemic = Float.parseFloat(String.valueOf(consumption.get("total-glycemic-load")));
                                    float proteins = Float.parseFloat(String.valueOf(consumption.get("total-proteins")));
                                    long id = Long.parseLong(String.valueOf(consumption.get("start")));
                                    NutritionDay nd = new NutritionDay(id, calories, carbohidrates, proteins, fats, glycemic);
                                    daysList.add(nd);
                                }

                                NutritionDay nd0 = new NutritionDay(today,0,0,0,0,0);
                                NutritionDay nd1 = new NutritionDay(yesterday1,0,0,0,0,0);
                                NutritionDay nd2 = new NutritionDay(yesterday2,0,0,0,0,0);
                                NutritionDay nd3 = new NutritionDay(yesterday3,0,0,0,0,0);
                                for (int i=0;i<daysList.size();i++){
                                    if(daysList.get(i).getTime()<=yesterday2){
                                        nd3.setKcal(nd3.getKcal()+daysList.get(i).getKcal());
                                        nd3.setcH(nd3.getcH()+daysList.get(i).getcH());
                                        nd3.setFat(nd3.getFat()+daysList.get(i).getFat());
                                        nd3.setGluc(nd3.getGluc()+daysList.get(i).getGluc());
                                        nd3.setProt(nd3.getProt()+daysList.get(i).getProt());
                                    }else if((daysList.get(i).getTime()>yesterday2) && (daysList.get(i).getTime()<=yesterday1)){
                                        nd2.setKcal(nd2.getKcal()+daysList.get(i).getKcal());
                                        nd2.setcH(nd2.getcH()+daysList.get(i).getcH());
                                        nd2.setFat(nd2.getFat()+daysList.get(i).getFat());
                                        nd2.setGluc(nd2.getGluc()+daysList.get(i).getGluc());
                                        nd2.setProt(nd2.getProt()+daysList.get(i).getProt());
                                    }else if((daysList.get(i).getTime()>yesterday1) && (daysList.get(i).getTime()<=today)){
                                        nd1.setKcal(nd1.getKcal()+daysList.get(i).getKcal());
                                        nd1.setcH(nd1.getcH()+daysList.get(i).getcH());
                                        nd1.setFat(nd1.getFat()+daysList.get(i).getFat());
                                        nd1.setGluc(nd1.getGluc()+daysList.get(i).getGluc());
                                        nd1.setProt(nd1.getProt()+daysList.get(i).getProt());
                                    }else{
                                        nd0.setKcal(nd0.getKcal()+daysList.get(i).getKcal());
                                        nd0.setcH(nd0.getcH()+daysList.get(i).getcH());
                                        nd0.setFat(nd0.getFat()+daysList.get(i).getFat());
                                        nd0.setGluc(nd0.getGluc()+daysList.get(i).getGluc());
                                        nd0.setProt(nd0.getProt()+daysList.get(i).getProt());
                                    }
                                }

                                ArrayList<NutritionDay> totalDaysList = new ArrayList<>();
                                totalDaysList.add(0, nd0);
                                totalDaysList.add(1, nd1);
                                totalDaysList.add(2, nd2);
                                totalDaysList.add(3, nd3);

                                tableNutrition.agregarCabecera(R.array.headerTable);
                                for(int i = 0; i < 4; i++) {
                                    ArrayList<String> elementos = new ArrayList<>();
                                    elementos.add(DateConvert.millisecondsToDateStringWithoutHours(totalDaysList.get(i).getTime()));
                                    elementos.add(df.format(totalDaysList.get(i).getKcal()));
                                    elementos.add(df.format(totalDaysList.get(i).getcH()));
                                    elementos.add(df.format(totalDaysList.get(i).getProt()));
                                    elementos.add(df.format(totalDaysList.get(i).getFat()));
                                    elementos.add(df.format(totalDaysList.get(i).getGluc()));

                                    tableNutrition.agregarFilaTabla(elementos);
                                }







                                //}

                            }else{
                                cvTableNutrition.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("PIECHARTMAIN: ", error.toString() );
                cvTableNutrition.setVisibility(View.INVISIBLE);
            }
        });


        Volley.newRequestQueue(getContext()).add(customRequest1);

    }





    @Override
    public void onResume() {
        super.onResume();
        if(Store.get().getEmail()==null){
            Intent int1 =  new Intent(getContext(), MainActivity.class);
            int1.putExtra("activity", "Main");
            Log.e("ONRESUME","Fragment Main");
            startActivity(int1);
        }
    }
}
