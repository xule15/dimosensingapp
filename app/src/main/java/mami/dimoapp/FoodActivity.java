package mami.dimoapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.model.FoodPoint;
import mami.dimoapp.model.Store;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class FoodActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText etDateNutrition, etTimeNutrition, etQuantity;
    private int mYear, mMonth, mDay, mHour, mMinute, when, day, month, year2, hour, minute2;
    private Spinner spKind, spTimes, spFood;
    private NumberPicker npHundreds, npTens, npUnits;
    private float grams;
    private String kindFood, aliment, quantity;
    private Button okButton, buttonAdd;
    private String name, email, personPhoto;
    private List<FoodPoint> datos;
    private RecyclerView rcFood;
    private RelativeLayout rlFoodList;
    private static final String URL_BASE = "http://mamilab.esi.uclm.es:5050/api/v2.0/consumptions";
    private ArrayList<String> foodList;

    private long time;
    private CustomRequest customRequest;
    private String fechaActual;
    private double breakfast;
    private double brunch;
    private double lunch;
    private double snack,calorias,proteinas,carbohidratos,fats,indiceGlucemico, totalcalorias,totalProteinas,totalCarbohidratos,totalFats,totalIG;
    private double dinner;
    private String date,nombre;
    private int points, totalPoints;
    private static final double max_breakfast=300; //20
    private static final double max_c_brunch=150; //10
    private static final double max_c_lunch=450; //30
    private static final double max_c_snack=225; //15
    private static final double max_c_dinner =375; //25
    private SearchableSpinner spkind2;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        email=Store.get().getEmail();
        breakfast=Store.get().getBreakfast();
        brunch=Store.get().getBrunch();
        lunch=Store.get().getLunch();
        snack =Store.get().getSnack();
        dinner=Store.get().getDinner();
        date=Store.get().getDate();
        points=Store.get().getdailyPoints();
        setContentView(R.layout.activity_food);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Registro alimentos");

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            //actionBar.setHomeAsUpIndicator(R.drawable.ic_close_dark);
        }

        //initialize date for the pickers
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        //initialize default date
        year2 = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute2 = c.get(Calendar.MINUTE);

        time = System.currentTimeMillis();
        grams = 70f;

        datos = new ArrayList<>();

        //Kind of food adapter
        spKind = (Spinner)findViewById(R.id.spinnerKind);



        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.kindOfFood, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spKind.setAdapter(adapter);

        spFood = (Spinner)findViewById(R.id.spinnerFood);




        spFood = (Spinner)findViewById(R.id.spinnerFood);
        rcFood = (RecyclerView)findViewById(R.id.foodList);

        //Kind of Times adapter
        spTimes = (Spinner)findViewById(R.id.spinnerTimes);
        ArrayAdapter<CharSequence> adapterTimes = ArrayAdapter.createFromResource(this,
                R.array.kindOfTimes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTimes.setAdapter(adapterTimes);

        okButton = (Button)findViewById(R.id.buttonOk);
        okButton.setOnClickListener(this);
        buttonAdd = (Button)findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(this);

        rlFoodList = (RelativeLayout)findViewById(R.id.rlFoodList);
        rlFoodList.setVisibility(View.GONE);

        etDateNutrition = (EditText) findViewById(R.id.editTextDateNutrition);
        etDateNutrition.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(FoodActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear,year))>=0){
                                    etDateNutrition.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    fechaActual=checkDate(dayOfMonth,monthOfYear+1,year);
                                    year2 = year;
                                    month = monthOfYear;
                                    day = dayOfMonth;

                                    time = DateConvert.valuesToMilliseconds(dayOfMonth, monthOfYear+1, year, hour, minute2, 0);

                                }else{
                                    Toast.makeText(getApplicationContext(),"La fecha introducida no debe ser posterior a la actual", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        etTimeNutrition = (EditText)findViewById(R.id.editTextTimeNutrition);

        etTimeNutrition.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TimePickerDialog tpd = new TimePickerDialog(FoodActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                etTimeNutrition.setText(checkTime(hourOfDay, minute));
                                hour = hourOfDay;
                                minute2 = minute;
                                time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2, 0)*1000000;
                            }
                        }, mHour, mMinute, true);

                tpd.show();
            }
        });

        spKind.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                kindFood = parent.getItemAtPosition(pos).toString();



                HashMap<Integer, Integer> hm = createHashMapFood();
                String url = "http://aplicaciondimo.com/getFoodByType.php?type=" + hm.get(pos);



                //Toast.makeText(getApplicationContext(), ""+URL, Toast.LENGTH_SHORT).show();

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        response = response.replace("][", ",");
                        if ((response.length() >= 0)) {
                            foodList = new ArrayList<String>();
                            try {

                                JSONArray ja = new JSONArray(response);
                                for(int i = 0; i<ja.length();i++){

                                    String food = ja.getString(i);
                                    foodList.add(food);
                                }
                                setAdapterFood();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

                queue.add(stringRequest);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        etQuantity = (EditText)findViewById(R.id.editTextGrams);
        etQuantity.setText("70");
        etQuantity.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(FoodActivity.this);

                alert.setTitle("Selecciona la cantidad: ");

                final LayoutInflater inflater = getLayoutInflater();
                final View viewGramsNp = inflater.inflate(R.layout.grams_picker,null);

                npHundreds = (NumberPicker)viewGramsNp.findViewById(R.id.npHundreds);
                npTens = (NumberPicker)viewGramsNp.findViewById(R.id.npTens);
                npUnits = (NumberPicker)viewGramsNp.findViewById(R.id.npUnits);

                npUnits.setMinValue(0);
                npUnits.setMaxValue(9);
                npHundreds.setMaxValue(9);
                npTens.setMaxValue(9);

                alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int hundreds = npHundreds.getValue();
                        int tens = npTens.getValue();
                        int units = npUnits.getValue();
                        if(npHundreds.getValue()==0 && npTens.getValue()==0){
                            etQuantity.setText(units+"");
                            grams = Float.parseFloat(units+"");
                        }else if(npHundreds.getValue()==0 && npTens.getValue()!=0){
                            etQuantity.setText(npTens.getValue() + "" + npUnits.getValue() + "");
                            grams = Float.parseFloat(npTens.getValue() + "" + npUnits.getValue() + "");
                        }else{
                            etQuantity.setText(hundreds+ "" + npTens.getValue() + "" + npUnits.getValue() + "");
                            grams = Float.parseFloat(npHundreds.getValue() + "" + npTens.getValue() + "" + npUnits.getValue() + "");
                        }
                    }
                });

                alert.setView(viewGramsNp);
                alert.show();
            }
        });

        iniciarFecha();
    }

    public void setAdapterFood(){

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                 android.R.layout.simple_spinner_item, foodList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);



        spFood.setAdapter(adapter);
    }
    public HashMap<Integer, Integer> createHashMapFood(){
        HashMap<Integer, Integer> hm = new HashMap<>();
        hm.put(0,6);
        hm.put(1,8);
        hm.put(2,1);
        hm.put(3,3);
        hm.put(4,9);
        hm.put(5,7);
        hm.put(6,10);
        hm.put(7,2);
        hm.put(8,5);
        hm.put(9,11);
        hm.put(10,4);
        hm.put(11,0);
        return hm;
    }

    private void iniciarFecha() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");

        etDateNutrition.setText(formatDate.format(c.getTime()));
        etTimeNutrition.setText(checkTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE)));
        //aqui tengo la fecha
        fechaActual=formatDate.format(c.getTime());
    }

    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    public String checkTime(int hour, int minute){
        String time = null;
        if(hour<10){
            time = "0"+hour;
        }else{
            time=""+hour;
        }
        if(minute<10){
            time = time + ":0"+minute;
        }else{
            time = time +":"+minute;
        }
        return time;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonAdd:
                aliment = spFood.getSelectedItem().toString();
                when = 0;
                if(spTimes.getSelectedItem().toString().equals("Desayuno"))when=0;
                if(spTimes.getSelectedItem().toString().equals("Almuerzo"))when=1;
                if(spTimes.getSelectedItem().toString().equals("Comida"))when=2;
                if(spTimes.getSelectedItem().toString().equals("Merienda"))when=3;
                if(spTimes.getSelectedItem().toString().equals("Cena"))when=4;
                if(spTimes.getSelectedItem().toString().equals("Cena ligera"))when=5;
                FoodPoint fp= new FoodPoint(aliment,when, time, grams);
                datos.add(fp);


                final FoodActivity.SimpleItemRecyclerViewAdapter adapter = new FoodActivity.SimpleItemRecyclerViewAdapter(datos);
                rcFood.setHasFixedSize(true);
                rcFood.setAdapter(adapter);
                rcFood.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
                rlFoodList.setVisibility(View.VISIBLE);
                getDataAlimentsSelected();
                break;
            case R.id.buttonOk:

               // makeRequestBD("https://aplicaciondimo.000webhostapp.com/setComsumptions.php?email="+email+"&start="+time+"&calorias="+totalcalorias+"&carbohidratos="+totalCarbohidratos+"&fats="+totalFats+"&indice="+totalIG+"&proteinas="+totalProteinas);
                makeRequestBD("http://aplicaciondimo.com/setComsumptions.php?email="+email+"&start="+time+"&calories="+totalcalorias+"&carbohidrates="+totalCarbohidratos+"&fats="+totalFats+"&index="+totalIG+"&proteins="+totalProteinas+"&when="+when);
                Store.get().setdailyPoints(Store.get().getdailyPoints()+5);
                makeRequestBD("http://aplicaciondimo.com/updateUser.php?email=" +Store.get().getEmail()+"&dailyPoints="+Store.get().getdailyPoints()+"&weeklyPoints="+Store.get().getweeklyPoints()+"&controlDay1="+Store.get().getControlDay1()+
                        "&controlDay2="+Store.get().getControlDay2()+"&controlDay3="+Store.get().getControlDay3()
                        +"&controlDay4="+Store.get().getControlDay4()+"&controlDay5="+Store.get().getControlDay5()+"&controlDay6="+Store.get().getControlDay6()+
                        "&controlDay7="+Store.get().getControlDay7()+ "&category="+Store.get().getCategoria());
                Toast.makeText(getApplicationContext(),"Consumición registrada", Toast.LENGTH_SHORT).show();

                Intent intApp = new Intent(this, MainActivity.class);
                startActivity(intApp);




                break;
        }
    }

    private void getDataAlimentsSelected(){
        JSONObject attributes = new JSONObject();
        JSONObject consumptions = new JSONObject();
        JSONObject meta = new JSONObject();
        JSONObject consumedFoods = new JSONObject();
        JSONObject relationships = new JSONObject();
        JSONObject data = new JSONObject();
        JSONArray data2 = new JSONArray();
        JSONObject dataAliment = new JSONObject();

        try {
            attributes.put("time", when);

            for(int i=0; i<datos.size();i++){
                FoodPoint pg = datos.get(i);
                dataAliment = new JSONObject();
                meta = new JSONObject();
                dataAliment.put("type","foods");
                dataAliment.put("id",pg.getAliment());
                //getDataAliment("https://aplicaciondimo.000webhostapp.com/consultaComidaByNombre.php?nombre="+pg.getAliment(),pg.getGrams());
                getDataAliment("http://aplicaciondimo.com/getFoodByName.php?name="+pg.getAliment(),pg.getGrams());

                meta.put("consumed-grams", pg.getGrams());

                dataAliment.put("meta", meta);
                data2.put(i, dataAliment);
            }
            relationships.put("consumed-foods", consumedFoods);
            consumedFoods.put("data", data2);

            consumptions.put("type", "consumptions");
            consumptions.put("id", time);
            consumptions.put("attributes", attributes);
            consumptions.put("relationships", relationships);
            data.put("data", consumptions);



        } catch (JSONException e) {
            e.printStackTrace();
        }


        customRequest = new CustomRequest(Request.Method.POST, URL_BASE, data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {



                            try {
                                JSONObject appointment = (JSONObject) response.get("data");
                               JSONObject time = (JSONObject) appointment.get("attributes");
                                    double calories = (double) time.get("total-calories");

                                if(spTimes.getSelectedItem().toString().equals("Desayuno")){
                                    double caloriesTotales=calories+breakfast;
                                    if(fechaActual.equals(date)&& caloriesTotales<=max_breakfast) {
                                        totalPoints=points+10;
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateCalorias.php?email="+email+"&desayuno="+caloriesTotales+"&comida="+lunch+"&merienda="+ snack +"&cena="+dinner+"&fecha="+fechaActual+"&almuerzo="+brunch);
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateUsuarios.php?email="+email+"&puntos="+totalPoints);
                                    }else if(fechaActual.equals(date)&& caloriesTotales>max_breakfast){
                                        totalPoints=points-5;
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateCalorias.php?email="+email+"&desayuno="+caloriesTotales+"&comida="+lunch+"&merienda="+ snack +"&cena="+dinner+"&fecha="+fechaActual+"&almuerzo="+brunch);
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateUsuarios.php?email="+email+"&puntos="+totalPoints);
                                    }


                                }

                                if(spTimes.getSelectedItem().toString().equals("Almuerzo")){
                                    double caloriesTotales=calories+brunch;
                                    if(fechaActual.equals(date)&& caloriesTotales<= max_c_brunch) {
                                        totalPoints=points+10;
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateCalorias.php?email="+email+"&desayuno="+breakfast+"&comida="+lunch+"&merienda="+ snack +"&cena="+dinner+"&fecha="+fechaActual+"&almuerzo="+caloriesTotales);
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateUsuarios.php?email="+email+"&puntos="+totalPoints);
                                    }else if(fechaActual.equals(date)&& caloriesTotales>max_breakfast){
                                        totalPoints=points-5;
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateCalorias.php?email="+email+"&desayuno="+breakfast+"&comida="+lunch+"&merienda="+ snack +"&cena="+dinner+"&fecha="+fechaActual+"&almuerzo="+caloriesTotales);
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateUsuarios.php?email="+email+"&puntos="+totalPoints);
                                    }


                                }

                                if(spTimes.getSelectedItem().toString().equals("Comida")){
                                    double caloriesTotales=calories+lunch;
                                    if(fechaActual.equals(date)&& caloriesTotales<= max_c_lunch) {
                                        totalPoints=points+10;
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateCalorias.php?email="+email+"&desayuno="+breakfast+"&comida="+caloriesTotales+"&merienda="+ snack +"&cena="+dinner+"&fecha="+fechaActual+"&almuerzo="+brunch);
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateUsuarios.php?email="+email+"&puntos="+totalPoints);
                                    }else if(fechaActual.equals(date)&& caloriesTotales>max_breakfast){
                                        totalPoints=points-5;
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateCalorias.php?email="+email+"&desayuno="+breakfast+"&comida="+caloriesTotales+"&merienda="+ snack +"&cena="+dinner+"&fecha="+fechaActual+"&almuerzo="+brunch);
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateUsuarios.php?email="+email+"&puntos="+totalPoints);
                                    }


                                }

                                if(spTimes.getSelectedItem().toString().equals("Merienda")){
                                    double caloriesTotales=calories+ snack;
                                    if(fechaActual.equals(date)&& caloriesTotales<= max_c_snack) {
                                        totalPoints=points+10;
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateCalorias.php?email="+email+"&desayuno="+breakfast+"&comida="+lunch+"&merienda="+caloriesTotales+"&cena="+dinner+"&fecha="+fechaActual+"&almuerzo="+brunch);
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateUsuarios.php?email="+email+"&puntos="+totalPoints);
                                    }else if(fechaActual.equals(date)&& caloriesTotales>max_breakfast){
                                        totalPoints=points-5;
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateCalorias.php?email="+email+"&desayuno="+breakfast+"&comida="+lunch+"&merienda="+caloriesTotales+"&cena="+dinner+"&fecha="+fechaActual+"&almuerzo="+brunch);
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateUsuarios.php?email="+email+"&puntos="+totalPoints);
                                    }


                                }

                                if(spTimes.getSelectedItem().toString().equals("Cena")){
                                    double caloriesTotales=calories+dinner;
                                    if(fechaActual.equals(date)&& caloriesTotales<= max_c_dinner) {
                                        totalPoints=points+10;
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateCalorias.php?email="+email+"&desayuno="+breakfast+"&comida="+lunch+"&merienda="+ snack +"&cena="+caloriesTotales+"&fecha="+fechaActual+"&almuerzo="+brunch);
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateUsuarios.php?email="+email+"&puntos="+totalPoints);
                                    }else if(fechaActual.equals(date)&& caloriesTotales>max_breakfast){
                                        totalPoints=points-5;
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateCalorias.php?email="+email+"&desayuno="+breakfast+"&comida="+lunch+"&merienda="+ snack +"&cena="+caloriesTotales+"&fecha="+fechaActual+"&almuerzo="+brunch);
                                        makeRequestBD("https://dimoapp.000webhostapp.com/updateUsuarios.php?email="+email+"&puntos="+totalPoints);
                                    }


                                }
                               // }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }



                        ////////////aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
                        Toast.makeText(getApplicationContext(),"Consumición registrada", Toast.LENGTH_SHORT).show();
                        datos = new ArrayList<>();
                        final FoodActivity.SimpleItemRecyclerViewAdapter adapter = new FoodActivity.SimpleItemRecyclerViewAdapter(datos);
                        rcFood.setHasFixedSize(true);
                        rcFood.setAdapter(adapter);
                        rcFood.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false));
                        rlFoodList.setVisibility(View.INVISIBLE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Food-ERROR:" ,error.toString());
                    }
                });

        //customRequest.setCookies(cookies);
        Volley.newRequestQueue(this).add(customRequest);

    }

    public void makeRequestBD(String URL){

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");
                if ( (response.length()>0)){
                    try {

                        JSONArray ja = new JSONArray(response);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    public void getDataAliment(String URL, final double grams){



        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");

                try {


                    JSONArray ja = new JSONArray(response);
                    nombre=ja.getString(0);
                    calorias=ja.getDouble(1);
                    proteinas=ja.getDouble(2);
                    carbohidratos=ja.getDouble(3);
                    fats=ja.getDouble(4);
                    indiceGlucemico=ja.getDouble(5);
                    totalcalorias=totalcalorias+((grams*calorias)/100);
                    totalProteinas=totalProteinas+((grams*proteinas)/100);
                    totalCarbohidratos=totalCarbohidratos+((grams*carbohidratos)/100);
                    totalFats=totalFats+((grams*fats)/100);
                    totalIG=totalIG+((grams*indiceGlucemico)/100);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //}


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    //Adapter para RecyclerView
    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<FoodActivity.SimpleItemRecyclerViewAdapter.ViewHolder> {

        private List<FoodPoint> datos;

        public SimpleItemRecyclerViewAdapter(List<FoodPoint> datos) {
            this.datos = datos;
        }

        @Override
        public FoodActivity.SimpleItemRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_view_food, parent, false);
            FoodActivity.SimpleItemRecyclerViewAdapter.ViewHolder tvh = new FoodActivity.SimpleItemRecyclerViewAdapter.ViewHolder(view);
            return tvh;
        }

        @Override
        public void onBindViewHolder(FoodActivity.SimpleItemRecyclerViewAdapter.ViewHolder holder, final int position) {
            holder.tvFoodName.setText(datos.get(position).getAliment());
            holder.tvMoment.setText(spTimes.getSelectedItem().toString());
            holder.tvGrams.setText(datos.get(position).getGrams()+" gramos");
            holder.itemView.setTag(0);
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener(){
                @Override
                public boolean onLongClick(View view) {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(FoodActivity.this);
                    builder1.setMessage("¿Deseas eliminar el alimento?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            datos.remove(position);
                            final FoodActivity.SimpleItemRecyclerViewAdapter adapter = new FoodActivity.SimpleItemRecyclerViewAdapter(datos);
                            rcFood.setHasFixedSize(true);
                            rcFood.setAdapter(adapter);
                            rcFood.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false));
                            Toast.makeText(getApplicationContext(), "Alimento eliminado", Toast.LENGTH_SHORT).show();
                            dialog.cancel();
                        }
                    });

                    builder1.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                    return false;
                }
            });
        }

        @Override
        public int getItemCount() {
            return datos.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public final TextView tvFoodName;
            public final TextView tvMoment;
            public final TextView tvGrams;

            public ViewHolder(View view) {
                super(view);

                tvFoodName = (TextView)view.findViewById(R.id.AppointmentTitle);
                tvMoment = (TextView)view.findViewById(R.id.tvMoment);
                tvGrams = (TextView)view.findViewById(R.id.tvGrams);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_appointment, menu);

        //setBadgeCount(this, mCartMenuIcon, ""+ 1);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            NavUtils.navigateUpTo(this, intent);
            return true;
        }
        if(id == R.id.action_Appointment_help) {
            ShowcaseConfig config = new ShowcaseConfig();
            MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this);
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)

                            .setTarget(etDateNutrition)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_DateFood))
                            .build()
            );
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(etTimeNutrition)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_HourFood))
                            .build()
            );
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(spKind)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_spKind))
                            .build()
            );
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(spFood)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_spFood))
                            .build()
            );
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(spTimes)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_spTimesFood))
                            .build()
            );


            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(etQuantity)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_QuantityFood))
                            .build()
            );

            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(buttonAdd)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_Save))
                            .build()
            );





            sequence.start();


        }
        return super.onOptionsItemSelected(item);
    }


}
