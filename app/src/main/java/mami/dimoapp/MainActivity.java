package mami.dimoapp;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mami.dimoapp.custom.BadgeDrawable;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.CustomRequest2;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.fragments.FragmentMain;
import mami.dimoapp.fragments.FragmentProfile;
import mami.dimoapp.model.Appointment;
import mami.dimoapp.model.Award;
import mami.dimoapp.model.Store;
import mami.dimoapp.services.ActivityRecognizedService;
import mami.dimoapp.services.LocationService;
import mami.dimoapp.services.StepService;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener{

    private static final int REQUEST_OAUTH = 1;
    private static final String AUTH_PENDING = "auth_state_pending";
    private boolean authInProgress = false;
    private GoogleApiClient mApiClient;
    private TextView tvName, tvMail;
    private Button btnSignout;
    private FloatingActionMenu fabmenu;
    private ImageView imageViewHeader;
    private boolean cerrarSesion;
    private NestedScrollView scMain;

    FloatingActionButton btnGlucose, btnWeight, btnNutrition, btnInsuline, btnHeart,btnfab;
    Chronometer chrono;
    enum ActivityType { RUN, WALK, STILL};
    FragmentManager fragmentManager = getSupportFragmentManager();
    private Toolbar toolbar;
    ActionBar actionBar;
    DrawerLayout drawer;
    private String name, surname, email, personPhoto,country;
    private Intent intentStep;
    private Boolean exit = false;

    private LayerDrawable mCartMenuIcon;

    private GoogleApiClient googleApiClient;
    private  boolean existe=false;
    private List<Award> datos;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);



        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        Log.e("ONCREATE","Fragment MainActivity");



       setToolbar("DiMo Sensing App");




        //Navigation Drawer header
        NavigationView navView = (NavigationView) findViewById(R.id.nav_view);
        navView.setNavigationItemSelectedListener(this);
        navView.getMenu().getItem(0).setChecked(true);
        View headerview = navView.getHeaderView(0);
        name = Store.get().getName();
        surname = Store.get().getSurname();
        email = Store.get().getEmail();
        country=Store.get().getCountry();
        personPhoto = Store.get().getPersonPhoto();
        imageViewHeader = (ImageView)headerview.findViewById(R.id.imageViewHeader);
        tvName = (TextView)headerview.findViewById(R.id.textViewName);
        tvMail = (TextView)headerview.findViewById(R.id.textViewMail);
        Picasso.with(this).load(personPhoto).into(imageViewHeader);
        tvName.setText(name+surname);
        tvMail.setText(email);
        cerrarSesion = false;
        btnSignout = (Button)headerview.findViewById(R.id.buttonSignout);
        btnSignout.setOnClickListener(this);

        scMain = (NestedScrollView) findViewById(R.id.scMain);




        //DrawerLayout
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        btnGlucose = (FloatingActionButton) findViewById(R.id.glucose_action);
        btnWeight = (FloatingActionButton)findViewById(R.id.weight_action);
        btnNutrition = (FloatingActionButton)findViewById(R.id.food_action);
        btnInsuline = (FloatingActionButton)findViewById(R.id.insuline_action);
        btnHeart  = (FloatingActionButton)findViewById(R.id.heart_action);
        btnfab  = (FloatingActionButton)findViewById(R.id.menufab_action);
        fabmenu=(FloatingActionMenu) findViewById(R.id.menufab);
        btnGlucose.setOnClickListener(this);
        btnWeight.setOnClickListener(this);
        btnNutrition.setOnClickListener(this);
        btnInsuline.setOnClickListener(this);
        btnHeart.setOnClickListener(this);





        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new FragmentMain())
                .commit();

        getInfo();

        if (checkInternetConnection()) {
            //If Singleton become null
            if (Store.get().getName() == null) {
                Store.get().setEnableActivity(true);
            }
        }

        if (savedInstanceState != null) {
            authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        }
        Log.e("ONCREATE", "FragmentProfile");
        if(Store.get().isEnableActivity()) {
            //Start ActivityRecognition and LocationService
            mApiClient = new GoogleApiClient.Builder(this)
                    .addApi(ActivityRecognition.API)
                    .addApi(LocationServices.API)
                    .addApi(Fitness.SENSORS_API)
                    .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }




        //TODO do services
        intentStep = new Intent(this, StepService.class);
        startService(intentStep);




    }






   public void Save(String URL){



        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                if ( (response.length()>0)){
                    try {
                        JSONArray ja = new JSONArray(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        queue.add(stringRequest);
    }

    public void getInfoUser(String URL){

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");


                    try {
                        JSONArray ja = new JSONArray(response);
                        String rank= ja.getString(11);
                        String[] max= rank.split("-");
                        Store.get().setdailyPoints((Integer) ja.getInt(3));
                        Store.get().setweeklyPoints((Integer) ja.getInt(4));
                        Store.get().setMaxGlucose(Float.parseFloat(max[1]));
                        Store.get().setMinGlucose(Float.parseFloat(max[0]));
                        Store.get().setControlDay1(ja.getInt(13));
                        Store.get().setControlDay2(ja.getInt(14));
                        Store.get().setControlDay3(ja.getInt(15));
                        Store.get().setControlDay4(ja.getInt(16));
                        Store.get().setControlDay5(ja.getInt(17));
                        Store.get().setControlDay6(ja.getInt(18));
                        Store.get().setControlDay7(ja.getInt(19));





                        getCalories("http://aplicaciondimo.com/getCalories.php?email="+email);
                        getExchange("http://aplicaciondimo.com/getExchanged.php?idUser="+email);
                        checkCategory();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                //Si el usuario no existe en la bd, lo registramos tanto en la tabla de usuario como en la de calorias, con valores por defecto
                if(response.length()==0){
                    Save("http://aplicaciondimo.com/setUser.php?email="+email+"&name="+name.replace(" ","_")+"&surname="+surname+"&dailyPoints=0"+"&weeklyPoints=0"+"country="+country);
                    Save("http://aplicaciondimo.com/setCalories.php?email="+email+"&breakfast=0.0&brunch=0.0&lunch=0.0&snack=0.0&dinner=0.0&date=0000-00-00");
                    }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }


    public void getCalories(String URL){

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setBreakfast(ja.getDouble(2));
                    Store.get().setBrunch(ja.getDouble(3));
                    Store.get().setLunch(ja.getDouble(4));
                    Store.get().setSnack(ja.getDouble(5));
                    Store.get().setDinner(ja.getDouble(6));
                    Store.get().setDate(ja.getString(7));

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getExchange(String url){


        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENExchange","Response is: "+ jsonArray.toString());
                        datos = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject award = (JSONObject) jsonArray1.get(i);
                                    int idaward=award.getInt("idAward");
                                    switch(idaward) {
                                        case 1:
                                            Store.get().setExchange1(true);
                                            break;
                                        case 2:
                                            Store.get().setExchange2(true);
                                            break;

                                        case 3:
                                            Store.get().setExchange3(true);
                                            break;
                                        case 4:
                                            Store.get().setExchange4(true);
                                            break;

                                        case 5:
                                            Store.get().setExchange5(true);
                                            break;
                                        case 6:
                                            Store.get().setExchange6(true);
                                            break;

                                        case 7:
                                            Store.get().setExchange7(true);
                                            break;
                                        case 8:
                                            Store.get().setExchange8(true);
                                            break;

                                        case 9:
                                            Store.get().setExchange9(true);
                                            break;
                                        case 10:
                                            Store.get().setExchange10(true);
                                            break;

                                        case 11:
                                            Store.get().setExchange11(true);
                                            break;
                                        case 12:
                                            Store.get().setExchange12(true);
                                            break;

                                        case 13:
                                            Store.get().setExchange13(true);
                                            break;
                                        case 14:
                                            Store.get().setExchange14(true);
                                            break;

                                        case 15:
                                            Store.get().setExchange15(true);
                                            break;
                                        case 16:
                                            Store.get().setExchange16(true);
                                            break;
                                        case 17:
                                            Store.get().setExchange17(true);
                                            break;
                                        case 18:
                                            Store.get().setExchange18(true);
                                            break;
                                        case 19:
                                            Store.get().setExchange19(true);
                                            break;
                                        case 20:
                                            Store.get().setExchange20(true);
                                            break;
                                        case 21:
                                            Store.get().setExchange21(true);
                                            break;
                                        case 22:
                                            Store.get().setExchange22(true);
                                            break;
                                        case 23:
                                            Store.get().setExchange23(true);
                                            break;
                                        case 24:
                                            Store.get().setExchange24(true);
                                            break;
                                        case 25:
                                            Store.get().setExchange25(true);
                                            break;
                                        default:

                                    }


                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errorPremio ","errorPremio "+ error.toString() );

            }
        });

        Volley.newRequestQueue(getApplicationContext()).add(customRequest);
    }
    public void checkCategory() {
        if (Store.get().getdailyPoints() < 50 && Store.get().getCategoria() != 1 && Store.get().getCategoria() !=2) {
            Store.get().setCategoria(0);
            makeRequestBD("http://aplicaciondimo.com/updateUser.php?email=" + Store.get().getEmail() + "&dailyPoints=" + Store.get().getdailyPoints() + "&weeklyPoints=" + Store.get().getweeklyPoints() + "&controlDay1=" + Store.get().getControlDay1() +
                    "&controlDay2=" + Store.get().getControlDay2() + "&controlDay3=" + Store.get().getControlDay3()
                    + "&controlDay4=" + Store.get().getControlDay4() + "&controlDay5=" + Store.get().getControlDay5() + "&controlDay6=" + Store.get().getControlDay6() +
                    "&controlDay7=" + Store.get().getControlDay7() + "&category=0");

        } else if (Store.get().getdailyPoints() < 100 && Store.get().getdailyPoints() > 50 && Store.get().getCategoria()== 0) {
            makeRequestBD("http://aplicaciondimo.com/updateUser.php?email=" + Store.get().getEmail() + "&dailyPoints=" + Store.get().getdailyPoints() + "&weeklyPoints=" + Store.get().getweeklyPoints() + "&controlDay1=" + Store.get().getControlDay1() +
                    "&controlDay2=" + Store.get().getControlDay2() + "&controlDay3=" + Store.get().getControlDay3()
                    + "&controlDay4=" + Store.get().getControlDay4() + "&controlDay5=" + Store.get().getControlDay5() + "&controlDay6=" + Store.get().getControlDay6() +
                    "&controlDay7=" + Store.get().getControlDay7() + "&category=1");
            Store.get().setCategoria(1);
        } else if (Store.get().getdailyPoints() >= 100 && Store.get().getCategoria()==1) {
            makeRequestBD("http://aplicaciondimo.com/updateUser.php?email=" + Store.get().getEmail() + "&dailyPoints=" + Store.get().getdailyPoints() + "&weeklyPoints=" + Store.get().getweeklyPoints() + "&controlDay1=" + Store.get().getControlDay1() +
                    "&controlDay2=" + Store.get().getControlDay2() + "&controlDay3=" + Store.get().getControlDay3()
                    + "&controlDay4=" + Store.get().getControlDay4() + "&controlDay5=" + Store.get().getControlDay5() + "&controlDay6=" + Store.get().getControlDay6() +
                    "&controlDay7=" + Store.get().getControlDay7() + "&category=2");

            Store.get().setCategoria(2);

        }
    }




    public void getAward(String url){


        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetAppointment","Response is: "+ jsonArray.toString());
                        datos = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject award = (JSONObject) jsonArray1.get(i);
                                    switch(i) {
                                        case 0:
                                            Store.get().setDescriptionDailyP1Bronze((String) award.get("description"));
                                            Store.get().setpDailyP1Bronze(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP1DailyBronze((String)award.get("type"));
                                            Store.get().setLinkP1DailyBronze((String) award.get("link"));
                                            Store.get().setIdP1DailyBronze(Integer.parseInt((String) award.get("id_award")));
                                            break;
                                        case 1:
                                            Store.get().setDescriptionDailyP2Bronze((String) award.get("description"));
                                            Store.get().setpDailyP2Bronze(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP2DailyBronze((String)award.get("type"));
                                            Store.get().setLinkP2DailyBronze((String) award.get("link"));
                                            Store.get().setIdP2DailyBronze(Integer.parseInt((String) award.get("id_award")));
                                            break;
                                        case 2:
                                            Store.get().setDescriptionDailyP3Bronze((String) award.get("description"));
                                            Store.get().setpDailyP3Bronze(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP3DailyBronze((String)award.get("type"));
                                            Store.get().setLinkP3DailyBronze((String) award.get("link"));
                                            Store.get().setIdP3DailyBronze(Integer.parseInt((String) award.get("id_award")));
                                            break;
                                        case 3:
                                            Store.get().setDescriptionDailyP4Bronze((String) award.get("description"));
                                            Store.get().setpDailyP4Bronze(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP4DailyBronze((String)award.get("type"));
                                            Store.get().setLinkP4DailyBronze((String) award.get("link"));
                                            Store.get().setIdP4DailyBronze(Integer.parseInt((String) award.get("id_award")));
                                            break;
                                        case 4:
                                            Store.get().setDescriptionDailyP1Silver((String) award.get("description"));
                                            Store.get().setpDailyP1Silver(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP1DailySilver((String)award.get("type"));
                                            Store.get().setLinkP1DailySilver((String) award.get("link"));
                                            Store.get().setIdP1DailySilver(Integer.parseInt((String) award.get("id_award")));
                                            break;
                                        case 5:
                                            Store.get().setDescriptionDailyP2Silver((String) award.get("description"));
                                            Store.get().setpDailyP2Silver(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP2DailySilver((String)award.get("type"));
                                            Store.get().setLinkP2DailySilver((String) award.get("link"));
                                            Store.get().setIdP2DailySilver(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 6:
                                            Store.get().setDescriptionDailyP3Silver((String) award.get("description"));
                                            Store.get().setpDailyP3Silver(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP3DailySilver((String)award.get("type"));
                                            Store.get().setLinkP3DailySilver((String) award.get("link"));
                                            Store.get().setIdP3DailySilver(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 7:
                                            Store.get().setDescriptionDailyP4Silver((String) award.get("description"));
                                            Store.get().setpDailyP4Silver(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP4DailySilver((String)award.get("type"));
                                            Store.get().setLinkP4DailySilver((String) award.get("link"));
                                            Store.get().setIdP4DailySilver(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 8:
                                            Store.get().setDescriptionDailyP1Gold((String) award.get("description"));
                                            Store.get().setpDailyP1Gold(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP1DailyGold((String)award.get("type"));
                                            Store.get().setLinkP1DailyGold((String) award.get("link"));
                                            Store.get().setIdP1DailyGold(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 9:
                                            Store.get().setDescriptionDailyP2Gold((String) award.get("description"));
                                            Store.get().setpDailyP2Gold(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP2DailyGold((String)award.get("type"));
                                            Store.get().setLinkP2DailyGold((String) award.get("link"));
                                            Store.get().setIdP2DailyGold(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 10:
                                            Store.get().setDescriptionDailyP3Gold((String) award.get("description"));
                                            Store.get().setpDailyP3Gold(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP3DailyGold((String)award.get("type"));
                                            Store.get().setLinkP3DailyGold((String) award.get("link"));
                                            Store.get().setIdP3DailyGold(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 11:
                                            Store.get().setDescriptionDailyP4Gold((String) award.get("description"));
                                            Store.get().setpDailyP4Gold(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP4DailyGold((String)award.get("type"));
                                            Store.get().setLinkP4DailyGold((String) award.get("link"));
                                            Store.get().setIdP4DailyGold(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 12:
                                            Store.get().setDescriptionWeeklyP1Bronze((String) award.get("description"));
                                            Store.get().setpWeeklyP1Bronze(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP1WeeklyBronze((String)award.get("type"));
                                            Store.get().setLinkP1WeeklyBronze((String) award.get("link"));
                                            Store.get().setIdP1WeeklyBronze(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 13:
                                            Store.get().setDescriptionWeeklyP2Bronze((String) award.get("description"));
                                            Store.get().setpWeeklyP2Bronze(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP2WeeklyBronze((String)award.get("type"));
                                            Store.get().setLinkP2WeeklyBronze((String) award.get("link"));
                                            Store.get().setIdP2WeeklyBronze(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 14:
                                            Store.get().setDescriptionWeeklyP3Bronze((String) award.get("description"));
                                            Store.get().setpWeeklyP3Bronze(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP3WeeklyBronze((String)award.get("type"));
                                            Store.get().setLinkP3WeeklyBronze((String) award.get("link"));
                                            Store.get().setIdP3WeeklyBronze(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 15:
                                            Store.get().setDescriptionWeeklyP4Bronze((String) award.get("description"));
                                            Store.get().setpWeeklyP4Bronze(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP4WeeklyBronze((String)award.get("type"));
                                            Store.get().setLinkP4WeeklyBronze((String) award.get("link"));
                                            Store.get().setIdP4WeeklyBronze(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 16:
                                            Store.get().setDescriptionWeeklyP1Silver((String) award.get("description"));
                                            Store.get().setpWeeklyP1Silver(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP1DailySilver((String)award.get("type"));
                                            Store.get().setLinkP1WeeklySilver((String) award.get("link"));
                                            Store.get().setIdP1WeeklySilver(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 17:
                                            Store.get().setDescriptionWeeklyP2Silver((String) award.get("description"));
                                            Store.get().setpWeeklyP2Silver(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP2DailySilver((String)award.get("type"));
                                            Store.get().setLinkP2WeeklySilver((String) award.get("link"));
                                            Store.get().setIdP2WeeklySilver(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 18:
                                            Store.get().setDescriptionWeeklyP3Silver((String) award.get("description"));
                                            Store.get().setpWeeklyP3Silver(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP3DailySilver((String)award.get("type"));
                                            Store.get().setLinkP3WeeklySilver((String) award.get("link"));
                                            Store.get().setIdP3WeeklySilver(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 19:
                                            Store.get().setDescriptionWeeklyP4Silver((String) award.get("description"));
                                            Store.get().setpWeeklyP4Silver(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP4DailySilver((String)award.get("type"));
                                            Store.get().setLinkP4WeeklySilver((String) award.get("link"));
                                            Store.get().setIdP4WeeklySilver(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 20:
                                            Store.get().setDescriptionWeeklyP1Gold((String) award.get("description"));
                                            Store.get().setpWeeklyP1Gold(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP1WeeklyGold((String)award.get("type"));
                                            Store.get().setLinkP1WeeklyGold((String) award.get("link"));
                                            Store.get().setIdP1WeeklyGold(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 21:
                                            Store.get().setDescriptionWeeklyP2Gold((String) award.get("description"));
                                            Store.get().setpWeeklyP2Gold(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP2WeeklyGold((String)award.get("type"));
                                            Store.get().setLinkP2WeeklyGold((String) award.get("link"));
                                            Store.get().setIdP2WeeklyGold(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 22:
                                            Store.get().setpWeeklyP3Gold(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setpWeeklyP3Gold(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP3WeeklyGold((String)award.get("type"));
                                            Store.get().setLinkP3WeeklyGold((String) award.get("link"));
                                            Store.get().setIdP3WeeklyGold(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        case 23:
                                            Store.get().setDescriptionWeeklyP4Gold((String) award.get("description"));
                                            Store.get().setpWeeklyP4Gold(Integer.parseInt((String) award.get("pNeeded")));
                                            Store.get().setTypeP4WeeklyGold((String)award.get("type"));
                                            Store.get().setLinkP4WeeklyGold((String) award.get("link"));
                                            Store.get().setIdP4WeeklyGold(Integer.parseInt((String) award.get("id_award")));

                                            break;
                                        default:

                                    }


                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errorPremio ","errorPremio "+ error.toString() );

            }
        });

        Volley.newRequestQueue(getApplicationContext()).add(customRequest);
    }
  /*  public void getAward1(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP1Bronze(ja.getString(1));
                    Store.get().setpDailyP1Bronze(ja.getInt(2));
                    Store.get().setLinkP1DailyBronze(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    public void getAward2(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP2Bronze(ja.getString(1));
                    Store.get().setpDailyP2Bronze(ja.getInt(2));
                    Store.get().setLinkP2DailyBronze(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    public void getAward3(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP3Bronze(ja.getString(1));
                    Store.get().setpDailyP3Bronze(ja.getInt(2));
                    Store.get().setLinkP3DailyBronze(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward4(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP4Bronze(ja.getString(1));
                    Store.get().setpDailyP4Bronze(ja.getInt(2));
                    Store.get().setLinkP4DailyBronze(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward5(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP1Silver(ja.getString(1));
                    Store.get().setpDailyP1Silver(ja.getInt(2));
                    Store.get().setLinkP1DailySilver(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward6(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP2Silver(ja.getString(1));
                    Store.get().setpDailyP2Silver(ja.getInt(2));
                    Store.get().setLinkP2DailySilver(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward7(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP3Silver(ja.getString(1));
                    Store.get().setpDailyP3Silver(ja.getInt(2));
                    Store.get().setLinkP3DailySilver(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward8(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP4Silver(ja.getString(1));
                    Store.get().setpDailyP4Silver(ja.getInt(2));
                    Store.get().setLinkP4DailySilver(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    public void getAward9(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP1Gold(ja.getString(1));
                    Store.get().setpDailyP1Gold(ja.getInt(2));
                    Store.get().setLinkP1DailyGold(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward10(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP2Gold(ja.getString(1));
                    Store.get().setpDailyP2Gold(ja.getInt(2));
                    Store.get().setLinkP2DailyGold(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward11(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP3Gold(ja.getString(1));
                    Store.get().setpDailyP3Gold(ja.getInt(2));
                    Store.get().setLinkP3DailyGold(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward12(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionDailyP4Gold(ja.getString(1));
                    Store.get().setpDailyP4Gold(ja.getInt(2));
                    Store.get().setLinkP4DailyGold(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward13(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP1Bronze(ja.getString(1));
                    Store.get().setpWeeklyP1Bronze(ja.getInt(2));
                    Store.get().setLinkP1WeeklyBronze(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward14(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP2Bronze(ja.getString(1));
                    Store.get().setpWeeklyP2Bronze(ja.getInt(2));
                    Store.get().setLinkP2WeeklyBronze(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward15(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP3Bronze(ja.getString(1));
                    Store.get().setpWeeklyP3Bronze(ja.getInt(2));
                    Store.get().setLinkP3WeeklyBronze(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward16(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP4Bronze(ja.getString(1));
                    Store.get().setpWeeklyP4Bronze(ja.getInt(2));
                    Store.get().setLinkP4WeeklyBronze(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward17(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP1Silver(ja.getString(1));
                    Store.get().setpWeeklyP1Silver(ja.getInt(2));
                    Store.get().setLinkP1WeeklySilver(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    public void getAward18(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP2Silver(ja.getString(1));
                    Store.get().setpWeeklyP2Silver(ja.getInt(2));
                    Store.get().setLinkP2WeeklySilver(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    public void getAward19(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP3Silver(ja.getString(1));
                    Store.get().setpWeeklyP3Silver(ja.getInt(2));
                    Store.get().setLinkP3WeeklySilver(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward20(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP4Silver(ja.getString(1));
                    Store.get().setpWeeklyP4Silver(ja.getInt(2));
                    Store.get().setLinkP4WeeklySilver(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    public void getAward21(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP1Gold(ja.getString(1));
                    Store.get().setpWeeklyP1Gold(ja.getInt(2));
                    Store.get().setLinkP1WeeklyGold(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    public void getAward22(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP2Gold(ja.getString(1));
                    Store.get().setpWeeklyP2Gold(ja.getInt(2));
                    Store.get().setLinkP2WeeklyGold(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    public void getAward23(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP3Gold(ja.getString(1));
                    Store.get().setpWeeklyP3Gold(ja.getInt(2));
                    Store.get().setLinkP3WeeklyGold(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }
    public void getAward24(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("][",",");
                try {
                    JSONArray ja = new JSONArray(response);
                    Store.get().setDescriptionWeeklyP4Gold(ja.getString(1));
                    Store.get().setpWeeklyP4Gold(ja.getInt(2));
                    Store.get().setLinkP4WeeklyGold(ja.getString(5));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }


*/






    private void getInfo() {
            getAward("http://aplicaciondimo.com/getAward.php");
            String query = "http://aplicaciondimo.com/getUser.php?email="+email;
            getInfoUser(query);


    }


   private void goLogInScreen() {
       Intent intent = new Intent(this, SignInActivity.class);
       intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

       startActivity(intent);


   }

    public void logOut(View view) {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess()) {
                    goLogInScreen();
                } else {
                    //Toast.makeText(getApplicationContext(), R.string.not_close_session, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void makeRequestBD(String URL){

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");
                if ( (response.length()>0)){
                    try {

                        JSONArray ja = new JSONArray(response);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

   private int getAppointments(){
        final int[] number = {0};
       String url= "http://aplicaciondimo.com/getAppointment.php?email="+Store.get().getEmail()+"&start="+System.currentTimeMillis();;

        //Create the list of the cookies
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        CustomRequest customRequest = new CustomRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetAppointment","Response is: "+ jsonArray.toString());
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            number[0] = jsonArray1.length();
                            setBadgeCount(getApplicationContext(), mCartMenuIcon, ""+ number[0]);
                            for(int i=0;i<jsonArray1.length();i++){
                                JSONObject appointment = (JSONObject)jsonArray1.get(i);
                                String description = (String) appointment.get("description");
                                String place = (String) appointment.get("place");
                                String title = (String) appointment.get("title");
                                long timestamp = Long.parseLong(String.valueOf(appointment.get("date")));
                                Appointment app = new Appointment(timestamp, title, place, description);
                                setAlarm(app);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("MALgetAppointments: ", error.toString() );
            }
        });

        Volley.newRequestQueue(getApplicationContext()).add(customRequest);
        return number[0];
    }

    private void setAlarm(Appointment appointment){
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");
        notificationIntent.putExtra("description", appointment.getTitle()+ " "+ DateConvert.millisecondsToDateStringWithoutSeconds(appointment.getTime()));

        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10);
        //alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, appointment.getTime(), broadcast); //1 hora antes
    }

    public boolean checkInternetConnection(){
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            // Operaciones http
            return true;
        } else {
            // Mostrar errores
            Toast.makeText(this, "No está disponible la conexión a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public void setToolbar(String title){
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {

            actionBar.setTitle(title);
            actionBar.setDisplayHomeAsUpEnabled(true);





        };
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e("ONSTART","MainActivity");
        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(ActivityRecognition.API)
                .addApi(LocationServices.API)
                .addApi(Fitness.SENSORS_API)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mApiClient.connect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 100){
            if( grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                Intent i =new Intent(getApplicationContext(),LocationService.class);
                startService(i);
            }else {
                runtime_permissions();
            }
        }
    }

    private BroadcastReceiver uiUpdatedStep = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String stepsString = intent.getExtras().getString("KeySteps");
            String [] parts =  stepsString.split("Value: ");
            String stepsPart = parts[1];

        }
    };

    private BroadcastReceiver uiUpdatedLocation = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String coordinatesString = intent.getExtras().getString("coordinatesKey");
            //tvLatitude.setText(coordinatesString);
        }
    };

    private BroadcastReceiver uiAlarmNotification = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(MainActivity.this)
                            .setSmallIcon(android.R.drawable.stat_sys_warning)
                            .setLargeIcon((((BitmapDrawable)getResources()
                                    .getDrawable(R.mipmap.ic_launcher)).getBitmap()))
                            .setContentTitle("Cita médica")
                            .setContentText("Ejemplo de notificación.")
                            .setTicker("Alerta!");

            //When clicks this activity appears
            Intent notIntent = new Intent(MainActivity.this, AppointmentActivity.class);
            PendingIntent contIntent = PendingIntent.getActivity(MainActivity.this, 0, notIntent, 0);

            mBuilder.setContentIntent(contIntent);

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            mNotificationManager.notify(0, mBuilder.build());
        }
    };

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Intent intent = new Intent(this, ActivityRecognizedService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(mApiClient, 3000, pendingIntent);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if( !authInProgress ) {
            try {
                authInProgress = true;
                connectionResult.startResolutionForResult( MainActivity.this, REQUEST_OAUTH );
            } catch(IntentSender.SendIntentException e ) {
                Log.e( "GoogleFit", "sendingIntentException " + e.getMessage() );
            }
        } else {
            Log.e( "GoogleFit", "authInProgress" );
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_OAUTH:
                authInProgress = false;
                if( resultCode == RESULT_OK ) {
                    if( !mApiClient.isConnecting() && !mApiClient.isConnected() ) {
                        mApiClient.connect();
                    }
                } else if( resultCode == RESULT_CANCELED ) {
                    Log.e( "GoogleFit", "RESULT_CANCELED" );
                }
                break;
        }
    }

    private boolean runtime_permissions() {
        if(Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},100);
            return true;        }
        return false;
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Se ha interrumpido la conexión con Google Play Services
        Log.e("Suspendido", "Se ha interrumpido la conexión con Google Play Services");
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()){
            case R.id.glucose_action:

               // CustomDialogGlucose dialogGlucose = new CustomDialogGlucose();
                //dialogGlucose.show(fragmentManager,"tagAlerta");
                Intent intGlucose = new Intent(this, GlucoseActivity.class);
                intGlucose.putExtra("email", email);
                intGlucose.putExtra("name", name);
                intGlucose.putExtra("photo", personPhoto);
                startActivity(intGlucose);


                break;
            case R.id.weight_action:

                Intent intWeight = new Intent(this, WeightActivity.class);
                intWeight.putExtra("email", email);
                intWeight.putExtra("name", name);
                intWeight.putExtra("photo", personPhoto);
                startActivity(intWeight);

                break;
            case R.id.food_action:
                Intent intFood = new Intent(this, FoodActivity.class);
                intFood.putExtra("email", email);
                intFood.putExtra("name", name);
                intFood.putExtra("photo", personPhoto);
                startActivity(intFood);
                break;
            case R.id.insuline_action:

                Intent intInsuline = new Intent(this, InsulineActivity.class);
                intInsuline.putExtra("email", email);
                intInsuline.putExtra("name", name);
                intInsuline.putExtra("photo", personPhoto);
                startActivity(intInsuline);

                break;
            case R.id.heart_action:

                Intent intHeartRates = new Intent(this, HeartRatesActivity.class);
                intHeartRates.putExtra("email", email);
                intHeartRates.putExtra("name", name);
                intHeartRates.putExtra("photo", personPhoto);
                startActivity(intHeartRates);
                break;
            case R.id.buttonSignout:
                logOut(v);


                cerrarSesion=false;



                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (exit) {
            moveTaskToBack(true);
        } else {
            Toast.makeText(this, "Pulsa atrás de nuevo para salir.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        mCartMenuIcon = (LayerDrawable) menu.findItem(R.id.action_appointment).getIcon();
       setBadgeCount(this, mCartMenuIcon, ""+ getAppointments());
       // setBadgeCount(this, mCartMenuIcon, "");
        //setBadgeCount(this, mCartMenuIcon, ""+ 1);
        return super.onCreateOptionsMenu(menu);
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {
        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        boolean fragmentTransaction = false;
        Fragment fragment=new FragmentMain();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if(item.isChecked()){
                item.setChecked(false);
                //Stop Step service
                stopService(intentStep);

                //Stop Activity recognition
                Intent intent = new Intent(this, ActivityRecognizedService.class);
                PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(mApiClient, pendingIntent);

                Store.get().setEnableActivity(false);

            }else if (!item.isChecked()){
                item.setChecked(true);
                Store.get().setEnableActivity(true);

                startService(intentStep);
            }
        }
        if (id == R.id.action_appointment){
            Log.e("appointmentError","appointmentError");
            Intent intApp = new Intent(this, AppointmentActivity.class);
            intApp.putExtra("email", email);
            intApp.putExtra("name", name);
            intApp.putExtra("photo", personPhoto);
            startActivity(intApp);
            scMain.setVisibility(View.INVISIBLE);
        }
        if (id == R.id.action_recentRecords){
            Intent intApp = new Intent(this, RecentRecordsActivity.class);

            startActivity(intApp);
            scMain.setVisibility(View.INVISIBLE);
        }

        if (id == R.id.action_help){
            // single example
            ShowcaseConfig config = new ShowcaseConfig();
            config.setDelay(500); // half second between each showcase view
            View menuItemView = findViewById(R.id.action_appointment);
            MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this);


            sequence.setConfig(config);
            sequence.addSequenceItem(menuItemView, getString(R.string.HelpText_Appointment) , getString(R.string.Helptext_Next));
            if(fabmenu.isShown()) {
                sequence.addSequenceItem(btnfab,
                        getString(R.string.HelpText_FabMenu), getString(R.string.Helptext_Next));
            }



            sequence.start();




        }

        if(fragmentTransaction) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();
            item.setChecked(true);
            setTitle(item.getTitle());
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        boolean fragmentTransaction = false;
        Fragment fragment = null;


        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.home) {
            Log.i("NavigationView", "Pulsada item11");
            checkCategory();
            // Handle the camera action
            fragment = new FragmentMain();
            fragmentTransaction = true;
            scMain.setVisibility(View.INVISIBLE);

        } else if (id == R.id.profile) {
            Log.i("NavigationView", "Pulsada item2");
            fragment = new FragmentProfile();
            fragmentTransaction = true;
            scMain.setVisibility(View.INVISIBLE);

        } else if (id == R.id.stats) {
            Intent int1 = new Intent(this, StatsActivity2.class);
            startActivity(int1);
            Log.i("NavigationView", "Pulsada item stats");
        } else if (id == R.id.premios) {
            Intent int1 = new Intent(this, RewardsActivity.class);
            startActivity(int1);
            Log.i("NavigationView", "Pulsada item premios");
        }else if(id== R.id.appointments){
            Intent int1 = new Intent(this, AppointmentActivity.class);
            startActivity(int1);
            Log.i("NavigationView", "Pulsada item appointments");
        }

        if(fragmentTransaction) {
           getSupportFragmentManager().beginTransaction()
                   .replace(R.id.content_frame, fragment)
                 .commit();
            item.setChecked(true);
            getSupportActionBar().setTitle(item.getTitle());
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }



}
