package mami.dimoapp.model;

/**
 * Created by Jesús
 */

public class Award {
    int id,pNeeded;

    public Award(int id_award, int pNeeded, String type, String category, long link) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getpNeeded() {
        return pNeeded;
    }

    public void setpNeeded(int pNeeded) {
        this.pNeeded = pNeeded;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getLink() {
        return link;
    }

    public void setLink(long link) {
        this.link = link;
    }

    String description,type,category;
    long link;


    public Award(int id,int pNeeded,String description,String type,String category,long link) {
        this.id = id;
        this.pNeeded = pNeeded;
        this.description = description;
        this.type = type;
        this.category=category;
        this.link=link;
    }


}
