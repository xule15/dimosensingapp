package mami.dimoapp.model;

/**
 * Created by Fernando on 15/11/2016.
 * Updated by Jesús

 */

public class FoodPoint {
    private String aliment;
    private int when;
    private long time;
    private float grams;

    public FoodPoint(String aliment, int when, long time, float grams) {
        this.aliment = aliment;
        this.when = when;
        this.time = time;
        this.grams = grams;
    }

    public String getAliment() {
        return aliment;
    }

    public void setAliment(String aliment) {
        this.aliment = aliment;
    }

    public int getWhen() {
        return when;
    }

    public void setWhen(int when) {
        this.when = when;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public float getGrams() {
        return grams;
    }

    public void setGrams(float grams) {
        this.grams = grams;
    }
}
