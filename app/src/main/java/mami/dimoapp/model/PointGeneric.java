package mami.dimoapp.model;

/**
 * Created by Fernando on 13/01/2017.
 * Updated by Jesús

 */

public class PointGeneric {
    float value;
    long time;

    public PointGeneric(float value, long time) {
        this.value = value;
        this.time = time;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
