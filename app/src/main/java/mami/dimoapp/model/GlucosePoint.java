package mami.dimoapp.model;

/**
 * Created by Fernando on 16/12/2016.
 * Updated by Jesús

 */

public class GlucosePoint  {

    long time;
    float index;
    int when;

    public GlucosePoint(){

    }

    public GlucosePoint(long time, float index, int when) {
        this.time = time;
        this.index = index;
        this.when = when;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public float getIndex() {
        return index;
    }

    public void setIndex(float index) {
        this.index = index;
    }

    public int getWhen() {
        return when;
    }

    public void setWhen(int when) {
        this.when = when;
    }

    /*private void makeRequest(Context context){
        JSONObject value = new JSONObject();
        JSONObject glucose = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            value.put("value", glucoseValue);
            value.put("check", when );
            glucose.put("type", "glucose");
            glucose.put("id", "me");
            glucose.put("attributes", value);
            data.put("data", glucose);
            Log.e("hola", data.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("CustomDialogGlucose: ",data.toString());

        RequestQueue queue = Volley.newRequestQueue(context);

        JsonObjectRequest jsArrayRequest = new JsonObjectRequest(
                Request.Method.POST,
                URL_BASE,
                data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("BIEN",response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Manejo de errores
                        Log.e("ERROR:" ,error.getMessage());
                    }
                });
        queue.add(jsArrayRequest);
    }*/
}

