package mami.dimoapp.model;

/**
 * Created by Fernando on 15/11/2016.
 * Updated by Jesús

 */

public class LocationPoint {
    private float latitude;
    private float longitude;

    public LocationPoint(float latitude, float longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
