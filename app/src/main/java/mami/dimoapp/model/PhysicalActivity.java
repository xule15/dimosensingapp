package mami.dimoapp.model;

/**
 * Created by Fernando on 15/11/2016.
 * Updated by Jesús

 */

public class PhysicalActivity {

    private long time;
    private int tipo;
    private long duration;
    private int steps;
    private int calories;
    private float distance;

    public PhysicalActivity(long time, int tipo, long duration, int steps, int calories, float distance) {
        this.time = time;
        this.tipo = tipo;
        this.duration = duration;
        this.steps = steps;
        this.calories = calories;
        this.distance = distance;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }
}
