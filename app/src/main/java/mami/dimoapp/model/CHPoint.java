package mami.dimoapp.model;

/**
 * Created by Fernando on 15/11/2016.
 * Updated by Jesús
 */

public class CHPoint {
    private float value;
    private int when;

    public CHPoint(float value, int when) {
        this.value = value;
        this.when = when;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public int getWhen() {
        return when;
    }

    public void setWhen(int when) {
        this.when = when;
    }
}
