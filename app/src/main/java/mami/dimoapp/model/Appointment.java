package mami.dimoapp.model;

/**
 * Created by Fernando on 14/11/2016.
 * Updated By Jesús
 */

public class Appointment {
    long time;
    String title;
    String place;
    String description;

    public Appointment(long time, String title, String place, String description) {
        this.time = time;
        this.title = title;
        this.place = place;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
