package mami.dimoapp.model;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by Fernando on 20/12/2016.
 * Updated by Jesús.

 */

public class Store {
    private String birthday;
    private long diabeticDebut;
    private String name;
    private String surname;
    private String email;
    private String address;
    private String country;
    private String language;
    private String gender;
    private float weight;
    private float height;
    private float minGlucose;
    private float maxGlucose;
    private String personPhoto;
    private int steps;
    private String token;
    private String serverUrl,pastActivity;
    private String cookie,date;
    private boolean enableActivity;
    private int dailyPoints,weeklyPoints;
    private double breakfast,brunch,lunch,snack,dinner;
    private int categoria;
    private int pDailyP1Bronze;
    private int pDailyP2Bronze;

    private int pDailyP3Bronze;
    private int pDailyP4Bronze;
    private int pDailyP1Silver;
    private int pDailyP2Silver;
    private int pDailyP3Silver;
    private int pDailyP4Silver;
    private int pDailyP1Gold;
    private int pDailyP2Gold;
    private int pDailyP3Gold;
    private int pDailyP4Gold;
    private int pWeeklyP1Bronze;
    private int pWeeklyP2Bronze;
    private int pWeeklyP3Bronze;
    private int pWeeklyP4Bronze;
    private int pWeeklyP1Silver;
    private int pWeeklyP2Silver;
    private int pWeeklyP3Silver;
    private int pWeeklyP4Silver;
    private int pWeeklyP1Gold;
    private int pWeeklyP2Gold;
    private int pWeeklyP3Gold;
    private int pWeeklyP4Gold;

    public int getControlDay1() {
        return controlDay1;
    }

    public void setControlDay1(int controlDay1) {
        this.controlDay1 = controlDay1;
    }

    public int getControlDay2() {
        return controlDay2;
    }

    public void setControlDay2(int controlDay2) {
        this.controlDay2 = controlDay2;
    }

    public int getControlDay3() {
        return controlDay3;
    }

    public void setControlDay3(int controlDay3) {
        this.controlDay3 = controlDay3;
    }

    public int getControlDay4() {
        return controlDay4;
    }

    public void setControlDay4(int controlDay4) {
        this.controlDay4 = controlDay4;
    }

    public int getControlDay5() {
        return controlDay5;
    }

    public void setControlDay5(int controlDay5) {
        this.controlDay5 = controlDay5;
    }

    public int getControlDay6() {
        return controlDay6;
    }

    public void setControlDay6(int controlDay6) {
        this.controlDay6 = controlDay6;
    }

    public int getControlDay7() {
        return controlDay7;
    }

    public void setControlDay7(int controlDay7) {
        this.controlDay7 = controlDay7;
    }

    private int controlDay1,controlDay2,controlDay3,controlDay4,controlDay5,controlDay6,controlDay7;
    private String descriptionDailyP1Bronze, descriptionDailyP2Bronze, descriptionDailyP3Bronze, descriptionDailyP4Bronze,
            descriptionDailyP1Silver, descriptionDailyP2Silver, descriptionDailyP3Silver, descriptionDailyP4Silver,
            descriptionDailyP1Gold, descriptionDailyP2Gold, descriptionDailyP3Gold, descriptionDailyP4Gold,
            descriptionWeeklyP1Bronze, descriptionWeeklyP2Bronze, descriptionWeeklyP3Bronze, descriptionWeeklyP4Bronze,
            descriptionWeeklyP1Silver, descriptionWeeklyP2Silver, descriptionWeeklyP3Silver, descriptionWeeklyP4Silver,
            descriptionWeeklyP1Gold, descriptionWeeklyP2Gold, descriptionWeeklyP3Gold, descriptionWeeklyP4Gold;
    private String typeP1DailyBronze, typeP2DailyBronze, typeP3DailyBronze, typeP4DailyBronze,
            typeP1DailySilver, typeP2DailySilver, typeP3DailySilver, typeP4DailySilver,
            typeP1DailyGold, typeP2DailyGold, typeP3DailyGold, typeP4DailyGold,
            typeP1WeeklyBronze, typeP2WeeklyBronze, typeP3WeeklyBronze, typeP4WeeklyBronze,
            typeP1WeeklySilver, typeP2WeeklySilver, typeP3WeeklySilver, typeP4WeeklySilver,
            typeP1WeeklyGold, typeP2WeeklyGold, typeP3WeeklyGold, typeP4WeeklyGold;
    private String linkP1DailyBronze,linkP2DailyBronze,linkP3DailyBronze,linkP4DailyBronze,linkP1DailySilver,linkP2DailySilver,
            linkP3DailySilver,linkP4DailySilver,linkP1DailyGold,linkP2DailyGold,linkP3DailyGold,linkP4DailyGold,linkP1WeeklyBronze, linkP2WeeklyBronze,
            linkP3WeeklyBronze,linkP4WeeklyBronze,linkP1WeeklySilver,linkP2WeeklySilver,linkP3WeeklySilver,linkP4WeeklySilver,
            linkP1WeeklyGold,linkP2WeeklyGold,linkP3WeeklyGold,linkP4WeeklyGold;

    private boolean exchange1,exchange2,exchange3,exchange4,exchange5,exchange6,exchange7,exchange8,exchange9,
            exchange10,exchange11,exchange12,exchange13,exchange14,exchange15,exchange16,exchange17,exchange18,
            exchange19,exchange20,exchange21,exchange22,exchange23,exchange24,exchange25;

    private int idP1DailyBronze,idP2DailyBronze,idP3DailyBronze,idP4DailyBronze,idP1DailySilver,idP2DailySilver,
            idP3DailySilver,idP4DailySilver,idP1DailyGold,idP2DailyGold,idP3DailyGold,idP4DailyGold,idP1WeeklyBronze, idP2WeeklyBronze,
            idP3WeeklyBronze,idP4WeeklyBronze,idP1WeeklySilver,idP2WeeklySilver,idP3WeeklySilver,idP4WeeklySilver,
            idP1WeeklyGold,idP2WeeklyGold,idP3WeeklyGold,idP4WeeklyGold;
    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }


    public int getpDailyP1Bronze() {
        return pDailyP1Bronze;
    }

    public void setpDailyP1Bronze(int pDailyP1Bronze) {
        this.pDailyP1Bronze = pDailyP1Bronze;
    }

    public int getpDailyP2Bronze() {
        return pDailyP2Bronze;
    }

    public void setpDailyP2Bronze(int pDailyP2Bronze) {
        this.pDailyP2Bronze = pDailyP2Bronze;
    }

    public int getpDailyP3Bronze() {
        return pDailyP3Bronze;
    }

    public void setpDailyP3Bronze(int pDailyP3Bronze) {
        this.pDailyP3Bronze = pDailyP3Bronze;
    }

    public int getpDailyP4Bronze() {
        return pDailyP4Bronze;
    }

    public void setpDailyP4Bronze(int pDailyP4Bronze) {
        this.pDailyP4Bronze = pDailyP4Bronze;
    }

    public String getDescriptionDailyP1Bronze() {
        return descriptionDailyP1Bronze;
    }

    public void setDescriptionDailyP1Bronze(String descriptionDailyP1Bronze) {
        this.descriptionDailyP1Bronze = descriptionDailyP1Bronze;
    }

    public String getTypeP1DailyBronze() {
        return typeP1DailyBronze;
    }

    public void setTypeP1DailyBronze(String typeP1DailyBronze) {
        this.typeP1DailyBronze = typeP1DailyBronze;
    }

    public String getTypeP2DailyBronze() {
        return typeP2DailyBronze;
    }

    public void setTypeP2DailyBronze(String typeP2DailyBronze) {
        this.typeP2DailyBronze = typeP2DailyBronze;
    }

    public String getTypeP3DailyBronze() {
        return typeP3DailyBronze;
    }

    public void setTypeP3DailyBronze(String typeP3DailyBronze) {
        this.typeP3DailyBronze = typeP3DailyBronze;
    }

    public String getTypeP4DailyBronze() {
        return typeP4DailyBronze;
    }

    public void setTypeP4DailyBronze(String typeP4DailyBronze) {
        this.typeP4DailyBronze = typeP4DailyBronze;
    }

    public String getTypeP1DailySilver() {
        return typeP1DailySilver;
    }

    public void setTypeP1DailySilver(String typeP1DailySilver) {
        this.typeP1DailySilver = typeP1DailySilver;
    }

    public String getTypeP2DailySilver() {
        return typeP2DailySilver;
    }

    public void setTypeP2DailySilver(String typeP2DailySilver) {
        this.typeP2DailySilver = typeP2DailySilver;
    }

    public String getTypeP3DailySilver() {
        return typeP3DailySilver;
    }

    public void setTypeP3DailySilver(String typeP3DailySilver) {
        this.typeP3DailySilver = typeP3DailySilver;
    }

    public String getTypeP4DailySilver() {
        return typeP4DailySilver;
    }

    public void setTypeP4DailySilver(String typeP4DailySilver) {
        this.typeP4DailySilver = typeP4DailySilver;
    }

    public String getTypeP1DailyGold() {
        return typeP1DailyGold;
    }

    public void setTypeP1DailyGold(String typeP1DailyGold) {
        this.typeP1DailyGold = typeP1DailyGold;
    }

    public String getTypeP2DailyGold() {
        return typeP2DailyGold;
    }

    public void setTypeP2DailyGold(String typeP2DailyGold) {
        this.typeP2DailyGold = typeP2DailyGold;
    }

    public String getTypeP3DailyGold() {
        return typeP3DailyGold;
    }

    public void setTypeP3DailyGold(String typeP3DailyGold) {
        this.typeP3DailyGold = typeP3DailyGold;
    }

    public String getTypeP4DailyGold() {
        return typeP4DailyGold;
    }

    public void setTypeP4DailyGold(String typeP4DailyGold) {
        this.typeP4DailyGold = typeP4DailyGold;
    }

    public String getTypeP1WeeklyBronze() {
        return typeP1WeeklyBronze;
    }

    public void setTypeP1WeeklyBronze(String typeP1WeeklyBronze) {
        this.typeP1WeeklyBronze = typeP1WeeklyBronze;
    }

    public String getTypeP2WeeklyBronze() {
        return typeP2WeeklyBronze;
    }

    public void setTypeP2WeeklyBronze(String typeP2WeeklyBronze) {
        this.typeP2WeeklyBronze = typeP2WeeklyBronze;
    }

    public String getTypeP3WeeklyBronze() {
        return typeP3WeeklyBronze;
    }

    public void setTypeP3WeeklyBronze(String typeP3WeeklyBronze) {
        this.typeP3WeeklyBronze = typeP3WeeklyBronze;
    }

    public String getTypeP4WeeklyBronze() {
        return typeP4WeeklyBronze;
    }

    public void setTypeP4WeeklyBronze(String typeP4WeeklyBronze) {
        this.typeP4WeeklyBronze = typeP4WeeklyBronze;
    }

    public String getTypeP1WeeklySilver() {
        return typeP1WeeklySilver;
    }

    public void setTypeP1WeeklySilver(String typeP1WeeklySilver) {
        this.typeP1WeeklySilver = typeP1WeeklySilver;
    }

    public String getTypeP2WeeklySilver() {
        return typeP2WeeklySilver;
    }

    public void setTypeP2WeeklySilver(String typeP2WeeklySilver) {
        this.typeP2WeeklySilver = typeP2WeeklySilver;
    }

    public String getTypeP3WeeklySilver() {
        return typeP3WeeklySilver;
    }

    public void setTypeP3WeeklySilver(String typeP3WeeklySilver) {
        this.typeP3WeeklySilver = typeP3WeeklySilver;
    }

    public String getTypeP4WeeklySilver() {
        return typeP4WeeklySilver;
    }

    public void setTypeP4WeeklySilver(String typeP4WeeklySilver) {
        this.typeP4WeeklySilver = typeP4WeeklySilver;
    }

    public String getTypeP1WeeklyGold() {
        return typeP1WeeklyGold;
    }

    public void setTypeP1WeeklyGold(String typeP1WeeklyGold) {
        this.typeP1WeeklyGold = typeP1WeeklyGold;
    }

    public String getTypeP2WeeklyGold() {
        return typeP2WeeklyGold;
    }

    public void setTypeP2WeeklyGold(String typeP2WeeklyGold) {
        this.typeP2WeeklyGold = typeP2WeeklyGold;
    }

    public String getTypeP3WeeklyGold() {
        return typeP3WeeklyGold;
    }

    public void setTypeP3WeeklyGold(String typeP3WeeklyGold) {
        this.typeP3WeeklyGold = typeP3WeeklyGold;
    }

    public String getTypeP4WeeklyGold() {
        return typeP4WeeklyGold;
    }

    public void setTypeP4WeeklyGold(String typeP4WeeklyGold) {
        this.typeP4WeeklyGold = typeP4WeeklyGold;
    }

    public String getLinkP1DailyBronze() {
        return linkP1DailyBronze;
    }

    public void setLinkP1DailyBronze(String linkP1DailyBronze) {
        this.linkP1DailyBronze = linkP1DailyBronze;
    }

    public String getLinkP2DailyBronze() {
        return linkP2DailyBronze;
    }

    public void setLinkP2DailyBronze(String linkP2DailyBronze) {
        this.linkP2DailyBronze = linkP2DailyBronze;
    }

    public String getLinkP3DailyBronze() {
        return linkP3DailyBronze;
    }

    public void setLinkP3DailyBronze(String linkP3DailyBronze) {
        this.linkP3DailyBronze = linkP3DailyBronze;
    }

    public String getLinkP4DailyBronze() {
        return linkP4DailyBronze;
    }

    public void setLinkP4DailyBronze(String linkP4DailyBronze) {
        this.linkP4DailyBronze = linkP4DailyBronze;
    }

    public String getLinkP1DailySilver() {
        return linkP1DailySilver;
    }

    public void setLinkP1DailySilver(String linkP1DailySilver) {
        this.linkP1DailySilver = linkP1DailySilver;
    }

    public String getLinkP2DailySilver() {
        return linkP2DailySilver;
    }

    public void setLinkP2DailySilver(String linkP2DailySilver) {
        this.linkP2DailySilver = linkP2DailySilver;
    }

    public String getLinkP3DailySilver() {
        return linkP3DailySilver;
    }

    public void setLinkP3DailySilver(String linkP3DailySilver) {
        this.linkP3DailySilver = linkP3DailySilver;
    }

    public String getLinkP4DailySilver() {
        return linkP4DailySilver;
    }

    public void setLinkP4DailySilver(String linkP4DailySilver) {
        this.linkP4DailySilver = linkP4DailySilver;
    }

    public String getLinkP1DailyGold() {
        return linkP1DailyGold;
    }

    public void setLinkP1DailyGold(String linkP1DailyGold) {
        this.linkP1DailyGold = linkP1DailyGold;
    }

    public String getLinkP2DailyGold() {
        return linkP2DailyGold;
    }

    public void setLinkP2DailyGold(String linkP2DailyGold) {
        this.linkP2DailyGold = linkP2DailyGold;
    }

    public String getLinkP3DailyGold() {
        return linkP3DailyGold;
    }

    public void setLinkP3DailyGold(String linkP3DailyGold) {
        this.linkP3DailyGold = linkP3DailyGold;
    }

    public String getLinkP4DailyGold() {
        return linkP4DailyGold;
    }

    public void setLinkP4DailyGold(String linkP4DailyGold) {
        this.linkP4DailyGold = linkP4DailyGold;
    }

    public String getLinkP1WeeklyBronze() {
        return linkP1WeeklyBronze;
    }

    public void setLinkP1WeeklyBronze(String linkP1WeeklyBronze) {
        this.linkP1WeeklyBronze = linkP1WeeklyBronze;
    }

    public String getLinkP2WeeklyBronze() {
        return linkP2WeeklyBronze;
    }

    public void setLinkP2WeeklyBronze(String linkP2WeeklyBronze) {
        this.linkP2WeeklyBronze = linkP2WeeklyBronze;
    }

    public String getLinkP3WeeklyBronze() {
        return linkP3WeeklyBronze;
    }

    public void setLinkP3WeeklyBronze(String linkP3WeeklyBronze) {
        this.linkP3WeeklyBronze = linkP3WeeklyBronze;
    }

    public String getLinkP4WeeklyBronze() {
        return linkP4WeeklyBronze;
    }

    public void setLinkP4WeeklyBronze(String linkP4WeeklyBronze) {
        this.linkP4WeeklyBronze = linkP4WeeklyBronze;
    }

    public String getLinkP1WeeklySilver() {
        return linkP1WeeklySilver;
    }

    public void setLinkP1WeeklySilver(String linkP1WeeklySilver) {
        this.linkP1WeeklySilver = linkP1WeeklySilver;
    }

    public String getLinkP2WeeklySilver() {
        return linkP2WeeklySilver;
    }

    public void setLinkP2WeeklySilver(String linkP2WeeklySilver) {
        this.linkP2WeeklySilver = linkP2WeeklySilver;
    }

    public String getLinkP3WeeklySilver() {
        return linkP3WeeklySilver;
    }

    public void setLinkP3WeeklySilver(String linkP3WeeklySilver) {
        this.linkP3WeeklySilver = linkP3WeeklySilver;
    }

    public String getLinkP4WeeklySilver() {
        return linkP4WeeklySilver;
    }

    public void setLinkP4WeeklySilver(String linkP4WeeklySilver) {
        this.linkP4WeeklySilver = linkP4WeeklySilver;
    }

    public String getLinkP1WeeklyGold() {
        return linkP1WeeklyGold;
    }

    public void setLinkP1WeeklyGold(String linkP1WeeklyGold) {
        this.linkP1WeeklyGold = linkP1WeeklyGold;
    }

    public String getLinkP2WeeklyGold() {
        return linkP2WeeklyGold;
    }

    public void setLinkP2WeeklyGold(String linkP2WeeklyGold) {
        this.linkP2WeeklyGold = linkP2WeeklyGold;
    }

    public String getLinkP3WeeklyGold() {
        return linkP3WeeklyGold;
    }

    public void setLinkP3WeeklyGold(String linkP3WeeklyGold) {
        this.linkP3WeeklyGold = linkP3WeeklyGold;
    }

    public String getLinkP4WeeklyGold() {
        return linkP4WeeklyGold;
    }

    public void setLinkP4WeeklyGold(String linkP4WeeklyGold) {
        this.linkP4WeeklyGold = linkP4WeeklyGold;
    }

    public String getDescriptionDailyP2Bronze() {
        return descriptionDailyP2Bronze;
    }

    public void setDescriptionDailyP2Bronze(String descriptionDailyP2Bronze) {
        this.descriptionDailyP2Bronze = descriptionDailyP2Bronze;
    }

    public String getDescriptionDailyP3Bronze() {
        return descriptionDailyP3Bronze;
    }

    public void setDescriptionDailyP3Bronze(String descriptionDailyP3Bronze) {
        this.descriptionDailyP3Bronze = descriptionDailyP3Bronze;
    }

    public String getDescriptionDailyP4Bronze() {
        return descriptionDailyP4Bronze;
    }

    public void setDescriptionDailyP4Bronze(String descriptionDailyP4Bronze) {
        this.descriptionDailyP4Bronze = descriptionDailyP4Bronze;
    }

    public String getDescriptionDailyP1Silver() {
        return descriptionDailyP1Silver;
    }

    public void setDescriptionDailyP1Silver(String descriptionDailyP1Silver) {
        this.descriptionDailyP1Silver = descriptionDailyP1Silver;
    }

    public String getDescriptionDailyP2Silver() {
        return descriptionDailyP2Silver;
    }

    public void setDescriptionDailyP2Silver(String descriptionDailyP2Silver) {
        this.descriptionDailyP2Silver = descriptionDailyP2Silver;
    }

    public String getDescriptionDailyP3Silver() {
        return descriptionDailyP3Silver;
    }

    public void setDescriptionDailyP3Silver(String descriptionDailyP3Silver) {
        this.descriptionDailyP3Silver = descriptionDailyP3Silver;
    }

    public String getDescriptionDailyP4Silver() {
        return descriptionDailyP4Silver;
    }

    public void setDescriptionDailyP4Silver(String descriptionDailyP4Silver) {
        this.descriptionDailyP4Silver = descriptionDailyP4Silver;
    }

    public String getDescriptionDailyP1Gold() {
        return descriptionDailyP1Gold;
    }

    public void setDescriptionDailyP1Gold(String descriptionDailyP1Gold) {
        this.descriptionDailyP1Gold = descriptionDailyP1Gold;
    }

    public String getDescriptionDailyP2Gold() {
        return descriptionDailyP2Gold;
    }

    public void setDescriptionDailyP2Gold(String descriptionDailyP2Gold) {
        this.descriptionDailyP2Gold = descriptionDailyP2Gold;
    }

    public String getDescriptionDailyP3Gold() {
        return descriptionDailyP3Gold;
    }

    public void setDescriptionDailyP3Gold(String descriptionDailyP3Gold) {
        this.descriptionDailyP3Gold = descriptionDailyP3Gold;
    }

    public String getDescriptionDailyP4Gold() {
        return descriptionDailyP4Gold;
    }

    public void setDescriptionDailyP4Gold(String descriptionDailyP4Gold) {
        this.descriptionDailyP4Gold = descriptionDailyP4Gold;
    }

    public String getDescriptionWeeklyP1Bronze() {
        return descriptionWeeklyP1Bronze;
    }

    public void setDescriptionWeeklyP1Bronze(String descriptionWeeklyP1Bronze) {
        this.descriptionWeeklyP1Bronze = descriptionWeeklyP1Bronze;
    }

    public String getDescriptionWeeklyP2Bronze() {
        return descriptionWeeklyP2Bronze;
    }

    public void setDescriptionWeeklyP2Bronze(String descriptionWeeklyP2Bronze) {
        this.descriptionWeeklyP2Bronze = descriptionWeeklyP2Bronze;
    }

    public String getDescriptionWeeklyP3Bronze() {
        return descriptionWeeklyP3Bronze;
    }

    public void setDescriptionWeeklyP3Bronze(String descriptionWeeklyP3Bronze) {
        this.descriptionWeeklyP3Bronze = descriptionWeeklyP3Bronze;
    }

    public String getDescriptionWeeklyP4Bronze() {
        return descriptionWeeklyP4Bronze;
    }

    public void setDescriptionWeeklyP4Bronze(String descriptionWeeklyP4Bronze) {
        this.descriptionWeeklyP4Bronze = descriptionWeeklyP4Bronze;
    }

    public String getDescriptionWeeklyP1Silver() {
        return descriptionWeeklyP1Silver;
    }

    public void setDescriptionWeeklyP1Silver(String descriptionWeeklyP1Silver) {
        this.descriptionWeeklyP1Silver = descriptionWeeklyP1Silver;
    }

    public String getDescriptionWeeklyP2Silver() {
        return descriptionWeeklyP2Silver;
    }

    public void setDescriptionWeeklyP2Silver(String descriptionWeeklyP2Silver) {
        this.descriptionWeeklyP2Silver = descriptionWeeklyP2Silver;
    }

    public String getDescriptionWeeklyP3Silver() {
        return descriptionWeeklyP3Silver;
    }

    public void setDescriptionWeeklyP3Silver(String descriptionWeeklyP3Silver) {
        this.descriptionWeeklyP3Silver = descriptionWeeklyP3Silver;
    }

    public String getDescriptionWeeklyP4Silver() {
        return descriptionWeeklyP4Silver;
    }

    public void setDescriptionWeeklyP4Silver(String descriptionWeeklyP4Silver) {
        this.descriptionWeeklyP4Silver = descriptionWeeklyP4Silver;
    }

    public String getDescriptionWeeklyP1Gold() {
        return descriptionWeeklyP1Gold;
    }

    public void setDescriptionWeeklyP1Gold(String descriptionWeeklyP1Gold) {
        this.descriptionWeeklyP1Gold = descriptionWeeklyP1Gold;
    }

    public String getDescriptionWeeklyP2Gold() {
        return descriptionWeeklyP2Gold;
    }

    public void setDescriptionWeeklyP2Gold(String descriptionWeeklyP2Gold) {
        this.descriptionWeeklyP2Gold = descriptionWeeklyP2Gold;
    }

    public String getDescriptionWeeklyP3Gold() {
        return descriptionWeeklyP3Gold;
    }

    public void setDescriptionWeeklyP3Gold(String descriptionWeeklyP3Gold) {
        this.descriptionWeeklyP3Gold = descriptionWeeklyP3Gold;
    }

    public String getDescriptionWeeklyP4Gold() {
        return descriptionWeeklyP4Gold;
    }

    public void setDescriptionWeeklyP4Gold(String descriptionWeeklyP4Gold) {
        this.descriptionWeeklyP4Gold = descriptionWeeklyP4Gold;
    }


    public int getIdP1DailyBronze() {
        return idP1DailyBronze;
    }

    public void setIdP1DailyBronze(int idP1DailyBronze) {
        this.idP1DailyBronze = idP1DailyBronze;
    }

    public int getIdP2DailyBronze() {
        return idP2DailyBronze;
    }

    public void setIdP2DailyBronze(int idP2DailyBronze) {
        this.idP2DailyBronze = idP2DailyBronze;
    }

    public int getIdP3DailyBronze() {
        return idP3DailyBronze;
    }

    public void setIdP3DailyBronze(int idP3DailyBronze) {
        this.idP3DailyBronze = idP3DailyBronze;
    }

    public int getIdP4DailyBronze() {
        return idP4DailyBronze;
    }

    public void setIdP4DailyBronze(int idP4DailyBronze) {
        this.idP4DailyBronze = idP4DailyBronze;
    }

    public int getIdP1DailySilver() {
        return idP1DailySilver;
    }

    public void setIdP1DailySilver(int idP1DailySilver) {
        this.idP1DailySilver = idP1DailySilver;
    }

    public int getIdP2DailySilver() {
        return idP2DailySilver;
    }

    public void setIdP2DailySilver(int idP2DailySilver) {
        this.idP2DailySilver = idP2DailySilver;
    }

    public int getIdP3DailySilver() {
        return idP3DailySilver;
    }

    public void setIdP3DailySilver(int idP3DailySilver) {
        this.idP3DailySilver = idP3DailySilver;
    }

    public int getIdP4DailySilver() {
        return idP4DailySilver;
    }

    public void setIdP4DailySilver(int idP4DailySilver) {
        this.idP4DailySilver = idP4DailySilver;
    }

    public int getIdP1DailyGold() {
        return idP1DailyGold;
    }

    public void setIdP1DailyGold(int idP1DailyGold) {
        this.idP1DailyGold = idP1DailyGold;
    }

    public int getIdP2DailyGold() {
        return idP2DailyGold;
    }

    public void setIdP2DailyGold(int idP2DailyGold) {
        this.idP2DailyGold = idP2DailyGold;
    }

    public int getIdP3DailyGold() {
        return idP3DailyGold;
    }

    public void setIdP3DailyGold(int idP3DailyGold) {
        this.idP3DailyGold = idP3DailyGold;
    }

    public int getIdP4DailyGold() {
        return idP4DailyGold;
    }

    public void setIdP4DailyGold(int idP4DailyGold) {
        this.idP4DailyGold = idP4DailyGold;
    }

    public int getIdP1WeeklyBronze() {
        return idP1WeeklyBronze;
    }

    public void setIdP1WeeklyBronze(int idP1WeeklyBronze) {
        this.idP1WeeklyBronze = idP1WeeklyBronze;
    }

    public int getIdP2WeeklyBronze() {
        return idP2WeeklyBronze;
    }

    public void setIdP2WeeklyBronze(int idP2WeeklyBronze) {
        this.idP2WeeklyBronze = idP2WeeklyBronze;
    }

    public int getIdP3WeeklyBronze() {
        return idP3WeeklyBronze;
    }

    public void setIdP3WeeklyBronze(int idP3WeeklyBronze) {
        this.idP3WeeklyBronze = idP3WeeklyBronze;
    }

    public int getIdP4WeeklyBronze() {
        return idP4WeeklyBronze;
    }

    public void setIdP4WeeklyBronze(int idP4WeeklyBronze) {
        this.idP4WeeklyBronze = idP4WeeklyBronze;
    }

    public int getIdP1WeeklySilver() {
        return idP1WeeklySilver;
    }

    public void setIdP1WeeklySilver(int idP1WeeklySilver) {
        this.idP1WeeklySilver = idP1WeeklySilver;
    }

    public int getIdP2WeeklySilver() {
        return idP2WeeklySilver;
    }

    public void setIdP2WeeklySilver(int idP2WeeklySilver) {
        this.idP2WeeklySilver = idP2WeeklySilver;
    }

    public int getIdP3WeeklySilver() {
        return idP3WeeklySilver;
    }

    public void setIdP3WeeklySilver(int idP3WeeklySilver) {
        this.idP3WeeklySilver = idP3WeeklySilver;
    }

    public int getIdP4WeeklySilver() {
        return idP4WeeklySilver;
    }

    public void setIdP4WeeklySilver(int idP4WeeklySilver) {
        this.idP4WeeklySilver = idP4WeeklySilver;
    }

    public int getIdP1WeeklyGold() {
        return idP1WeeklyGold;
    }

    public void setIdP1WeeklyGold(int idP1WeeklyGold) {
        this.idP1WeeklyGold = idP1WeeklyGold;
    }

    public int getIdP2WeeklyGold() {
        return idP2WeeklyGold;
    }

    public void setIdP2WeeklyGold(int idP2WeeklyGold) {
        this.idP2WeeklyGold = idP2WeeklyGold;
    }

    public int getIdP3WeeklyGold() {
        return idP3WeeklyGold;
    }

    public void setIdP3WeeklyGold(int idP3WeeklyGold) {
        this.idP3WeeklyGold = idP3WeeklyGold;
    }

    public int getIdP4WeeklyGold() {
        return idP4WeeklyGold;
    }

    public void setIdP4WeeklyGold(int idP4WeeklyGold) {
        this.idP4WeeklyGold = idP4WeeklyGold;
    }

    public int getpDailyP1Silver() {
        return pDailyP1Silver;
    }

    public void setpDailyP1Silver(int pDailyP1Silver) {
        this.pDailyP1Silver = pDailyP1Silver;
    }

    public int getpDailyP2Silver() {
        return pDailyP2Silver;
    }

    public void setpDailyP2Silver(int pDailyP2Silver) {
        this.pDailyP2Silver = pDailyP2Silver;
    }

    public int getpDailyP3Silver() {
        return pDailyP3Silver;
    }

    public void setpDailyP3Silver(int pDailyP3Silver) {
        this.pDailyP3Silver = pDailyP3Silver;
    }

    public int getpDailyP4Silver() {
        return pDailyP4Silver;
    }

    public void setpDailyP4Silver(int pDailyP4Silver) {
        this.pDailyP4Silver = pDailyP4Silver;
    }

    public int getpDailyP1Gold() {
        return pDailyP1Gold;
    }

    public void setpDailyP1Gold(int pDailyP1Gold) {
        this.pDailyP1Gold = pDailyP1Gold;
    }

    public int getpDailyP2Gold() {
        return pDailyP2Gold;
    }

    public void setpDailyP2Gold(int pDailyP2Gold) {
        this.pDailyP2Gold = pDailyP2Gold;
    }

    public int getpDailyP3Gold() {
        return pDailyP3Gold;
    }

    public void setpDailyP3Gold(int pDailyP3Gold) {
        this.pDailyP3Gold = pDailyP3Gold;
    }

    public int getpDailyP4Gold() {
        return pDailyP4Gold;
    }

    public void setpDailyP4Gold(int pDailyP4Gold) {
        this.pDailyP4Gold = pDailyP4Gold;
    }

    public int getpWeeklyP1Bronze() {
        return pWeeklyP1Bronze;
    }

    public void setpWeeklyP1Bronze(int pWeeklyP1Bronze) {
        this.pWeeklyP1Bronze = pWeeklyP1Bronze;
    }

    public int getpWeeklyP2Bronze() {
        return pWeeklyP2Bronze;
    }

    public void setpWeeklyP2Bronze(int pWeeklyP2Bronze) {
        this.pWeeklyP2Bronze = pWeeklyP2Bronze;
    }

    public int getpWeeklyP3Bronze() {
        return pWeeklyP3Bronze;
    }

    public void setpWeeklyP3Bronze(int pWeeklyP3Bronze) {
        this.pWeeklyP3Bronze = pWeeklyP3Bronze;
    }

    public int getpWeeklyP4Bronze() {
        return pWeeklyP4Bronze;
    }

    public void setpWeeklyP4Bronze(int pWeeklyP4Bronze) {
        this.pWeeklyP4Bronze = pWeeklyP4Bronze;
    }

    public int getpWeeklyP1Silver() {
        return pWeeklyP1Silver;
    }

    public void setpWeeklyP1Silver(int pWeeklyP1Silver) {
        this.pWeeklyP1Silver = pWeeklyP1Silver;
    }

    public int getpWeeklyP2Silver() {
        return pWeeklyP2Silver;
    }

    public void setpWeeklyP2Silver(int pWeeklyP2Silver) {
        this.pWeeklyP2Silver = pWeeklyP2Silver;
    }

    public int getpWeeklyP3Silver() {
        return pWeeklyP3Silver;
    }

    public void setpWeeklyP3Silver(int pWeeklyP3Silver) {
        this.pWeeklyP3Silver = pWeeklyP3Silver;
    }

    public int getpWeeklyP4Silver() {
        return pWeeklyP4Silver;
    }

    public void setpWeeklyP4Silver(int pWeeklyP4Silver) {
        this.pWeeklyP4Silver = pWeeklyP4Silver;
    }

    public int getpWeeklyP1Gold() {
        return pWeeklyP1Gold;
    }

    public void setpWeeklyP1Gold(int pWeeklyP1Gold) {
        this.pWeeklyP1Gold = pWeeklyP1Gold;
    }

    public int getpWeeklyP2Gold() {
        return pWeeklyP2Gold;
    }

    public void setpWeeklyP2Gold(int pWeeklyP2Gold) {
        this.pWeeklyP2Gold = pWeeklyP2Gold;
    }

    public int getpWeeklyP3Gold() {
        return pWeeklyP3Gold;
    }

    public void setpWeeklyP3Gold(int pWeeklyP3Gold) {
        this.pWeeklyP3Gold = pWeeklyP3Gold;
    }

    public int getpWeeklyP4Gold() {
        return pWeeklyP4Gold;
    }

    public void setpWeeklyP4Gold(int pWeeklyP4Gold) {
        this.pWeeklyP4Gold = pWeeklyP4Gold;
    }




    private boolean exists;
    public boolean isExists() {
        return exists;
    }



    public void setExists(boolean exists) {
        this.exists = exists;
    }





    private static Store store;

    private Store(){

    }

    public static Store get() {
        if (store==null)
            store=new Store();
        return store;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }


    public double getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(double breakfast) {
        this.breakfast = breakfast;
    }

    public double getBrunch() {
        return brunch;
    }

    public void setBrunch(double brunch) {
        this.brunch = brunch;
    }

    public double getLunch() {
        return lunch;
    }

    public void setLunch(double lunch) {
        this.lunch = lunch;
    }

    public double getSnack() {
        return snack;
    }

    public void setSnack(double snack) {
        this.snack = snack;
    }

    public double getDinner() {
        return dinner;
    }

    public void setDinner(double dinner) {
        this.dinner = dinner;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getDiabeticDebut() {
        return diabeticDebut;
    }

    public void setDiabeticDebut(long diabeticDebut) {
        this.diabeticDebut = diabeticDebut;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getMinGlucose() {
        return minGlucose;
    }

    public void setMinGlucose(float minGlucose) {
        this.minGlucose = minGlucose;
    }

    public float getMaxGlucose() {
        return maxGlucose;
    }

    public void setMaxGlucose(float maxGlucose) {
        this.maxGlucose = maxGlucose;
    }

    public String getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(String personPhoto) {
        this.personPhoto = personPhoto;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static Store getStore() {
        return store;
    }

    public static void setStore(Store store) {
        Store.store = store;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public boolean isEnableActivity() {
        return enableActivity;
    }

    public void setEnableActivity(boolean enableActivity) {
        this.enableActivity = enableActivity;
    }

    public int getdailyPoints() {
        return dailyPoints;
    }

    public void setdailyPoints(int dailyPoints) {
        this.dailyPoints = dailyPoints;
    }
    public int getweeklyPoints() {
        return weeklyPoints;
    }

    public void setweeklyPoints(int weeklyPoints) {
        this.weeklyPoints = weeklyPoints;
    }

    public boolean isExchange1() {
        return exchange1;
    }

    public void setExchange1(boolean exchange1) {
        this.exchange1 = exchange1;
    }

    public boolean isExchange2() {
        return exchange2;
    }

    public void setExchange2(boolean exchange2) {
        this.exchange2 = exchange2;
    }

    public boolean isExchange3() {
        return exchange3;
    }

    public void setExchange3(boolean exchange3) {
        this.exchange3 = exchange3;
    }

    public boolean isExchange4() {
        return exchange4;
    }

    public void setExchange4(boolean exchange4) {
        this.exchange4 = exchange4;
    }

    public boolean isExchange5() {
        return exchange5;
    }

    public void setExchange5(boolean exchange5) {
        this.exchange5 = exchange5;
    }

    public boolean isExchange6() {
        return exchange6;
    }

    public void setExchange6(boolean exchange6) {
        this.exchange6 = exchange6;
    }

    public boolean isExchange7() {
        return exchange7;
    }

    public void setExchange7(boolean exchange7) {
        this.exchange7 = exchange7;
    }

    public boolean isExchange8() {
        return exchange8;
    }

    public void setExchange8(boolean exchange8) {
        this.exchange8 = exchange8;
    }

    public boolean isExchange9() {
        return exchange9;
    }

    public void setExchange9(boolean exchange9) {
        this.exchange9 = exchange9;
    }

    public boolean isExchange10() {
        return exchange10;
    }

    public void setExchange10(boolean exchange10) {
        this.exchange10 = exchange10;
    }

    public boolean isExchange11() {
        return exchange11;
    }

    public void setExchange11(boolean exchange11) {
        this.exchange11 = exchange11;
    }

    public boolean isExchange12() {
        return exchange12;
    }

    public void setExchange12(boolean exchange12) {
        this.exchange12 = exchange12;
    }

    public boolean isExchange13() {
        return exchange13;
    }

    public void setExchange13(boolean exchange13) {
        this.exchange13 = exchange13;
    }

    public boolean isExchange14() {
        return exchange14;
    }

    public void setExchange14(boolean exchange14) {
        this.exchange14 = exchange14;
    }

    public boolean isExchange15() {
        return exchange15;
    }

    public void setExchange15(boolean exchange15) {
        this.exchange15 = exchange15;
    }

    public boolean isExchange16() {
        return exchange16;
    }

    public void setExchange16(boolean exchange16) {
        this.exchange16 = exchange16;
    }

    public boolean isExchange17() {
        return exchange17;
    }

    public void setExchange17(boolean exchange17) {
        this.exchange17 = exchange17;
    }

    public boolean isExchange18() {
        return exchange18;
    }

    public void setExchange18(boolean exchange18) {
        this.exchange18 = exchange18;
    }

    public boolean isExchange19() {
        return exchange19;
    }

    public void setExchange19(boolean exchange19) {
        this.exchange19 = exchange19;
    }

    public boolean isExchange20() {
        return exchange20;
    }

    public void setExchange20(boolean exchange20) {
        this.exchange20 = exchange20;
    }

    public boolean isExchange21() {
        return exchange21;
    }

    public void setExchange21(boolean exchange21) {
        this.exchange21 = exchange21;
    }

    public boolean isExchange22() {
        return exchange22;
    }

    public void setExchange22(boolean exchange22) {
        this.exchange22 = exchange22;
    }

    public boolean isExchange23() {
        return exchange23;
    }

    public void setExchange23(boolean exchange23) {
        this.exchange23 = exchange23;
    }

    public boolean isExchange24() {
        return exchange24;
    }

    public void setExchange24(boolean exchange24) {
        this.exchange24 = exchange24;
    }

    public boolean isExchange25() {
        return exchange25;
    }

    public void setExchange25(boolean exchange25) {
        this.exchange25 = exchange25;
    }
    public String getPastActivity() {
        return pastActivity;
    }

    public void setPastActivity(String pastActivity) {
        this.pastActivity = pastActivity;
    }

    public boolean checkInternetConnection(Context context){
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            // Operaciones http
            return true;
        } else {
            // Mostrar errores
            Toast.makeText(context, "No está disponible la conexión a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }
}
