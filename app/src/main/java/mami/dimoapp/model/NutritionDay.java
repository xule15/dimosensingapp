package mami.dimoapp.model;

/**
 * Created by Fernando on 13/01/2017.
 * Updated by Jesús
 */

public class NutritionDay {
    long time;
    float kcal;
    float cH;
    float prot;
    float fat;
    float gluc;

    public NutritionDay(long time, float kcal, float cH, float prot, float fat, float gluc) {
        this.time = time;
        this.kcal = kcal;
        this.cH = cH;
        this.prot = prot;
        this.fat = fat;
        this.gluc = gluc;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public float getKcal() {
        return kcal;
    }

    public void setKcal(float kcal) {
        this.kcal = kcal;
    }

    public float getcH() {
        return cH;
    }

    public void setcH(float cH) {
        this.cH = cH;
    }

    public float getProt() {
        return prot;
    }

    public void setProt(float prot) {
        this.prot = prot;
    }

    public float getFat() {
        return fat;
    }

    public void setFat(float fat) {
        this.fat = fat;
    }

    public float getGluc() {
        return gluc;
    }

    public void setGluc(float gluc) {
        this.gluc = gluc;
    }
}
