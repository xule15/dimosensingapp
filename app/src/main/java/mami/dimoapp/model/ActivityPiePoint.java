package mami.dimoapp.model;

/**
 * Created by Fernando on 15/11/2016.
 * Updated by Jesús
 */

public class ActivityPiePoint {
    private float value;
    private int type;

    public ActivityPiePoint(int type, float value) {
        this.type = type;
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
