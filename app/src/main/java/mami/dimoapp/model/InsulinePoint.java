package mami.dimoapp.model;

/**
 * Created by Fernando on 16/12/2016.
 * Updated by Jesús
 */

public class InsulinePoint {

    long time;
    float dose;
    int type;

    public InsulinePoint(long time, float dose, int type) {
        this.time = time;
        this.dose = dose;
        this.type = type;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public float getDose() {
        return dose;
    }

    public void setDose(float dose) {
        this.dose = dose;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}

