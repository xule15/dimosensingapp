package mami.dimoapp.model;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mami.dimoapp.custom.CustomRequest;

/**
 * Created by Fernando on 18/11/2016.
 * Updated by Jesús.

 */

public class Profile {

    private int birthday;
    private int diabeticDebut;
    private String name;
    private String surname;
    private String address;
    private String country;
    private String language;
    private String gender;
    private String email;
    private int weight;
    private int height;
    private int min;
    private int max;
    private String personPhoto;


    public Profile(){
    }

    public Profile(int birthday, int diabeticDebut, String name, String surname, String address,
                   String country, String language, String gender, int weight, int height, int min, int max) {
        this.birthday = birthday;
        this.diabeticDebut = diabeticDebut;
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.country = country;
        this.language = language;
        this.gender = gender;
        this.weight = weight;
        this.height = height;
        this.min = min;
        this.max = max;
    }

    public void getProfileRequest(Context context){
        String url = "http://mamilab.esi.uclm.es:5050/api/v2.0/profiles/me";

        //Create the list of the cookies, i.e. key=value
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        CustomRequest customRequest = new CustomRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        try {
                            JSONObject jsonArray1 = (JSONObject) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                JSONObject attributes = (JSONObject) jsonArray1.get("attributes");
                                String email = (String) attributes.get("email");
                                String gender = (String) attributes.get("gender");
                                String name = (String) attributes.get("name");
                                String surname = (String) attributes.get("surname");
                                String picture = (String) attributes.get("picture");

                                if(name!=null)Store.get().setName(name);
                                if(surname!=null)Store.get().setSurname(surname);
                                if(email!=null)Store.get().setEmail(email);
                                if(picture!=null)Store.get().setPersonPhoto(picture);
                                if(gender!=null)Store.get().setGender(gender);
                                //Store.get().setDiabeticDebut(diabeticDebut);
                                //Store.get().setBirthday(birthday);
                                //Store.get().setAddress("");
                                //Store.get().setCountry(country);

                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartGlu: ", error.toString() );
            }
        });

        customRequest.setCookies(cookies);
        // and finally add the request to the queue
        Volley.newRequestQueue(context).add(customRequest);
    }

    public void getWeightRequest(Context context){
        //String url = "http://IP/api/v2.0/glucose?"+startDate+"&"+endDate;
        String url = "http://mamilab.esi.uclm.es:5050/api/v2.0/weights";

        //Create the list of the cookies, i.e. key=value
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        CustomRequest customRequest = new CustomRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                JSONObject glucose = (JSONObject) jsonArray1.get(jsonArray1.length()-1);
                                JSONObject attributes = (JSONObject) glucose.get("attributes");
                                Float weightValue = Float.parseFloat(String.valueOf(attributes.get("value")));
                                Store.get().setWeight(weightValue);
                            }else{
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartGlu: ", error.toString() );
            }
        });

        customRequest.setCookies(cookies);
        // and finally add the request to the queue
        Volley.newRequestQueue(context).add(customRequest);
    }

    public void getHeightRequest(Context context){
        //String url = "http://IP/api/v2.0/glucose?"+startDate+"&"+endDate;
        String url = "http://mamilab.esi.uclm.es:5050/api/v2.0/heights";

        //Create the list of the cookies, i.e. key=value
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        CustomRequest customRequest = new CustomRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                JSONObject height = (JSONObject) jsonArray1.get(jsonArray1.length()-1);
                                JSONObject attributes = (JSONObject) height.get("attributes");
                                float heightValue = Float.parseFloat(String.valueOf(attributes.get("value")));
                                Store.get().setHeight(heightValue);
                            }else{
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORgetHeight: ", error.toString() );
            }
        });

        customRequest.setCookies(cookies);
        // and finally add the request to the queue
        Volley.newRequestQueue(context).add(customRequest);
    }

    public void getControlRangeRequest(Context context){
        String url = "http://mamilab.esi.uclm.es:5050/api/v2.0/control_ranges";

        //Create the list of the cookies, i.e. key=value
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        CustomRequest customRequest = new CustomRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENControlRange","Response is: "+ jsonArray.toString());
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                JSONObject controlRange = (JSONObject) jsonArray1.get(jsonArray1.length()-1);
                                JSONObject attributes = (JSONObject) controlRange.get("attributes");
                                float maxGlucose = Float.parseFloat(String.valueOf(attributes.get("max")));
                                float minGlucose = Float.parseFloat(String.valueOf(attributes.get("min")));
                                Store.get().setMaxGlucose(maxGlucose);
                                Store.get().setMinGlucose(minGlucose);
                            }else{
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORControlRange: ", error.toString() );
            }
        });

        customRequest.setCookies(cookies);
        // and finally add the request to the queue
        Volley.newRequestQueue(context).add(customRequest);
    }





    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getBirthday() {
        return birthday;
    }

    public void setBirthday(int birthday) {
        this.birthday = birthday;
    }

    public int getDiabeticDebut() {
        return diabeticDebut;
    }

    public void setDiabeticDebut(int diabeticDebut) {
        this.diabeticDebut = diabeticDebut;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(String personPhoto) {
        this.personPhoto = personPhoto;
    }
}
