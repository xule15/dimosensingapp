package mami.dimoapp.dialogs;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mami.dimoapp.MainActivity;
import mami.dimoapp.R;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.model.Store;

/**
 * Created by Fernando on 06/10/2016.
 */

public class CustomDialogGlucose extends  DialogFragment{


    private NumberPicker np;
    private EditText etDateGlucose, etTimeGlucose, etGlucose;
    private int mYear, mMonth, mDay, mHour, mMinute, value, year2, month, day, hour, minute2;
    private Spinner spinnerWhen;
    private int when;
    private long time;
    private static final String URL_BASE = "http://mamilab.esi.uclm.es:5050/api/v2.0/glucose";
    private CustomRequest customRequest;
    private String fechaActual;
    private String email;
    private int points, totalPoints;



    public CustomDialogGlucose() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return createGlucoseDialog();
    }

    public AlertDialog createGlucoseDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_glucose, null);
        builder.setView(view);

        //initialize date for the pickers

        email=Store.get().getEmail();
        Store.get().setExists(true);
        points=Store.get().getdailyPoints();
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        //initialize default date
        year2 = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute2 = c.get(Calendar.MINUTE);

       // time = System.currentTimeMillis()*1000000;
        time = System.currentTimeMillis();
        when = 0;
        value= 100;

        //Types of insuline adapter
        spinnerWhen = (Spinner)view.findViewById(R.id.spinnerWhen);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.whenGlucose, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWhen.setAdapter(adapter);

        spinnerWhen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if(parent.getItemAtPosition(pos).toString().equals("Antes de comer")){
                    when = 0;
                }else if(parent.getItemAtPosition(pos).toString().equals("Después de comer")){
                    when = 1;
                }else{
                    when = 2;
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        etDateGlucose = (EditText) view.findViewById(R.id.editTextDateInsuline);
        etDateGlucose.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear,year))>=0){
                                    etDateGlucose.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    year2 = year;
                                    month = monthOfYear;
                                    day = dayOfMonth;
                                    time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2, 0);
                                    Log.e("tiempo","tiempo"+time);
                                    fechaActual=checkDate(dayOfMonth,monthOfYear+1,year);
                                    Log.e("tiempo","tiempo2"+fechaActual);
                                }else{
                                    Toast.makeText(getContext(),"La fecha introducida no debe ser posterior a la actual", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        etTimeGlucose = (EditText)view.findViewById(R.id.editTextTimeInsuline);
        etTimeGlucose.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TimePickerDialog tpd = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                etTimeGlucose.setText(checkTime(hourOfDay, minute));
                                hour = hourOfDay;
                                minute2 = minute;
                                time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2, 0)*1000000;
                            }
                        }, mHour, mMinute, true);

                tpd.show();
            }
        });

        etGlucose = (EditText)view.findViewById(R.id.editTextInsuline);
        etGlucose.setText(100+"");

        //etGlucose.setOnClickListener(new View.OnClickListener() {

          //  public void onClick(View v) {

                //AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

                //alert.setTitle("Selecciona el nivel de glucosa: ");

                //np = new NumberPicker(getContext());
                //np.setMinValue(20);
                //np.setMaxValue(450);
                //np.setWrapSelectorWheel(false);
                //np.setValue(100);
                //np.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);

                //NumberPicker.OnValueChangeListener myValChangedListener = new NumberPicker.OnValueChangeListener(){
          //          @Override
                  //  public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    //    Log.i("value is",""+newVal);
                   // }
                //};

                //alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                  //  public void onClick(DialogInterface dialog, int whichButton) {
                       // etGlucose.setText(np.getValue()+"");
                        //value=np.getValue();
                //    }
                //});

                //alert.setView(np);
                //alert.show();
            //}
        //});

        iniciarFecha(view);
        Log.e("fecha2","fecha2"+fechaActual);
        Log.e("Hola","time"+time);

        builder.setTitle("Introduce el nivel de glucosa");

        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if (Store.get().checkInternetConnection(getContext())) {
                    //makeRequest();
                    //totalPoints=points+2;
                    //makeRequestBD("https://dimoapp.000webhostapp.com/updateUsuarios.php?email="+email+"&puntos="+totalPoints);

                    makeRequestBD("https://aplicaciondimo.000webhostapp.com/setGlucose.php?email=" + email + "&fecha=" + time + "&tipo=" + when + "&valor=" + value);


                    Toast.makeText(getContext(), "Valor de glucosa registrado correctamente", Toast.LENGTH_LONG).show();
                }
                dialog.cancel();
                Intent intent = new Intent(getContext(), MainActivity.class);

                startActivity(intent);


            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Log.i("Dialogos", "Confirmacion Cancelada.");
                dialog.cancel();
            }
        });

        return builder.create();
    }







    private void makeRequest(){
        JSONObject data = new JSONObject();
        JSONObject glucose = new JSONObject();
        JSONObject attributes = new JSONObject();

        try {
            attributes.put("value", value);
            attributes.put("check", when);
            glucose.put("type","glucose");
            glucose.put("id",time);
            glucose.put("attributes", attributes);
            data.put("data", glucose);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("CustomDialogGlucose: ",data.toString());

        //Create the list of the cookies, i.e. key=value
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        CustomRequest customRequest = new CustomRequest( Request.Method.POST, URL_BASE,
                data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("BIEN Dialog",response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Manejo de errores
                        Log.e("ERRORAppointment:" ,error.toString());
                     //   int1.putExtra("activity", "Main");
                        Log.i("RECARGA LA COOKIE","Fragment Main");
                       // startActivity(int1);
                    }
                });
        customRequest.setCookies(cookies);
        Volley.newRequestQueue(getContext()).add(customRequest);


    }

    private void iniciarFecha(View view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etDateGlucose.setText(formatDate.format(c.getTime()));
        etTimeGlucose.setText(checkTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE)));
        fechaActual=formatDate.format(c.getTime());
    }

    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    public String checkTime(int hour, int minute){
        String time = null;
        if(hour<10){
            time = "0"+hour;
        }else{
            time=""+hour;
        }
        if(minute<10){
            time = time + ":0"+minute;
        }else{
            time = time +":"+minute;
        }
        return time;
    }



    public void makeRequestBD(String URL){

        //Toast.makeText(getActivity(), ""+URL, Toast.LENGTH_SHORT).show();

        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");
                if ( (response.length()==0)) {
                    totalPoints=points+2;

                    Log.e("hola1", "hola1");

                    try {

                        JSONArray ja = new JSONArray(response);
                        Log.e("hola", "hola");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);


    }



}