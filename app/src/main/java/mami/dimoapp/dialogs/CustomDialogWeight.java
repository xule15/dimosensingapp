package mami.dimoapp.dialogs;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mami.dimoapp.R;
import mami.dimoapp.WebViewLogin;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.model.Store;

/**
 * Created by Fernando on 06/10/2016.
 */

public class CustomDialogWeight extends DialogFragment {


    private NumberPicker npKg, npGr;
    private EditText etDateWeight, etTimeWeight, etWeight;
    private int mYear, mMonth, mDay, mHour, mMinute, year2, month, day, hour, minute2;;
    private float valueFloat;
    private long time;

    protected RequestQueue requestQueue;
    private JsonObjectRequest jsArrayRequest;
    private static final String URL_BASE = "http://mamilab.esi.uclm.es:5050/api/v2.0/weights";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_weight, null);
        builder.setView(view);

        final View viewWeightNp = inflater.inflate(R.layout.weight_picker,null);

        //initialize date for the pickers
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        //initialize default date
        year2 = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute2 = c.get(Calendar.MINUTE);

        time = System.currentTimeMillis()*1000000;


        etDateWeight = (EditText) view.findViewById(R.id.editTextDateWeight);
        etDateWeight.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear,year))>=0){
                                    etDateWeight.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    year2 = year;
                                    month = monthOfYear;
                                    day = dayOfMonth;
                                    time = DateConvert.valuesToMilliseconds(day, month, year2, hour, minute2, 0)*1000000;
                                }else{
                                    Toast.makeText(getContext(),"La fecha introducida no debe ser posterior a la actual", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        etTimeWeight = (EditText)view.findViewById(R.id.editTextTimeWeight);
        etTimeWeight.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TimePickerDialog tpd = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                etTimeWeight.setText(checkTime(hourOfDay, minute));
                                hour = hourOfDay;
                                minute2 = minute;
                                time = DateConvert.valuesToMilliseconds(day, month, year2, hour, minute2, 0)*1000000;
                            }
                        }, mHour, mMinute, true);

                tpd.show();
            }
        });

        etWeight = (EditText)view.findViewById(R.id.editTextWeight);
        etWeight.setText("70");
        etWeight.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

                alert.setTitle("Selecciona tu peso: ");

                final View viewWeightNp = inflater.inflate(R.layout.weight_picker,null);

                npKg = (NumberPicker)viewWeightNp.findViewById(R.id.npkg);
                npGr = (NumberPicker)viewWeightNp.findViewById(R.id.npgr);

                npKg.setMinValue(15);
                npKg.setMaxValue(200);
                npKg.setWrapSelectorWheel(false);
                npKg.setValue(70);

                npGr.setMinValue(0);
                npGr.setMaxValue(99);
                npGr.setWrapSelectorWheel(false);
                npGr.setValue(0);

                NumberPicker.OnValueChangeListener myValChangedListener = new NumberPicker.OnValueChangeListener(){
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        Log.i("value is",""+newVal);
                    }
                };

                alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value;
                        if(npGr.getValue()<10){
                            value = "0"+npGr.getValue();
                        }else{
                            value=""+npGr.getValue();
                        }

                        etWeight.setText(npKg.getValue()+"."+ value);
                        valueFloat = Float.parseFloat(npKg.getValue()+"."+value);

                    }
                });

                alert.setView(viewWeightNp);
                alert.show();
            }
        });

        iniciarFecha(view);

        builder.setTitle("Introduce el peso")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i("Dialogos", "Confirmacion Aceptada.");

                        if(Store.get().checkInternetConnection(getContext())) {
                            makeRequest();
                            Toast.makeText(getContext(), "Valor registrado correctamente", Toast.LENGTH_LONG).show();
                        }
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i("Dialogos", "Confirmacion Cancelada.");
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    private void makeRequest(){

        JSONObject attributes = new JSONObject();
        JSONObject weight = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            attributes.put("value", valueFloat);
            weight.put("type", "weights");
            weight.put("id", time);
            weight.put("attributes", attributes);
            data.put("data", weight);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("CustomDialogWeight: ",data.toString());

        //Create the list of the cookies, i.e. key=value
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, URL_BASE,
                data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("BIEN",response.toString());
                        //showToast();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Manejo de errores
                        Log.e("ERROR:" ,error.toString());
                        Intent int1 =  new Intent(getActivity(), WebViewLogin.class);
                        int1.putExtra("activity", "Main");
                        Log.i("RECARGA LA COOKIE","Fragment Main");
                        startActivity(int1);
                    }
                });
        customRequest.setCookies(cookies);
        Volley.newRequestQueue(getContext()).add(customRequest);
    }

    private void iniciarFecha(View view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etDateWeight.setText(formatDate.format(c.getTime()));
        etTimeWeight.setText(checkTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE)));
    }

    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    public String checkTime(int hour, int minute){
        String time = null;
        if(hour<10){
            time = "0"+hour;
        }else{
            time=""+hour;
        }
        if(minute<10){
            time = time + ":0"+minute;
        }else{
            time = time +":"+minute;
        }

        return time;
    }
    private void showToast() {
        Toast.makeText(getActivity().getApplicationContext(),"Peso registrado correctamente",Toast.LENGTH_SHORT).show();
    }
}