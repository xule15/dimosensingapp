package mami.dimoapp.dialogs;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mami.dimoapp.R;

import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.CustomRequest2;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.model.Appointment;
import mami.dimoapp.model.Store;

/**
 * Created by Fernando on 13/10/2016.
 */

public class CustomDialogAppointment extends DialogFragment {
    EditText etDateAppointment, etTimeAppointment, etTitle, etDescription, etPlace;
    int mYear, mMonth, mDay, mHour, mMinute, year2, month, day, hour, minutes;
    private static final int ALARM_REQUEST_CODE = 1;
    private String description, title, place;
    private long time;
    private static final String URL_BASE = "http://mamilab.esi.uclm.es:5050/api/v2.0/appointments";;

    public CustomDialogAppointment() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createAppointmentDialog();
    }

    public AlertDialog createAppointmentDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_appointment, null);
        builder.setView(view);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        day = mDay;
        month = mMonth;
        year2 = mYear;
        hour = mHour;
        minutes = mMinute;

        time = DateConvert.valuesToMilliseconds(day, month, year2, hour, minutes,0);

        description = title = place = "";

        etTitle = (EditText)view.findViewById(R.id.editTextTitleAppointment);
        etDescription = (EditText)view.findViewById(R.id.editTextDescriptionAppointment);
        etPlace = (EditText)view.findViewById(R.id.editTextPlaceAppointment);

        etDateAppointment = (EditText) view.findViewById(R.id.editTextDateAppointment);
        etDateAppointment.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                day = dayOfMonth;
                                month = monthOfYear+1;
                                year2 = year;
                                time = DateConvert.valuesToMilliseconds(day, month, year2, hour, minutes, 0);
                                etDateAppointment.setText(checkDate(day,month,year2));
                            }
                        }, mYear, mMonth-1, mDay);
                dpd.show();
            }
        });

        etTimeAppointment = (EditText)view.findViewById(R.id.editTextTimeAppointment);
        etTimeAppointment.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TimePickerDialog tpd = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                etTimeAppointment.setText(checkTime(hourOfDay, minute));
                                hour= hourOfDay;
                                minutes = minute;
                                time = DateConvert.valuesToMilliseconds(day, month, year2, hour, minutes, 0 );
                            }
                        }, mHour, mMinute, true);

                tpd.show();
            }
        });

        iniciarFecha(view);

        builder.setTitle("Introduce la cita")
            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //Date, Time and title must no be blank
                    if(!etTitle.getText().toString().equals("")||
                            etDescription.getText().toString().equals("")) {

                        Appointment appointment = new Appointment(DateConvert.valuesToMilliseconds(day, month, year2, hour, minutes, 0)*1000000,
                                etTitle.getText().toString().trim(), etPlace.getText().toString().trim(),
                                etDescription.getText().toString().trim());

                        title = etTitle.getText().toString().trim();
                        description = etDescription.getText().toString().trim();
                        place = etPlace.getText().toString().trim();

                        //makeRequest();
                        Store.get().setdailyPoints(Store.get().getdailyPoints()+5);
                        makeRequestBD("http://aplicaciondimo.com/updateUser.php?email=" +Store.get().getEmail()+"&dailyPoints="+Store.get().getdailyPoints()+"&weeklyPoints="+Store.get().getweeklyPoints()+"&controlDay1="+Store.get().getControlDay1()+
                                "&controlDay2="+Store.get().getControlDay2()+"&controlDay3="+Store.get().getControlDay3()
                                +"&controlDay4="+Store.get().getControlDay4()+"&controlDay5="+Store.get().getControlDay5()+"&controlDay6="+Store.get().getControlDay6()+
                                "&controlDay7="+Store.get().getControlDay7()+ "&category="+Store.get().getCategoria());
                        makeRequestBD("http://aplicaciondimo.com/setAppointment.php?email="+Store.get().getEmail()+"&date="+time+"&title="+title+"&description="+description+"&place="+place);

                        Toast.makeText(getContext(), "Valor registrado correctamente", Toast.LENGTH_LONG).show();
                            setAlarm(appointment);

                        dialog.cancel();
                    }else{
                        Toast.makeText(getContext(),"El título y la descripción debem estar informados",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            })
            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Log.i("Dialogos", "Confirmacion Cancelada.");
                    dialog.cancel();
                }
            });

        return builder.create();
    }



    private void makeRequestBD(String URL){

        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");
                if ( (response.length()>0)){
                    try {

                        JSONArray ja = new JSONArray(response);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    private void setAlarm(Appointment appointment){
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");
        notificationIntent.putExtra("description", appointment.getTitle()+ " "+ DateConvert.millisecondsToDateStringWithoutSeconds(appointment.getTime()));

        PendingIntent broadcast = PendingIntent.getBroadcast(getContext(), 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10);
        //alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, appointment.getTime(), broadcast); //1 hora antes
        //alarmManager.setExact(AlarmManager.RTC_WAKEUP, appointment.getTime()+90000, broadcast);
    }

    private void iniciarFecha(View view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        etDateAppointment.setText(format.format(c.getTime()));
        etTimeAppointment.setText(checkTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE)));
    }

    //Date format
    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    //Time format
    public String checkTime(int hour, int minute){
        String time = null;
        if(hour<10){
            time = "0"+hour;
        }else{
            time=""+hour;
        }
        if(minute<10){
            time = time + ":0"+minute;
        }else{
            time = time +":"+minute;
        }
        return time;
    }
}
