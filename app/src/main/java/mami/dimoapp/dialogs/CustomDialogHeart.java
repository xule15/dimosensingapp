package mami.dimoapp.dialogs;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mami.dimoapp.R;
import mami.dimoapp.WebViewLogin;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.model.Store;

/**
 * Created by Fernando on 06/10/2016.
 */

public class CustomDialogHeart extends DialogFragment{


    private NumberPicker np;
    private EditText etDateHeart, etTimeHeart, etHeart;
    private int mYear, mMonth, mDay, mHour, mMinute, year2, month, day, hour, minute2;
    private float valueHeart;
    private long time;
    private static final String URL_BASE = "http://mamilab.esi.uclm.es:5050/api/v2.0/heart_rates";
    private CustomRequest customRequest;

    public CustomDialogHeart() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createHeartDialog();
    }

    public AlertDialog createHeartDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_heart, null);
        builder.setView(view);

        //initialize date for the pickers
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        //initialize default date
        year2 = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute2 = c.get(Calendar.MINUTE);

        time = System.currentTimeMillis()*1000000;
        valueHeart = 80;

        etDateHeart = (EditText) view.findViewById(R.id.editTextDateHeart);
        etDateHeart.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear+1,year))>=0){
                                    etDateHeart.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    year2 = year;
                                    month = monthOfYear+1;
                                    day = dayOfMonth;
                                    time = DateConvert.valuesToMilliseconds(day, month, year2, hour, minute2, 0)*1000000;
                                }else{
                                    Toast.makeText(getContext(),"La fecha introducida no debe ser posterior a la actual", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYear, mMonth-1, mDay);
                dpd.show();
            }
        });

        etTimeHeart = (EditText)view.findViewById(R.id.editTextTimeHeart);
        etTimeHeart.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TimePickerDialog tpd = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                etTimeHeart.setText(checkTime(hourOfDay, minute));
                                hour = hourOfDay;
                                minute2 = minute;
                                time = DateConvert.valuesToMilliseconds(day, month, year2, hour, minute2, 0)*1000000;
                            }
                        }, mHour, mMinute, true);

                tpd.show();
            }
        });

        etHeart = (EditText)view.findViewById(R.id.editTextHeart);
        etHeart.setText(80+"");
        etHeart.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

                alert.setTitle("Selecciona el pulso cardiaco: ");

                np = new NumberPicker(getContext());
                np.setMinValue(1);
                np.setMaxValue(200);
                np.setWrapSelectorWheel(false);
                np.setValue(80);
                np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                NumberPicker.OnValueChangeListener myValChangedListener = new NumberPicker.OnValueChangeListener(){
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        Log.i("value is",""+newVal);
                    }
                };

                alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        etHeart.setText(np.getValue()+"");
                        valueHeart = (float)np.getValue();
                    }
                });

                alert.setView(np);
                alert.show();
            }
        });

        iniciarFecha(view);

        builder.setTitle("Introduce el pulso cardiaco")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(Store.get().checkInternetConnection(getContext())) {
                            makeRequest();
                            Toast.makeText(getContext(), "Valor registrado correctamente", Toast.LENGTH_LONG).show();
                        }
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i("Dialogos", "Confirmacion Cancelada.");
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    private void makeRequest(){
        JSONObject data = new JSONObject();
        JSONObject heart = new JSONObject();
        JSONObject attributes = new JSONObject();

        try {
            attributes.put("value", valueHeart);
            heart.put("type","heart-rates");
            heart.put("id",time);
            heart.put("attributes",attributes);
            data.put("data", heart);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("CustomDialogHeart: ",data.toString());

        //Create the list of the cookies, i.e. key=value
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        customRequest = new CustomRequest(Request.Method.POST, URL_BASE, data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("BIEN",response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Manejo de errores
                        // As of f605da3 the following should work
                        NetworkResponse response = error.networkResponse;
                        if (error instanceof ServerError && response != null) {
                            try {
                                String res = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                // Now you can use any deserializer to make sense of data
                                JSONObject obj = new JSONObject(res);
                            } catch (UnsupportedEncodingException e1) {
                                // Couldn't properly decode data to string
                                e1.printStackTrace();
                            } catch (JSONException e2) {
                                // returned data is not JSONObject?
                                e2.printStackTrace();
                            }
                        }
                        Log.e("ERROR:" ,error.toString());
                        Intent int1 =  new Intent(getActivity(), WebViewLogin.class);
                        int1.putExtra("activity", "Main");
                        Log.i("RECARGA LA COOKIE","Fragment Main");
                        startActivity(int1);
                    }
                });
        customRequest.setCookies(cookies);
        // and finally add the request to the queue
        Volley.newRequestQueue(getContext()).add(customRequest);
    }

    private void iniciarFecha(View view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etDateHeart.setText(formatDate.format(c.getTime()));
        etTimeHeart.setText(checkTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE)));
    }
    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    public String checkTime(int hour, int minute){
        String time = null;
        if(hour<10){
            time = "0"+hour;
        }else{
            time=""+hour;
        }
        if(minute<10){
            time = time + ":0"+minute;
        }else{
            time = time +":"+minute;
        }
        return time;
    }
}