package mami.dimoapp.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by Fernando on 06/10/2016.
 */

public class CustomDialogDeleteAppointment extends DialogFragment{

    public CustomDialogDeleteAppointment() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createHeartDialog();
    }

    public AlertDialog createHeartDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Cita médica")
            .setMessage("¿Deseas eliminar la cita médica?")

            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        })
            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });


        return builder.create();
    }

}