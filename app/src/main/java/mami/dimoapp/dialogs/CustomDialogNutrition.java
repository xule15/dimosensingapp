package mami.dimoapp.dialogs;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mami.dimoapp.R;
import mami.dimoapp.WebViewLogin;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.model.Store;

/**
 * Created by Fernando on 13/10/2016.
 */

public class CustomDialogNutrition extends DialogFragment {

    private EditText etDateNutrition, etTimeNutrition, etQuantity;
    private int mYear, mMonth, mDay, mHour, mMinute, year2, month, day, hour, minute2;;
    private Spinner spKind;
    private NumberPicker npHundreds, npTens, npUnits;
    private float valueFloat;
    private String kindFood;
    private long time;
    private static final String URL_BASE = "http://mamilab.esi.uclm.es:5050/api/v2.0/consumptions";

    public enum KindOfFood {
        VEGETABLES_LEGUMES, MEAT, FRUIT_NUTS, CEREALS, FISH_SEAFOOD,
                EGGS_DAIRY, OIL, SWEETS, DRINKS, FAST_FOOD, SPICIES, OTHER
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_nutrition, null);
        builder.setView(view);

        spKind = (Spinner)view.findViewById(R.id.spinnerKind);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.kindOfFood, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spKind.setAdapter(adapter);

        //initialize date for the pickers
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        //initialize default date
        year2 = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute2 = c.get(Calendar.MINUTE);

        time = System.currentTimeMillis()*1000000;

        etDateNutrition = (EditText) view.findViewById(R.id.editTextDateNutrition);
        etDateNutrition.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear,year))>=0){
                                    etDateNutrition.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    year2 = year;
                                    month = monthOfYear;
                                    day = dayOfMonth;
                                    time = DateConvert.valuesToMilliseconds(day, month, year2, hour, minute2, 0)*1000000;
                                }else{
                                    Toast.makeText(getContext(),"La fecha introducida no debe ser posterior a la actual", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        etTimeNutrition = (EditText)view.findViewById(R.id.editTextTimeNutrition);
        etTimeNutrition.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TimePickerDialog tpd = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                etTimeNutrition.setText(checkTime(hourOfDay, minute));
                                hour = hourOfDay;
                                minute2 = minute;
                                time = DateConvert.valuesToMilliseconds(day, month, year2, hour, minute2, 0)*1000000;
                            }
                        }, mHour, mMinute, true);

                tpd.show();
            }
        });

        spKind.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                kindFood = parent.getItemAtPosition(pos).toString();
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        etQuantity = (EditText)view.findViewById(R.id.editTextGrams);
        etQuantity.setText("70");
        etQuantity.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

                alert.setTitle("Selecciona la cantidad: ");

                final View viewGramsNp = inflater.inflate(R.layout.grams_picker,null);

                npHundreds = (NumberPicker)viewGramsNp.findViewById(R.id.npHundreds);
                npTens = (NumberPicker)viewGramsNp.findViewById(R.id.npTens);
                npUnits = (NumberPicker)viewGramsNp.findViewById(R.id.npUnits);

                npUnits.setMinValue(1);
                npUnits.setMaxValue(9);
                npHundreds.setMaxValue(9);
                npTens.setMaxValue(9);

                alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int hundreds = npHundreds.getValue();
                        int tens = npTens.getValue();
                        int units = npUnits.getValue();
                        if(npHundreds.getValue()==0 && npTens.getValue()==0){
                            etQuantity.setText(units+"");
                            valueFloat = Float.parseFloat(units+"");
                        }else if(npHundreds.getValue()==0 && npTens.getValue()!=0){
                            etQuantity.setText(npTens.getValue() + "" + npUnits.getValue() + "");
                            valueFloat = Float.parseFloat(npTens.getValue() + "" + npUnits.getValue() + "");
                        }else{
                            etQuantity.setText(hundreds+ "" + npTens.getValue() + "" + npUnits.getValue() + "");
                            valueFloat = Float.parseFloat(npHundreds.getValue() + "" + npTens.getValue() + "" + npUnits.getValue() + "");
                        }

                    }
                });

                alert.setView(viewGramsNp);
                alert.show();
            }
        });

        iniciarFecha();


        builder.setTitle("Introduce el alimento")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        makeRequest();

                        Log.i("Dialogos", "Confirmacion Aceptada.");
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i("Dialogos", "Confirmacion Cancelada.");
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

    private void makeRequest(){
        JSONObject attributes = new JSONObject();
        JSONObject consumptions = new JSONObject();
        JSONObject consumptions2 = new JSONObject();
        JSONObject consumedFoods = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            attributes.put("time", valueFloat);
            consumptions.put("type", "consumptions");
            consumptions.put("id", time);
            /*consumptions2.put("relationships", value);
            consumptions2.put("relationships", value);*/
            data.put("data", consumptions);
            data.put("data", consumptions2);
            Log.e("hola", data.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("CustomDialogNutrition: ",data.toString());

        //Create the list of the cookies, i.e. key=value
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, URL_BASE,
                data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("BIEN",response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Manejo de errores
                        Log.e("ERROR:" ,error.getMessage());
                        Intent int1 =  new Intent(getActivity(), WebViewLogin.class);
                        int1.putExtra("activity", "Main");
                        Log.i("RECARGA LA COOKIE","Fragment Main");
                        startActivity(int1);
                    }
                });

        customRequest.setCookies(cookies);
        Volley.newRequestQueue(getContext()).add(customRequest);
    }

    private void iniciarFecha() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etDateNutrition.setText(formatDate.format(c.getTime()));
        etTimeNutrition.setText(checkTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE)));
    }

    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    public String checkTime(int hour, int minute){
        String time = null;
        if(hour<10){
            time = "0"+hour;
        }else{
            time=""+hour;
        }
        if(minute<10){
            time = time + ":0"+minute;
        }else{
            time = time +":"+minute;
        }

        return time;
    }

}
