package mami.dimoapp.dialogs;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mami.dimoapp.R;
import mami.dimoapp.WebViewLogin;
import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.model.Store;

/**
 * Created by Fernando on 06/10/2016.
 */

public class CustomDialogInsulin extends DialogFragment{


    private NumberPicker npKg, npGr;
    private EditText etDateInsulin, etTimeInsulin, etDosisInsulin;
    private Spinner spinnerInsulin;
    private int mYear, mMonth, mDay, mHour, mMinute, year2, month, day, hour, minute2;
    private long time;
    private int insulineType;
    private float dosis;
    private static final String URL_BASE = "http://mamilab.esi.uclm.es:5050/api/v2.0/insulin";
    private TextView decimales, unidades;

    public CustomDialogInsulin() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createGlucoseDialog();
    }

    public AlertDialog createGlucoseDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_insuline, null);
        builder.setView(view);

        //initialize date for the pickers
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        //initialize default date
        year2 = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute2 = c.get(Calendar.MINUTE);

        time = System.currentTimeMillis()*1000000;

        insulineType = 0;
        dosis = 1.0f;

        spinnerInsulin = (Spinner)view.findViewById(R.id.spinnerTypeInsuline);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.kindOfInsuline, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerInsulin.setAdapter(adapter);

        spinnerInsulin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if(parent.getItemAtPosition(pos).toString().equals("Rápida")){
                    insulineType = 0;
                }else if(parent.getItemAtPosition(pos).toString().equals("Corta")){
                    insulineType = 1;
                }else if(parent.getItemAtPosition(pos).toString().equals("Intermedia")){
                    insulineType = 2;
                }else{
                    insulineType = 3;
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        etDateInsulin = (EditText) view.findViewById(R.id.editTextDateInsuline);
        etDateInsulin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear,year))>=0){
                                    etDateInsulin.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    year2 = year;
                                    month = monthOfYear;
                                    day = dayOfMonth;
                                    time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2,0)*1000000;
                                }else{
                                    Toast.makeText(getContext(),"La fecha introducida no debe ser posterior a la actual", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        etTimeInsulin = (EditText)view.findViewById(R.id.editTextTimeInsuline);
        etTimeInsulin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TimePickerDialog tpd = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                etTimeInsulin.setText(checkTime(hourOfDay, minute));
                                hour = hourOfDay;
                                minute2 = minute;
                                time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2,0)*1000000;
                            }
                        }, mHour, mMinute, true);

                tpd.show();
            }
        });

        etDosisInsulin = (EditText)view.findViewById(R.id.editTextDosisInsuline);
        etDosisInsulin.setText(1+"");
        etDosisInsulin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

                alert.setTitle("Selecciona la dosis de insulina inyectada: ");

                final View viewWeightNp = inflater.inflate(R.layout.weight_picker,null);

                npKg = (NumberPicker)viewWeightNp.findViewById(R.id.npkg);
                npGr = (NumberPicker)viewWeightNp.findViewById(R.id.npgr);
                unidades = (TextView)viewWeightNp.findViewById(R.id.textViewUnidades);
                decimales = (TextView)viewWeightNp.findViewById(R.id.textViewDecimales);
                unidades.setText("Unidades");
                decimales.setText("Decimales");

                npKg.setMinValue(0);
                npKg.setMaxValue(99);
                npKg.setWrapSelectorWheel(false);
                npKg.setValue(1);

                npGr.setMinValue(0);
                npGr.setMaxValue(99);
                npGr.setWrapSelectorWheel(false);
                npGr.setValue(0);

                NumberPicker.OnValueChangeListener myValChangedListener = new NumberPicker.OnValueChangeListener(){
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        Log.i("value is",""+newVal);
                    }
                };

                alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value=""+npGr.getValue();

                        etDosisInsulin.setText(npKg.getValue()+"."+ value);
                        dosis = Float.parseFloat(npKg.getValue()+"."+value);
                    }
                });

                alert.setView(viewWeightNp);
                alert.show();
            }
        });

        iniciarFecha(view);

        builder.setTitle("Introduce la insulina inyectada")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if(Store.get().checkInternetConnection(getContext())) {
                            makeRequest();
                            Toast.makeText(getContext(), "Valor registrado correctamente", Toast.LENGTH_LONG).show();
                        }
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i("Dialogos", "Confirmacion Cancelada.");
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    private void makeRequest(){
        JSONObject data = new JSONObject();
        JSONObject insulin = new JSONObject();
        JSONObject attributes = new JSONObject();

        try {
            attributes.put("type", insulineType);
            attributes.put("dose", dosis);
            insulin.put("type", "insulin");
            insulin.put("id", time);
            insulin.put("attributes", attributes);
            data.put("data", insulin);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("CustomDialogInsulin: ",data.toString());

        //Create the list of the cookies, i.e. key=value
        List<String> cookies = new ArrayList<>();
        cookies.add(Store.get().getCookie());

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, URL_BASE, data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("BIEN",response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Manejo de errores
                        Log.e("ERROR:" ,error.toString());
                        Intent int1 =  new Intent(getActivity(), WebViewLogin.class);
                        int1.putExtra("activity", "Main");
                        Log.i("RECARGA LA COOKIE","Fragment Main");
                        startActivity(int1);
                    }
                });

        customRequest.setCookies(cookies);
        Volley.newRequestQueue(getContext()).add(customRequest);
    }

    private void iniciarFecha(View view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etDateInsulin.setText(formatDate.format(c.getTime()));
        etTimeInsulin.setText(checkTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE)));
    }
    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    public String checkTime(int hour, int minute){
        String time = null;
        if(hour<10){
            time = "0"+hour;
        }else{
            time=""+hour;
        }
        if(minute<10){
            time = time + ":0"+minute;
        }else{
            time = time +":"+minute;
        }
        return time;
    }
}