package mami.dimoapp;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.data.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.CustomRequest2;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.fragments.FragmentGlucose;
import mami.dimoapp.model.GlucosePoint;
import mami.dimoapp.model.Store;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class GlucoseActivity extends AppCompatActivity implements View.OnClickListener{
    private NumberPicker np;
    private EditText etDateGlucose, etTimeGlucose, etGlucose;
    private int mYear, mMonth, mDay, mHour, mMinute, value, year2, month, day, hour, minute2;
    private Spinner spinnerWhen;
    private int when;
    private long time;
    private CustomRequest customRequest;
    private String fechaActual;
    private String email;
    private int dailyPoints,weeklyPoints,startWeekDay;
    private Button okButton;
    private long start, end;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glucose);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Glucemia");
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            //actionBar.setHomeAsUpIndicator(R.drawable.ic_close_dark);
        }


        //initialize date for the pickers

        email=Store.get().getEmail();
        Store.get().setExists(true);
        dailyPoints=Store.get().getdailyPoints();
        weeklyPoints=Store.get().getweeklyPoints();
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        //initialize default date
        year2 = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute2 = c.get(Calendar.MINUTE);
        okButton = (Button)findViewById(R.id.buttonOk);
        okButton.setOnClickListener(this);

        // time = System.currentTimeMillis()*1000000;
        time = System.currentTimeMillis();
        end=time-604800000;
        when = 0;
        value= 100;
        //checkControl(time,end);




        //Types of insuline adapter
        spinnerWhen = (Spinner) findViewById(R.id.spinnerWhen);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.whenGlucose, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWhen.setAdapter(adapter);

        spinnerWhen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if(parent.getItemAtPosition(pos).toString().equals("Antes de comer")){
                    when = 0;
                }else if(parent.getItemAtPosition(pos).toString().equals("Después de comer")){
                    when = 1;
                }else{
                    when = 2;
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        etDateGlucose = (EditText)findViewById(R.id.editTextDateInsuline);
        etDateGlucose.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(v.getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear,year))>=0){
                                    etDateGlucose.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    year2 = year;
                                    month = monthOfYear;
                                    day = dayOfMonth;
                                    time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2, 0);
                                    end=time-604800000;
                                    fechaActual=checkDate(dayOfMonth,monthOfYear+1,year);
                                }else{
                                    Toast.makeText(getApplicationContext(),"La fecha introducida no debe ser posterior a la actual", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        etTimeGlucose = (EditText)findViewById(R.id.editTextTimeInsuline);
        etTimeGlucose.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TimePickerDialog tpd = new TimePickerDialog(v.getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                etTimeGlucose.setText(checkTime(hourOfDay, minute));
                                hour = hourOfDay;
                                minute2 = minute;
                                //time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2, 0)*1000000;
                                time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2, 0);


                            }
                        }, mHour, mMinute, true);

                tpd.show();
            }
        });

        etGlucose = (EditText) findViewById(R.id.editTextInsuline);
        etGlucose.setText(100+"");


        iniciarFecha(this);

    }
    private void iniciarFecha(GlucoseActivity view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etDateGlucose.setText(formatDate.format(c.getTime()));
        etTimeGlucose.setText(checkTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE)));
        fechaActual=formatDate.format(c.getTime());
    }
    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    public String checkTime(int hour, int minute){
        String time = null;
        if(hour<10){
            time = "0"+hour;
        }else{
            time=""+hour;
        }
        if(minute<10){
            time = time + ":0"+minute;
        }else{
            time = time +":"+minute;
        }
        return time;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_appointment, menu);

        //setBadgeCount(this, mCartMenuIcon, ""+ 1);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            NavUtils.navigateUpTo(this, intent);
            return true;
        }
        if(id == R.id.action_Appointment_help) {
            ShowcaseConfig config = new ShowcaseConfig();
            MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this);
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(etDateGlucose)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_DateGlucose))
                            .build()
            );
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(etTimeGlucose)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_HourGlucose))
                            .build()
            );
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(etGlucose)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_ValueGlucose))
                            .build()
            );

            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(spinnerWhen)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_WhenGlucose))
                            .build()
            );


            //if(okButton.isShown()) {
            sequence.addSequenceItem(okButton,
                    getString(R.string.HelpText_Save), getString(R.string.Helptext_Next));
            //}



            sequence.start();


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonOk:

                value=Integer.valueOf(String.valueOf(etGlucose.getText()));
                checkControl(time,end);
                makeRequestBD("http://aplicaciondimo.com/setGlucose.php?email=" +email+"&date="+time+"&type="+when+"&value="+value);


                Toast.makeText(getApplicationContext(),"Valor de glucemia introducido correctamente", Toast.LENGTH_SHORT).show();
                break;

            case R.id.action_Appointment_help:
                ShowcaseConfig config = new ShowcaseConfig();
                config.setDelay(500); // half second between each showcase view
                View menuItemView = findViewById(R.id.action_appointment);
                MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this);


                sequence.setConfig(config);
                sequence.addSequenceItem(menuItemView, getString(R.string.HelpText_Appointment) , getString(R.string.Helptext_Next));
                //if(okButton.isShown()) {
                    sequence.addSequenceItem(okButton,
                            getString(R.string.HelpText_FabMenu), getString(R.string.Helptext_Next));
                //}



                sequence.start();


                break;

        }

    }

    //@Override
    //public void onResume() {
      //  super.onResume();
        //if(Store.get().getEmail()==null){
          //  Intent int1 =  new Intent(this, SignInActivity.class);
            //int1.putExtra("activity", "GlucoseActivity");
            //startActivity(int1);
            //Toast.makeText(this,"Conectando al servidor",Toast.LENGTH_SHORT).show();
        //}
    //}





    public void makeRequestBD(String URL){

        //Toast.makeText(getActivity(), ""+URL, Toast.LENGTH_SHORT).show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");




                    try {

                        JSONArray ja = new JSONArray(response);




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            //}
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error","Errror "+error);

            }
        });

        queue.add(stringRequest);
        finish();

    }
    public void checkControl(long time,long end){
        Store.get().setdailyPoints(Store.get().getdailyPoints()+5);
        Calendar c=Calendar.getInstance(Locale.FRANCE);
        Date d= new Date(time);
        c.setTime(d);
        final int weekOftime=c.get(Calendar.WEEK_OF_YEAR);

        final int  dayOfWeek=c.get(Calendar.DAY_OF_WEEK);

        switch (dayOfWeek){
            case 1:
                startWeekDay=7;
                break;
            case 2:
                startWeekDay=1;
                break;
            case 3:
                startWeekDay=2;
                break;
            case 4:
                startWeekDay=3;
                break;
            case 5:
                startWeekDay=4;
                break;
            case 6:
                startWeekDay=5;
                break;
            case 7:
                startWeekDay=6;
                break;
        }

        String url = "http://aplicaciondimo.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+end+"&end="+time;

        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject glucose = (JSONObject) jsonArray1.get(i);
                                    long date = Long.parseLong(String.valueOf(glucose.get("date")));
                                    Calendar c=Calendar.getInstance();
                                    Date dateofGlucose= new Date(date);
                                    c.setTime(dateofGlucose);
                                    int weekOfDate=c.get(Calendar.WEEK_OF_YEAR);
                                    //Control changue of week
                                   if(weekOfDate==weekOftime) {


                                       if (startWeekDay == 1 && Store.get().getControlDay1() == 0) {
                                           Store.get().setControlDay1(1);


                                       }
                                       if (startWeekDay == 2 && Store.get().getControlDay2() == 0) {
                                           Store.get().setControlDay2(1);

                                       }
                                       if (startWeekDay == 3 && Store.get().getControlDay3() == 0) {
                                           Store.get().setControlDay3(1);

                                       }
                                       if (startWeekDay == 4 && Store.get().getControlDay4() == 0) {
                                           Store.get().setControlDay4(1);


                                       }
                                       if (startWeekDay == 5 && Store.get().getControlDay5() == 0) {
                                           Store.get().setControlDay5(1);

                                       }

                                       if (startWeekDay == 6 && Store.get().getControlDay6() == 0) {

                                           Store.get().setControlDay6(1);
                                       }
                                       if (startWeekDay == 7 && Store.get().getControlDay7() == 0) {
                                           Store.get().setControlDay7(1);
                                       }
                                   }
                                    if(Store.get().getControlDay1()==1 && Store.get().getControlDay2()==1 && Store.get().getControlDay3()==1
                                            && Store.get().getControlDay4()==1 && Store.get().getControlDay5()==1 && Store.get().getControlDay6()==1 &&
                                            Store.get().getControlDay7()==1) {


                                        Store.get().setweeklyPoints(Store.get().getweeklyPoints()+250);
                                        Store.get().setControlDay1(0);
                                        Store.get().setControlDay2(0);
                                        Store.get().setControlDay3(0);
                                        Store.get().setControlDay4(0);
                                        Store.get().setControlDay5(0);
                                        Store.get().setControlDay6(0);
                                        Store.get().setControlDay7(0);
                                        makeRequestBD("http://aplicaciondimo.com/updateUser.php?email=" +Store.get().getEmail()+"&dailyPoints="+Store.get().getdailyPoints()+"&weeklyPoints="+Store.get().getweeklyPoints()+"&controlDay1="+Store.get().getControlDay1()+
                                                "&controlDay2="+Store.get().getControlDay2()+"&controlDay3="+Store.get().getControlDay3()
                                                +"&controlDay4="+Store.get().getControlDay4()+"&controlDay5="+Store.get().getControlDay5()+"&controlDay6="+Store.get().getControlDay6()+
                                                "&controlDay7="+Store.get().getControlDay7()+ "&category="+Store.get().getCategoria());
                                    }

                                }

                            }

                            makeRequestBD("http://aplicaciondimo.com/updateUser.php?email=" +Store.get().getEmail()+"&dailyPoints="+Store.get().getdailyPoints()+"&weeklyPoints="+Store.get().getweeklyPoints()+"&controlDay1="+Store.get().getControlDay1()+
                                    "&controlDay2="+Store.get().getControlDay2()+"&controlDay3="+Store.get().getControlDay3()
                                    +"&controlDay4="+Store.get().getControlDay4()+"&controlDay5="+Store.get().getControlDay5()+"&controlDay6="+Store.get().getControlDay6()+
                                    "&controlDay7="+Store.get().getControlDay7()+ "&category="+Store.get().getCategoria());




                                } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        // and finally add the request to the queue
        Volley.newRequestQueue(this).add(customRequest);
    }


}
