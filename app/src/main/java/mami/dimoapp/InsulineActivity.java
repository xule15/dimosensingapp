package mami.dimoapp;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.data.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.CustomRequest2;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.model.Store;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class InsulineActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText etDateInsuline, etTimeInsuline, etInsuline,etDateGlucose,etTimeGlucose,etIndiceGlucose;
    private TextView tvDateGlucose,tvTimeGlucose,tvIndiceGlucose,tvMgGlucose,tvWhenGlucose;
    private CheckBox checkGlucose;
    private int mYearInsuline, mMonthInsuline, mDayInsuline, mHourInsuline, mMinuteInsuline, valueInsuline, yearInsuline, monthInsuline, dayInsuline, hourInsuline, minuteInsuline,mYear, mMonth, mDay, mHour, mMinute, value, year2, month, day, hour, minute2;
    private Spinner spinnerInsulin,spinnerWhenGlucose;
    private int insulineType;
    private long timeInsuline,time,end;
    private int when, startWeekDay;
    private String fechaActualInsuline,fechaActual;
    private String email;
    private int dailyPoints,weeklyPoints;

    private Button okButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insuline);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Insulina/Glucemia");
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            //actionBar.setHomeAsUpIndicator(R.drawable.ic_close_dark);
        }


        //initialize date for the pickers

        email=Store.get().getEmail();
        Store.get().setExists(true);
        dailyPoints=Store.get().getdailyPoints();
        weeklyPoints=Store.get().getweeklyPoints();

        final Calendar c = Calendar.getInstance();
        mYearInsuline = c.get(Calendar.YEAR);
        mMonthInsuline = c.get(Calendar.MONTH);
        mDayInsuline = c.get(Calendar.DAY_OF_MONTH);
        mHourInsuline = c.get(Calendar.HOUR_OF_DAY);
        mMinuteInsuline = c.get(Calendar.MINUTE);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        //initialize default date
        year2 = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute2 = c.get(Calendar.MINUTE);

        //initialize default date
        yearInsuline = c.get(Calendar.YEAR);
        monthInsuline = c.get(Calendar.MONTH);
        dayInsuline = c.get(Calendar.DAY_OF_MONTH);
        hourInsuline = c.get(Calendar.HOUR_OF_DAY);
        minuteInsuline = c.get(Calendar.MINUTE);
        okButton = (Button)findViewById(R.id.buttonOk);
        okButton.setOnClickListener(this);

        // time = System.currentTimeMillis()*1000000;
        timeInsuline = System.currentTimeMillis();
        insulineType = 0;
        valueInsuline= 100;
        time = System.currentTimeMillis();
        when = 0;
        value= 100;
        time = System.currentTimeMillis();
        end=time-604800000;





        spinnerInsulin = (Spinner) findViewById(R.id.spinnerTypeInsuline);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.kindOfInsuline, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerInsulin.setAdapter(adapter2);

        spinnerInsulin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if(parent.getItemAtPosition(pos).toString().equals("Rápida")){
                    insulineType = 0;
                }else if(parent.getItemAtPosition(pos).toString().equals("Corta")){
                    insulineType = 1;
                }else if(parent.getItemAtPosition(pos).toString().equals("Intermedia")){
                    insulineType = 2;
                }else{
                    insulineType = 3;
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        tvDateGlucose = (TextView) findViewById(R.id.tvDateGlucose);
        etDateGlucose = (EditText)findViewById(R.id.editTextDateGlucose);
        etTimeGlucose= (EditText) findViewById(R.id.editTextTimeGlucose);
        etIndiceGlucose=(EditText) findViewById(R.id.editTextGlucose);
        tvTimeGlucose=(TextView) findViewById(R.id.tvTimeGlucose);
        tvIndiceGlucose=(TextView) findViewById(R.id.tvIndiceGlucose);
        tvMgGlucose=(TextView) findViewById(R.id.tvMgGlucose);
        tvWhenGlucose=(TextView) findViewById(R.id.tvWhenGlucose);
        spinnerWhenGlucose=(Spinner) findViewById(R.id.spinnerWhenGlucose);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.whenGlucose, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWhenGlucose.setAdapter(adapter);

        spinnerWhenGlucose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if(parent.getItemAtPosition(pos).toString().equals("Antes de comer")){
                    when = 0;
                }else if(parent.getItemAtPosition(pos).toString().equals("Después de comer")){
                    when = 1;
                }else{
                    when = 2;
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        etDateGlucose.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(v.getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear,year))>=0){
                                    etDateGlucose.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    year2 = year;
                                    month = monthOfYear;
                                    day = dayOfMonth;
                                    time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2, 0);
                                    fechaActual=checkDate(dayOfMonth,monthOfYear+1,year);
                                }else{
                                    Toast.makeText(getApplicationContext(),"La fecha introducida no debe ser posterior a la actual", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });


        etTimeGlucose.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TimePickerDialog tpd = new TimePickerDialog(v.getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                etTimeGlucose.setText(checkTime(hourOfDay, minute));
                                hour = hourOfDay;
                                minute2 = minute;
                                time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2, 0);
                            }
                        }, mHour, mMinute, true);

                tpd.show();
            }
        });









        checkGlucose= (CheckBox) findViewById(R.id.checkBox);
        checkGlucose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    System.out.println("Checked");
                    tvDateGlucose.setVisibility(view.VISIBLE);
                    etDateGlucose.setVisibility(view.VISIBLE);
                    etTimeGlucose.setVisibility(view.VISIBLE);
                    etIndiceGlucose.setVisibility(view.VISIBLE);

                    etIndiceGlucose.setText(100+"");
                    tvTimeGlucose.setVisibility(view.VISIBLE);
                    tvIndiceGlucose.setVisibility(view.VISIBLE);
                    tvMgGlucose.setVisibility(view.VISIBLE);
                    tvWhenGlucose.setVisibility(view.VISIBLE);
                    spinnerWhenGlucose.setVisibility(view.VISIBLE);
                } else {
                    System.out.println("Un-Checked");
                    tvDateGlucose.setVisibility(view.INVISIBLE);
                    etDateGlucose.setVisibility(view.INVISIBLE);
                    etTimeGlucose.setVisibility(view.INVISIBLE);
                    etIndiceGlucose.setVisibility(view.INVISIBLE);
                    tvTimeGlucose.setVisibility(view.INVISIBLE);
                    tvIndiceGlucose.setVisibility(view.INVISIBLE);
                    tvWhenGlucose.setVisibility(view.INVISIBLE);
                    spinnerWhenGlucose.setVisibility(view.INVISIBLE);
                    tvMgGlucose.setVisibility(view.INVISIBLE);
                }
            }
        });

        etDateInsuline = (EditText)findViewById(R.id.editTextDateInsuline);
        etDateInsuline.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(v.getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDayInsuline,mMonthInsuline,mYearInsuline).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear,year))>=0){
                                    etDateInsuline.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    yearInsuline = year;
                                    monthInsuline = monthOfYear;
                                    dayInsuline = dayOfMonth;
                                    timeInsuline = DateConvert.valuesToMilliseconds(dayInsuline, monthInsuline+1, yearInsuline, hourInsuline, minuteInsuline, 0);
                                    end=timeInsuline-604800000;
                                    fechaActualInsuline=checkDate(dayOfMonth,monthOfYear+1,year);
                                }else{
                                    Toast.makeText(getApplicationContext(),"La fecha introducida no debe ser posterior a la actual", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYearInsuline, mMonthInsuline, mDayInsuline);
                dpd.show();
            }
        });

        etTimeInsuline = (EditText)findViewById(R.id.editTextTimeInsuline);
        etTimeInsuline.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TimePickerDialog tpd = new TimePickerDialog(v.getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                etTimeInsuline.setText(checkTime(hourOfDay, minute));
                                hourInsuline = hourOfDay;
                                minuteInsuline = minute;
                                timeInsuline = DateConvert.valuesToMilliseconds(dayInsuline, monthInsuline+1, yearInsuline, hourInsuline, minuteInsuline, 0);
                            }
                        }, mHourInsuline, mMinuteInsuline, true);

                tpd.show();
            }
        });

        etInsuline = (EditText) findViewById(R.id.editTextInsuline);
        etInsuline.setText(1+"");


        iniciarFecha(this);

    }
    private void iniciarFecha(InsulineActivity view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etDateInsuline.setText(formatDate.format(c.getTime()));
        etTimeInsuline.setText(checkTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE)));
        fechaActualInsuline=formatDate.format(c.getTime());
        etDateGlucose.setText(formatDate.format(c.getTime()));
        etTimeGlucose.setText(checkTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE)));
        fechaActual=formatDate.format(c.getTime());
    }
    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    public String checkTime(int hour, int minute){
        String time = null;
        if(hour<10){
            time = "0"+hour;
        }else{
            time=""+hour;
        }
        if(minute<10){
            time = time + ":0"+minute;
        }else{
            time = time +":"+minute;
        }
        return time;
    }

    public void checkControl(long time,long end){
        Store.get().setdailyPoints(Store.get().getdailyPoints()+5);
        Calendar c=Calendar.getInstance(Locale.FRANCE);
        Date d= new Date(time);
        c.setTime(d);
        final int weekOftime=c.get(Calendar.WEEK_OF_YEAR);

        final int  dayOfWeek=c.get(Calendar.DAY_OF_WEEK);

        switch (dayOfWeek){
            case 1:
                startWeekDay=7;
                break;
            case 2:
                startWeekDay=1;
                break;
            case 3:
                startWeekDay=2;
                break;
            case 4:
                startWeekDay=3;
                break;
            case 5:
                startWeekDay=4;
                break;
            case 6:
                startWeekDay=5;
                break;
            case 7:
                startWeekDay=6;
                break;
        }





        String url = "http://aplicaciondimo.com/getInsuline.php?email="+Store.get().getEmail()+"&start="+end+"&end="+time;

        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject glucose = (JSONObject) jsonArray1.get(i);
                                    long date = Long.parseLong(String.valueOf(glucose.get("date")));
                                    Calendar c = Calendar.getInstance();
                                    Date dateofInsuline = new Date(date);
                                    c.setTime(dateofInsuline);
                                    int weekOfDate = c.get(Calendar.WEEK_OF_YEAR);
                                    //Control changue of week
                                    if (weekOfDate == weekOftime) {


                                        if (startWeekDay == 1 && Store.get().getControlDay1() == 0) {
                                            Store.get().setControlDay1(1);


                                        }
                                        if (startWeekDay == 2 && Store.get().getControlDay2() == 0) {
                                            Store.get().setControlDay2(1);

                                        }
                                        if (startWeekDay == 3 && Store.get().getControlDay3() == 0) {
                                            Store.get().setControlDay3(1);

                                        }
                                        if (startWeekDay == 4 && Store.get().getControlDay4() == 0) {
                                            Store.get().setControlDay4(1);


                                        }
                                        if (startWeekDay == 5 && Store.get().getControlDay5() == 0) {
                                            Store.get().setControlDay5(1);

                                        }

                                        if (startWeekDay == 6 && Store.get().getControlDay6() == 0) {

                                            Store.get().setControlDay6(1);
                                        }
                                        if (startWeekDay == 7 && Store.get().getControlDay7() == 0) {
                                            Store.get().setControlDay7(1);
                                        }
                                    }



                                    if(Store.get().getControlDay1()==1 && Store.get().getControlDay2()==1 && Store.get().getControlDay3()==1
                                            && Store.get().getControlDay4()==1 && Store.get().getControlDay5()==1 && Store.get().getControlDay6()==1 &&
                                            Store.get().getControlDay7()==1) {

                                        Store.get().setweeklyPoints(Store.get().getweeklyPoints()+250);
                                        Store.get().setControlDay1(0);
                                        Store.get().setControlDay2(0);
                                        Store.get().setControlDay3(0);
                                        Store.get().setControlDay4(0);
                                        Store.get().setControlDay5(0);
                                        Store.get().setControlDay6(0);
                                        Store.get().setControlDay7(0);
                                        makeRequestBD("http://aplicaciondimo.com/updateUser.php?email=" +Store.get().getEmail()+"&dailyPoints="+Store.get().getdailyPoints()+"&weeklyPoints="+Store.get().getweeklyPoints()+"&controlDay1="+Store.get().getControlDay1()+
                                                "&controlDay2="+Store.get().getControlDay2()+"&controlDay3="+Store.get().getControlDay3()
                                                +"&controlDay4="+Store.get().getControlDay4()+"&controlDay5="+Store.get().getControlDay5()+"&controlDay6="+Store.get().getControlDay6()+
                                                "&controlDay7="+Store.get().getControlDay7()+ "&category="+Store.get().getCategoria());
                                    }

                                }

                            }

                            makeRequestBD("http://aplicaciondimo.com/updateUser.php?email=" +Store.get().getEmail()+"&dailyPoints="+Store.get().getdailyPoints()+"&weeklyPoints="+Store.get().getweeklyPoints()+"&controlDay1="+Store.get().getControlDay1()+
                                    "&controlDay2="+Store.get().getControlDay2()+"&controlDay3="+Store.get().getControlDay3()
                                    +"&controlDay4="+Store.get().getControlDay4()+"&controlDay5="+Store.get().getControlDay5()+"&controlDay6="+Store.get().getControlDay6()+
                                    "&controlDay7="+Store.get().getControlDay7()+ "&category="+Store.get().getCategoria());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartGlu: ", error.toString() );

            }
        });

        // and finally add the request to the queue
        Volley.newRequestQueue(this).add(customRequest);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_appointment, menu);

        //setBadgeCount(this, mCartMenuIcon, ""+ 1);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            NavUtils.navigateUpTo(this, intent);
            return true;
        }
        if(id == R.id.action_Appointment_help) {
            ShowcaseConfig config = new ShowcaseConfig();
            MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this);
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)

                            .setTarget(etDateInsuline)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_DateInsuline))
                            .build()
            );
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(etTimeInsuline)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_HourInsuline))
                            .build()
            );
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(etInsuline)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_ValueInsuline))
                            .build()
            );
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(checkGlucose)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_checkInsuline))
                            .build()
            );

            sequence.addSequenceItem(okButton,
                    getString(R.string.HelpText_Save), getString(R.string.Helptext_Next));




            sequence.start();


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonOk:
                checkControl(timeInsuline,end);


                valueInsuline=Integer.valueOf(String.valueOf(etInsuline.getText()));
                if(etIndiceGlucose.getVisibility()==View.VISIBLE){
                    value=Integer.valueOf(String.valueOf(etIndiceGlucose.getText()));
                    makeRequestBD("http://aplicaciondimo.com/setGlucose.php?email=" +email+"&date="+time+"&type="+when+"&value="+value);

                    Toast.makeText(getApplicationContext(),"Se ha introducido correctamente la dosis de insulina y el indice de glucemia", Toast.LENGTH_SHORT).show();

                }

                makeRequestBD("http://aplicaciondimo.com/setInsuline.php?email="+email+"&date="+timeInsuline+ "&type=" + insulineType + "&value=" + valueInsuline);


                if(etIndiceGlucose.getVisibility()==View.INVISIBLE) {
                    Toast.makeText(getApplicationContext(), "Dosis de insulina introducida correctamente", Toast.LENGTH_SHORT).show();

                }



                break;

            case R.id.action_Appointment_help:

                break;

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(Store.get().getEmail()==null){
            Intent int1 =  new Intent(this, SignInActivity.class);
            int1.putExtra("activity", "GlucoseActivity");
            startActivity(int1);
            Toast.makeText(this,"Conectando al servidor",Toast.LENGTH_SHORT).show();
        }
    }
    public void makeRequestBD(String URL){

        //Toast.makeText(getActivity(), ""+URL, Toast.LENGTH_SHORT).show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");
                if ( (response.length()==0)) {


                    try {

                        JSONArray ja = new JSONArray(response);



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error","Error "+error);

            }
        });

        queue.add(stringRequest);
        finish();

    }

}
