package mami.dimoapp;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.model.Store;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class WeightActivity extends AppCompatActivity implements View.OnClickListener{
    private NumberPicker np;
    private EditText etDateWeight, etTimeWeight, etWeight;
    private int mYear;
    private int mMonth;
    private int mDay;
    private int mHour;
    private int mMinute;
    private double value;
    private int year2;
    private int month;
    private int day;
    private int hour;
    private int minute2;
    private Spinner spinnerWhen;
    private int when;
    private long time;
    private CustomRequest customRequest;
    private String fechaActual;
    private String email;
    private int points, totalPoints;
    private Button okButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Peso");
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        //initialize date for the pickers

        email=Store.get().getEmail();
        Store.get().setExists(true);
        points=Store.get().getdailyPoints();
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        //initialize default date
        year2 = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute2 = c.get(Calendar.MINUTE);
        okButton = (Button)findViewById(R.id.buttonOk);
        okButton.setOnClickListener(this);

        time = System.currentTimeMillis();
        value= 80.2;






        etDateWeight = (EditText)findViewById(R.id.editTextDateInsuline);
        etDateWeight.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(v.getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(DateConvert.valuesToDate(mDay,mMonth,mYear).compareTo(DateConvert.valuesToDate(dayOfMonth,monthOfYear,year))>=0){
                                    etDateWeight.setText(checkDate(dayOfMonth,monthOfYear+1,year));
                                    year2 = year;
                                    month = monthOfYear;
                                    day = dayOfMonth;
                                    time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2, 0);
                                    fechaActual=checkDate(dayOfMonth,monthOfYear+1,year);
                                }else{
                                    Toast.makeText(getApplicationContext(),"La fecha introducida no debe ser posterior a la actual", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        etTimeWeight = (EditText)findViewById(R.id.editTextTimeInsuline);
        etTimeWeight.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                TimePickerDialog tpd = new TimePickerDialog(v.getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                etTimeWeight.setText(checkTime(hourOfDay, minute));
                                hour = hourOfDay;
                                minute2 = minute;
                                time = DateConvert.valuesToMilliseconds(day, month+1, year2, hour, minute2, 0);
                            }
                        }, mHour, mMinute, true);

                tpd.show();
            }
        });

        etWeight = (EditText) findViewById(R.id.editTextInsuline);
        etWeight.setText(70.2+"");


        iniciarFecha(this);

    }
    private void iniciarFecha(WeightActivity view) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        etDateWeight.setText(formatDate.format(c.getTime()));
        etTimeWeight.setText(checkTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE)));
        fechaActual=formatDate.format(c.getTime());
    }
    public String checkDate(int dayOfMonth, int monthOfYear, int year){
        String date = null;
        if(dayOfMonth<10){
            date = "0"+dayOfMonth;
        }else{
            date=""+dayOfMonth;
        }
        if(monthOfYear<10){
            date = date + "/0"+monthOfYear;
        }else{
            date = date +"/"+monthOfYear;
        }
        date = date+"/"+year;
        return date;
    }

    public String checkTime(int hour, int minute){
        String time = null;
        if(hour<10){
            time = "0"+hour;
        }else{
            time=""+hour;
        }
        if(minute<10){
            time = time + ":0"+minute;
        }else{
            time = time +":"+minute;
        }
        return time;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_appointment, menu);

        //setBadgeCount(this, mCartMenuIcon, ""+ 1);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            NavUtils.navigateUpTo(this, intent);
            return true;
        }
        if(id == R.id.action_Appointment_help) {
            ShowcaseConfig config = new ShowcaseConfig();
            MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this);
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)

                            .setTarget(etDateWeight)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_DateWeight))
                            .build()
            );
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(etTimeWeight)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_HourWeight))
                            .build()
            );
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(etWeight)
                            .setDismissText(getString(R.string.Helptext_Next))
                            .withRectangleShape()
                            .setContentText(getString(R.string.HelpText_ValueWeight))
                            .build()
            );

            sequence.addSequenceItem(okButton,
                    getString(R.string.HelpText_Save), getString(R.string.Helptext_Next));




            sequence.start();


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonOk:
                value=Double.valueOf(String.valueOf(etWeight.getText()));
                Store.get().setdailyPoints(Store.get().getdailyPoints()+5);
                makeRequestBD("http://aplicaciondimo.com/updateUser.php?email=" +Store.get().getEmail()+"&dailyPoints="+Store.get().getdailyPoints()+"&weeklyPoints="+Store.get().getweeklyPoints()+"&controlDay1="+Store.get().getControlDay1()+
                        "&controlDay2="+Store.get().getControlDay2()+"&controlDay3="+Store.get().getControlDay3()
                        +"&controlDay4="+Store.get().getControlDay4()+"&controlDay5="+Store.get().getControlDay5()+"&controlDay6="+Store.get().getControlDay6()+
                        "&controlDay7="+Store.get().getControlDay7()+ "&category="+Store.get().getCategoria());
                makeRequestBD("http://aplicaciondimo.com/setWeight.php?email=" +email + "&date=" + time + "&value=" + value);

                Toast.makeText(getApplicationContext(),"Peso registrado correctamente", Toast.LENGTH_SHORT).show();
                break;

            case R.id.action_Appointment_help:

                break;

        }

    }


    public void makeRequestBD(String URL){



        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");
                if ( (response.length()==0)) {
                    totalPoints=points+2;


                    try {

                        JSONArray ja = new JSONArray(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error","Error "+error);

            }
        });

        queue.add(stringRequest);
        finish();

    }

}
