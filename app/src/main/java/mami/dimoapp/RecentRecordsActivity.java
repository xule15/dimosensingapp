package mami.dimoapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.data.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import mami.dimoapp.custom.CustomRequest;
import mami.dimoapp.custom.CustomRequest2;
import mami.dimoapp.custom.DateConvert;
import mami.dimoapp.dialogs.CustomDialogAppointment;
import mami.dimoapp.fragments.FragmentGlucose;
import mami.dimoapp.model.Appointment;
import mami.dimoapp.model.GlucosePoint;
import mami.dimoapp.model.InsulinePoint;
import mami.dimoapp.model.Store;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class RecentRecordsActivity extends AppCompatActivity implements View.OnClickListener{

    private RecyclerView recView;
    private SwipeRefreshLayout swipeContainer;
    private List<Appointment> datos=new ArrayList<>();
    private List<GlucosePoint> datos3;

    FragmentManager fragmentManager = getSupportFragmentManager();
    private String name, email, personPhoto;
    private SimpleItemRecyclerViewAdapter adapter,adapterGlucose;
    private TextView tvAppointments;
    int mYear, mMonth, mDay, mHour, mMinute;
    private long start, end;
    private Date dateStart;
    private ImageView img;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_records);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Últimos Registros");
        email=Store.get().getEmail();
        img= (ImageView) findViewById(R.id.activityImg);


        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        tvAppointments = (TextView)findViewById(R.id.tvAppointments);


        fragmentManager = getSupportFragmentManager();

        getAppointmentRequest();
       // getGlucose();
        getChartGlucose();
        getChartInsuline();


        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer .setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAppointmentRequest();
                getChartGlucose();
                getChartInsuline();
                swipeContainer.setRefreshing(false);


            }
        });

    }

    public void setAdapter(final SimpleItemRecyclerViewAdapter adapter){
        //Setear propiedades RecyclerView
        recView = (RecyclerView) findViewById(R.id.recentRecordsList);
        recView.setHasFixedSize(true);

        recView.setAdapter(adapter);
        recView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false));
    }

    public void getAppointmentRequest2(){
      //  img= (ImageView) findViewById(R.id.activityImg);

       // if(img!=null){
         //   img.setImageResource(R.drawable.ic_blood_18dp);
        //}
        String URL= "http://aplicaciondimo.com/getAppointment.php?email="+Store.get().getEmail()+"&start="+System.currentTimeMillis();;


        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");


                try {


                    JSONArray ja = new JSONArray(response);
                    if(ja.length()>0){
                       // datos = new ArrayList<>();
                        for(int i=0;i<response.length();i++) {
                            long timestamp = ja.getLong(0);
                            long timestamp2 = System.currentTimeMillis();
                            String description = ja.getString(3);
                            String place = ja.getString(2);
                            String title = ja.getString(1);



                            Appointment app = new Appointment(timestamp, title, place, description);

                            datos.add(app);

                            datos.add(app);
                        }

                        adapter = new SimpleItemRecyclerViewAdapter(datos);
                        Collections.sort(datos, new Comparator<Appointment>() {
                            @Override
                            public int compare(Appointment gp1, Appointment gp2) {
                                return (int) (gp1.getTime() - gp2.getTime());
                            }
                        });

                        adapter = new SimpleItemRecyclerViewAdapter(datos);

                        setAdapter(adapter);
                    }else{
                        tvAppointments.setText("No existen próximas citas");
                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //}


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    public void getAppointmentRequest(){
        String url= "http://aplicaciondimo.com/getAppointment.php?email="+Store.get().getEmail()+"&start="+System.currentTimeMillis();;


        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIENgetAppointment","Response is: "+ jsonArray.toString());
                        datos = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0) {
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject appointment = (JSONObject) jsonArray1.get(i);
                                    String description = (String) appointment.get("description");
                                    String place = (String) appointment.get("place");
                                    String title = (String) appointment.get("title");
                                    long timestamp = Long.parseLong(String.valueOf(appointment.get("date")));
                                    Appointment app = new Appointment(timestamp, title, place, description);

                                    datos.add(app);



                                }

                                Collections.sort(datos, new Comparator<Appointment>() {
                                    @Override
                                    public int compare(Appointment gp1, Appointment gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });

                                adapter = new SimpleItemRecyclerViewAdapter(datos);

                                setAdapter(adapter);
                            }else{
                                tvAppointments.setText("No existen próximas citas");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });

        Volley.newRequestQueue(getApplicationContext()).add(customRequest);
    }


    public void getGlucose(){


        long today =System.currentTimeMillis();
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        dateStart = DateConvert.valuesToDate(mDay,mMonth,mYear);

        start = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0);
        //start = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0)+86399999;

        //start = start-259200000L;
        end = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0)+86399999;



        String URL = "http://aplicaciondimo.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;



        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");


                try {

                    JSONArray ja = new JSONArray(response);
                    if(ja.length()>0){
                        int elemento=0;


                        for(int i=0;i<ja.length()/3;i++) {
                            long timestamp = ja.getLong(elemento);

                            String description = ja.getString(elemento+1);
                            String place = ja.getString(elemento+2);
                            String title = ja.getString(elemento+2);



                            Appointment app = new Appointment(timestamp, title, place, description);


                            datos.add(app);
                            elemento+=3;
                        }
                        adapter = new SimpleItemRecyclerViewAdapter(datos);
                        Collections.sort(datos, new Comparator<Appointment>() {
                            @Override
                            public int compare(Appointment gp1, Appointment gp2) {
                                return (int) (gp1.getTime() - gp2.getTime());
                            }
                        });

                        adapter = new SimpleItemRecyclerViewAdapter(datos);


                        setAdapter(adapter);
                    }else{
                        tvAppointments.setText("No existen próximas citas");
                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //}


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);
        getInsuline();

    }

    public void getChartGlucose(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        final ArrayList<Entry> entries = new ArrayList<Entry>();
        start = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0);
        start = start-259200000L;
        end = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0)+86399999;

        //String url = "http://IP/api/v2.0/glucose?"+startDate+"&"+endDate;
        String url = "http://aplicaciondimo.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;

        //Create the list of the cookies, i.e. key=value

        datos = new ArrayList<>();

        CustomRequest2 customRequest = new CustomRequest2(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("BIEN","Response is: "+ jsonArray.toString());
                        try {

                            if(jsonArray.length()>0) {
                                JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                                if(jsonArray1.length()>0) {
                                    String type;
                                    for (int i = 0; i < jsonArray1.length(); i++) {
                                        JSONObject glucose = (JSONObject) jsonArray1.get(i);
                                        String check = String.valueOf(glucose.get("type"));
                                        String value = String.valueOf(glucose.get("value"));
                                        long id = Long.parseLong(String.valueOf(glucose.get("date")));

                                        if (check.equals("0")) {
                                            type = "Antes de comer";

                                        } else if (check.equals("1")) {
                                            type = "Después de comer";
                                        } else {
                                            type = "Otros";
                                        }
                                        Appointment app = new Appointment(id, "Glucosa: " + value, "", type);
                                        datos.add(app);
                                    }
                                }



                                adapter = new SimpleItemRecyclerViewAdapter(datos);
                                Collections.sort(datos, new Comparator<Appointment>() {
                                    @Override
                                    public int compare(Appointment gp1, Appointment gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });

                                adapter = new SimpleItemRecyclerViewAdapter(datos);


                                setAdapter(adapter);
                            }else{
                                tvAppointments.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartGlu: ", error.toString() );

            }
        });

        // and finally add the request to the queue
        Volley.newRequestQueue(this).add(customRequest);
    }
    public void getChartInsuline(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        final ArrayList<Entry> entries = new ArrayList<Entry>();
        start = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0);
        start = start-259200000L;
        end = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0)+86399999;

        //String url = "http://IP/api/v2.0/glucose?"+startDate+"&"+endDate;
        String url ="http://aplicaciondimo.com/getInsuline.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;

        //Create the list of the cookies, i.e. key=value

        datos = new ArrayList<>();

        final CustomRequest customRequest = new CustomRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        ArrayList<InsulinePoint> insulinList = new ArrayList<>();
                        try {
                            JSONArray jsonArray1 = (JSONArray) jsonArray.get("data");
                            if(jsonArray1.length()>0 ) {
                                String typeInsuline;

                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject insulin = (JSONObject) jsonArray1.get(i);
                                    int type = Integer.parseInt(String.valueOf(insulin.get("type")));
                                    float dose = Float.parseFloat(String.valueOf(insulin.get("value")));
                                    long id = Long.parseLong(String.valueOf(insulin.get("date")));

                                    if(type==0){
                                        typeInsuline="Rápida";

                                    } else  if(type==1){
                                        typeInsuline="Corta";
                                    } else  if(type==2){
                                        typeInsuline="Intermedia";
                                    }else{
                                        typeInsuline="Lenta";
                                    }
                                    Appointment app = new Appointment(id, "Insulina: "+dose,"",typeInsuline);
                                    datos.add(app);

                                }




                                adapter = new SimpleItemRecyclerViewAdapter(datos);
                                Collections.sort(datos, new Comparator<Appointment>() {
                                    @Override
                                    public int compare(Appointment gp1, Appointment gp2) {
                                        return (int) (gp1.getTime() - gp2.getTime());
                                    }
                                });

                                adapter = new SimpleItemRecyclerViewAdapter(datos);


                                setAdapter(adapter);
                            }else{
                                tvAppointments.setText("No existen datos suficientes");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ChartGlu: ", error.toString() );

            }
        });

        // and finally add the request to the queue
        Volley.newRequestQueue(this).add(customRequest);
    }


    public void getInsuline(){


        long today =System.currentTimeMillis();
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH)+1;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        dateStart = DateConvert.valuesToDate(mDay,mMonth,mYear);

        start = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0);
        //start = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0)+86399999;

        //start = start-259200000L;
        end = DateConvert.valuesToMilliseconds(mDay,mMonth,mYear,0,0,0)+86399999;


        String URL = "https://aplicaciondimo.000webhostapp.com/getGlucose.php?email="+Store.get().getEmail()+"&start="+start+"&end="+end;

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][",",");


                try {

                    JSONArray ja = new JSONArray(response);
                    if(ja.length()>0){
                        int elemento=0;


                        for(int i=0;i<ja.length()/3;i++) {
                            long timestamp = ja.getLong(elemento);

                            String description = ja.getString(elemento+1);
                            String place = ja.getString(elemento+2);
                            String title = ja.getString(elemento+2);



                            Appointment app = new Appointment(timestamp, title, place, description);


                            datos.add(app);
                            elemento+=3;
                        }
                        adapter = new SimpleItemRecyclerViewAdapter(datos);
                        Collections.sort(datos, new Comparator<Appointment>() {
                            @Override
                            public int compare(Appointment gp1, Appointment gp2) {
                                return (int) (gp1.getTime() - gp2.getTime());
                            }
                        });

                        adapter = new SimpleItemRecyclerViewAdapter(datos);

                        setAdapter(adapter);
                    }else{
                        tvAppointments.setText("No existen próximas citas");
                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //}


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            //NavUtils.navigateUpTo(this, intent);
            return true;
        }
        if(id == R.id.action_Appointment_help) {

            new MaterialShowcaseView.Builder(this)
                    .setTarget(tvAppointments)
                    .setDismissText(getString(R.string.Helptext_Next))
                    .withRectangleShape()
                    .setContentText(getString(R.string.HelpText_RecentRecord))
                    .show();



        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_appointment, menu);

        //setBadgeCount(this, mCartMenuIcon, ""+ 1);
        return super.onCreateOptionsMenu(menu);
    }









    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.appointment_action:
                CustomDialogAppointment dialogAppointment = new CustomDialogAppointment();
                dialogAppointment.show(fragmentManager,"AppointmentDialog");
                break;
            case R.id.action_Appointment_help:

                break;
        }

    }

    //Adapter para RecyclerView
    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private List<Appointment> datos;


        public SimpleItemRecyclerViewAdapter(List<Appointment> datos) {
            this.datos = datos;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_view_recent_records, parent, false);
            ViewHolder tvh = new ViewHolder(view);

            return tvh;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.recentTitle.setText(datos.get(position).getTitle());
            //holder.appointmentPlace.setText(datos.get(position).getPlace());
            holder.recentDescription.setText(datos.get(position).getDescription());

            holder.recentDate.setText(DateConvert.millisecondsToDateStringWithoutSeconds(datos.get(position).getTime()));
            holder.itemView.setTag(0);
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener(){

                @Override
                public boolean onLongClick(View view) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(RecentRecordsActivity.this);
                    builder1.setTitle("Atención");
                    builder1.setMessage("¿Deseas eliminar este registro?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //se elmina aqui
                            String url;

                            if(datos.get(position).getTitle().toString().contains("Glucosa")){
                                url="http://aplicaciondimo.com/deleteGlucose.php";
                                DeleteRecords(url,datos.get(position).getTime());

                                Intent int1 = new Intent(getApplicationContext(), RecentRecordsActivity.class);
                                startActivity(int1);




                            }
                            else if(datos.get(position).getTitle().toString().contains("Insulina")){
                                url="http://aplicaciondimo.com/deleteInsuline.php";
                                DeleteRecords(url,datos.get(position).getTime());
                                Intent int1 = new Intent(getApplicationContext(), RecentRecordsActivity.class);
                                startActivity(int1);

                            }else{
                                url="http://aplicaciondimo.com/deleteAppointment.php";
                                DeleteRecords(url,datos.get(position).getTime());
                                Intent int1 = new Intent(getApplicationContext(), RecentRecordsActivity.class);
                                startActivity(int1);

                            }
                            dialog.cancel();
                        }
                    });

                    builder1.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                    return false;
                }
            });
        }





        //Conseguir tamaño del array
        @Override
        public int getItemCount() {
            return datos.size();
        }

        public class ViewHolder
                extends RecyclerView.ViewHolder {

            public final TextView recentTitle;

            public final TextView recentDescription;
            public final TextView recentDate;
            public final RelativeLayout appointmentRl;



            public ViewHolder(View view) {
                super(view);
                recentTitle = (TextView)view.findViewById(R.id.RecentTitle);
                recentDescription = (TextView)view.findViewById(R.id.RecentDescription);
                recentDate = (TextView)view.findViewById(R.id.RecentDate);
                appointmentRl = (RelativeLayout) view.findViewById(R.id.appointmentRl);
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if(Store.get().getEmail()==null){
            Intent int1 =  new Intent(this, MainActivity.class);
            startActivity(int1);
            Toast.makeText(this,"Conectando al servidor",Toast.LENGTH_SHORT).show();
        }
    }


    public void DeleteRecords(String url, long time){

        JSONObject activity = new JSONObject();



        try {
            activity.put("email",Store.get().getEmail());
            activity.put("date", time);




        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Instantiate the RequestQueue.


        Log.e("urlActivityDelete ","urlActivity "+url);

        CustomRequest2 customRequest = new CustomRequest2(Request.Method.POST, url,activity,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonArray) {
                        // Display the first 500 characters of the response string.
                        Log.e("Eliminado reciente","Response is: "+ jsonArray.toString());
                        //Store.get().setSteps(0);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errorDelete ","errorDelete "+ error.toString() );
            }
        });


        // and finally add the request to the queue
        Volley.newRequestQueue(getApplicationContext()).add(customRequest);

        //pastActivity = "";
    }

}
