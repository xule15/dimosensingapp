package mami.dimoapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import mami.dimoapp.model.Appointment;
import mami.dimoapp.model.Profile;
import mami.dimoapp.model.Store;

public class WebViewLogin extends AppCompatActivity {

    private WebView webView;
    private String cookies;
    private CookieManager cookie;
    String openActivity;
    private ProgressBar progressLoading;
    private TextView tvloading;
    public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_login);
    /*final ActionBar actionBar = getSupportActionBar();
          if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_close_dark);
        }*/

        progressLoading = (ProgressBar) findViewById(R.id.progressBar3);
        tvloading = (TextView) findViewById(R.id.textView2);

        openActivity = "";
        webView = (WebView) findViewById(R.id.webView1);
        webView.getSettings().setUserAgentString(USER_AGENT);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());

        if (getIntent().hasExtra("activity")) {
            openActivity = getIntent().getExtras().getString("activity");
            webView.loadUrl("http://mamilab.esi.uclm.es:5050/login");
        } else {
            //webView.loadUrl(getIntent().getExtras().getString("url"));
            webView.loadUrl("http://mamilab.esi.uclm.es:5050/login");
        }

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if(url.contains("https://accounts.google.com/ServiceLogin")||url.contains("https://accounts.google.com/signin/challenge/sl/password")){
                    webView.setVisibility(View.VISIBLE);
                    progressLoading.setVisibility(View.GONE);
                    tvloading.setVisibility(View.GONE);
                }
                if(!url.equals("http://mamilab.esi.uclm.es:5050/webapp/login")) {
                    //cookie = CookieManager.getInstance();
                    cookies = CookieManager.getInstance().getCookie(url);
                    Store.get().setCookie(cookies);
                    Store.get().setEnableActivity(true);
                    Log.e("COOKIEE", "Response is: " + cookies);
                    if(cookies!=null) {
                        if (cookies.substring(0, 7).equals("session") && cookies != null) {
                            Intent intent = new Intent();
                            intent = new Intent(getApplicationContext(), MainActivity.class);
                          /*  if (openActivity.equals("Appointments"))
                                intent = new Intent(getApplicationContext(), Appointment.class);
                            if (openActivity.equals("Main"))
                                intent = new Intent(getApplicationContext(), MainActivity.class);
                            if (openActivity.equals("Stats"))
                              intent = new Intent(getApplicationContext(), RewardsActivity.class);
                            if (openActivity.equals("appointments2"))
                                intent = new Intent(getApplicationContext(), RewardsActivity.class);*/

                           // if (openActivity.equals("Stats"))
                             //   intent = new Intent(getApplicationContext(), StatsActivity2.class);

                            getProfile();
                           if(openActivity.equals(""))Toast.makeText(getApplicationContext(), "Bienvenido a DiMo", Toast.LENGTH_LONG).show();
                            startActivity(intent);
                        }
                    }
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if(url.equals("http://mamilab.esi.uclm.es:5050/webapp/login") || url.equals("http://mamilab.esi.uclm.es:5050/webapp?filename=#")){
                    webView.stopLoading();
                    Log.e("STOP", "STOP LOADING");
                }
                /*if(url.equals("http://mamilab.esi.uclm.es:5050/login")||
                    url.contains("https://accounts.google.com/o/oauth2")||
                    url.contains("http://mamilab.esi.uclm.es:5050/oauthorized")||
                    url.contains("ht tps://accounts.google.com/signin/challenge/sl/password")){
                }else{
                    webView.stopLoading();

                }*/
            }

        });



        /*webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading (WebView view, String url){
                //True if the host application wants to leave the current WebView and handle the url itself, otherwise return false.
                return false;
            }
        });*/
    }

    public void getProfile(){
        Profile profile = new Profile();
        profile.getProfileRequest(this);
        profile.getWeightRequest(this);
        profile.getHeightRequest(this);
        profile.getControlRangeRequest(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home) {

            //NavUtils.navigateUpTo(this, intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}